//
//  Contacts+CoreDataProperties.swift
//  
//
//  Created by kimseungmin on 2017. 4. 29..
//
//

import Foundation
import CoreData


extension Contacts {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Contacts> {
        return NSFetchRequest<Contacts>(entityName: "Contacts")
    }

    @NSManaged public var note: NSObject?
    @NSManaged public var phone: NSObject?
    @NSManaged public var name: NSObject?
    @NSManaged public var seq: NSNumber?

}
