//
//  CallDirectoryHandler.swift
//  CallKit-EX
//
//  Created by seungmin kim on 27/05/2019.
//  Copyright © 2019 remain. All rights reserved.
//

import Foundation
import CallKit
//import Alamofire
import ObjectMapper
import UserNotifications

class CallDirectoryHandler: CXCallDirectoryProvider {
	let msg:String = "적용이 완료되었습니다\n최근 통화내역에서 확인할 수 있습니다"
	let title:String = "투넘버 연락처 발신번호 표시"
	
    override func beginRequest(with context: CXCallDirectoryExtensionContext) {
        context.delegate = self
        
		self.addAllIdentificationPhoneNumbers(to: context)
		context.completeRequest()
        // Check whether this is an "incremental" data request. If so, only provide the set of phone number blocking
        // and identification entries which have been added or removed since the last time this extension's data was loaded.
        // But the extension must still be prepared to provide the full set of data at any time, so add all blocking
        // and identification phone numbers if the request is not incremental.
//        if context.isIncremental {
//            addOrRemoveIncrementalBlockingPhoneNumbers(to: context)
//
//            addOrRemoveIncrementalIdentificationPhoneNumbers(to: context)
//        } else {
//            addAllBlockingPhoneNumbers(to: context)
//
//            addAllIdentificationPhoneNumbers(to: context)
//        }
//        do {
//
//            try addAllIdentificationPhoneNumbers(to: context)
//			self.postNotification(msg: "연락처 적용이 완료 되었습니다.\n확인되지 않는 경우 다시 시도 해주세요")
//
//        } catch {
//
//            NSLog("Unable to add identification phone numbers")
//
//            let error = NSError(domain: "CallDirectoryHandler", code: 2, userInfo: nil)
//
//            context.cancelRequest(withError: error)
//			self.postNotification(msg: "연락처 적용이 실패하였습니다.\n잠시 후 다시 시도 해 주세요")
//
//            return
//
//        }
//        context.completeRequest()
    }

    private func addAllBlockingPhoneNumbers(to context: CXCallDirectoryExtensionContext) {
        // Retrieve all phone numbers to block from data store. For optimal performance and memory usage when there are many phone numbers,
        // consider only loading a subset of numbers at a given time and using autorelease pool(s) to release objects allocated during each batch of numbers which are loaded.
        //
        // Numbers must be provided in numerically ascending order.
//        let allPhoneNumbers: [CXCallDirectoryPhoneNumber] = [ 1_408_555_5555, 1_800_555_5555 ]
//        for phoneNumber in allPhoneNumbers {
//            context.addBlockingEntry(withNextSequentialPhoneNumber: phoneNumber)
//        }
    }
    
    private func addOrRemoveIncrementalBlockingPhoneNumbers(to context: CXCallDirectoryExtensionContext) {
        // Retrieve any changes to the set of phone numbers to block from data store. For optimal performance and memory usage when there are many phone numbers,
        // consider only loading a subset of numbers at a given time and using autorelease pool(s) to release objects allocated during each batch of numbers which are loaded.
//        let phoneNumbersToAdd: [CXCallDirectoryPhoneNumber] = [ 1_408_555_1234 ]
//        for phoneNumber in phoneNumbersToAdd {
//            context.addBlockingEntry(withNextSequentialPhoneNumber: phoneNumber)
//        }
//
//        let phoneNumbersToRemove: [CXCallDirectoryPhoneNumber] = [ 1_800_555_5555 ]
//        for phoneNumber in phoneNumbersToRemove {
//            context.removeBlockingEntry(withPhoneNumber: phoneNumber)
//        }
        
        // Record the most-recently loaded set of blocking entries in data store for the next incremental load...
    }
    
    private func addAllIdentificationPhoneNumbers(to context: CXCallDirectoryExtensionContext) {

		if let userDefaults = UserDefaults(suiteName: "group.com.remain.Callkit") {
			if let json = userDefaults.string(forKey: "data"){
				let models = Mapper<ContactModel>().mapArray(JSONString: json) ?? []
				for item in models{
					if let phoneNumber = Int(item.phone){
						let name = item.name
						context.addIdentificationEntry(withNextSequentialPhoneNumber: CXCallDirectoryPhoneNumber(phoneNumber), label: name)
					}
					
				}
				
				let isPurchased = userDefaults.bool(forKey: "purchase")
				if isPurchased{
					self.postNotification(msg: self.msg)
				}
				else{
					// no purchase
				}
				
				
			}
			
		}
		
//
//        guard let userDefaults = UserDefaults(suiteName: "group.com.remain.Callkit") else { return }
//        let callKitLoad = userDefaults.bool(forKey:"firstLoad")
//        if callKitLoad == true{
//            let phoneNumbers: [CXCallDirectoryPhoneNumber] = [82120, 82215889999]
//            let labels = [ "국민은행 대표번호", "서울 다산콜센터" ]
//            for (phoneNumber, label) in zip(phoneNumbers, labels) {
//                context.addIdentificationEntry(withNextSequentialPhoneNumber: phoneNumber, label: label)
//            }
//            userDefaults.set(false, forKey: "firstLoad")
//
//        }else{
//            var phoneNumbers: [CXCallDirectoryPhoneNumber] = []
//            var labels:[String] = []
//
//            // Group으로 설정하여 UserDefaults 을 이용하여 데이터를 통신합니다.
//
//            guard let json = userDefaults.string(forKey: "data") else { return }
//
//
//            let models = Mapper<ContactModel>().mapArray(JSONString: json) ?? []
//            for item in models{
//                if let phoneNumber = Int(item.phone){
//                    phoneNumbers.append(CXCallDirectoryPhoneNumber(phoneNumber))
//                    let name = item.name
//                    labels.append(name)
//                }
////                else{
////                    phoneNumbers.append(CXCallDirectoryPhoneNumber(0000000))
////                }
//
//            }
//
//
//            for (phoneNumber, label) in zip(phoneNumbers, labels) {
//
//                context.addIdentificationEntry(withNextSequentialPhoneNumber: phoneNumber, label: label)
//
//            }
//        }
        
        
        
    }
    
    private func addOrRemoveIncrementalIdentificationPhoneNumbers(to context: CXCallDirectoryExtensionContext) {
        // Retrieve any changes to the set of phone numbers to identify (and their identification labels) from data store. For optimal performance and memory usage when there are many phone numbers,
        // consider only loading a subset of numbers at a given time and using autorelease pool(s) to release objects allocated during each batch of numbers which are loaded.
//        let phoneNumbersToAdd: [CXCallDirectoryPhoneNumber] = [ 821053488922 ]
//        let labelsToAdd = [ "New local business" ]
//
//        for (phoneNumber, label) in zip(phoneNumbersToAdd, labelsToAdd) {
//            context.addIdentificationEntry(withNextSequentialPhoneNumber: phoneNumber, label: label)
//        }
//
//        let phoneNumbersToRemove: [CXCallDirectoryPhoneNumber] = [ 1_888_555_5555 ]
//
//        for phoneNumber in phoneNumbersToRemove {
//            context.removeIdentificationEntry(withPhoneNumber: phoneNumber)
//        }
        
        // Record the most-recently loaded set of identification entries in data store for the next incremental load...
    }
    
}

extension CallDirectoryHandler: CXCallDirectoryExtensionContextDelegate {
    
    func requestFailed(for extensionContext: CXCallDirectoryExtensionContext, withError error: Error) {
        // An error occurred while adding blocking or identification entries, check the NSError for details.
        // For Call Directory error codes, see the CXErrorCodeCallDirectoryManagerError enum in <CallKit/CXError.h>.
        //
        // This may be used to store the error details in a location accessible by the extension's containing app, so that the
        // app may be notified about errors which occured while loading data even if the request to load data was initiated by
        // the user in Settings instead of via the app itself.
    }
    
}

extension CallDirectoryHandler{
	
	func postNotification(msg:String){
		let userNotificationCenter = UNUserNotificationCenter.current()
		let notificationContent = UNMutableNotificationContent()

		notificationContent.title = self.title
		notificationContent.body = msg
		notificationContent.sound = UNNotificationSound.default
		notificationContent.threadIdentifier = "F39-C521-A7B"

		let userData:[String:String] = ["phoneNumber":"",
										"name":""
										]
		notificationContent.userInfo = ["userInfo": userData]


		let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
		let nowDouble = NSDate().timeIntervalSince1970
		let now = Int(nowDouble*1000)
		let request = UNNotificationRequest(identifier: "\(now)",
											content: notificationContent,
											trigger: trigger)
		userNotificationCenter.removeAllPendingNotificationRequests()
		userNotificationCenter.add(request) { error in
			if let error = error {

				NSLog("Notification Error: \(error)")
			}
			else{
				
			}
		}
		
		
	}
}
