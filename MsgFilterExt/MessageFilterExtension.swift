//
//  MessageFilterExtension.swift
//  MsgFilterExt
//
//  Created by vincent_kim on 2020/10/04.
//  Copyright © 2020 remain. All rights reserved.
//

import IdentityLookup
//import UserNotifications
//import Alamofire


final class MessageFilterExtension: ILMessageFilterExtension {

	
	var words:[String] = []
    var trustSenders:[String] = ["821034747123"]
	var trustNames:[String] = []
//	var contacts:[UserData] = [UserData(phoneNumber: "821050666129", name: "UserName")]
    var junk_senders:[String] = []
	var expectSender:String = ""
	var expectName:String = ""
	
	
	func loadItems() {
		
		// Group으로 설정하여 UserDefaults 을 이용하여 데이터를 통신합니다.
		let userDefaults = UserDefaults(suiteName: "group.com.remain.MsgFilterExt")
		
//            var i = 0
		guard let json = userDefaults?.object(forKey: "data") as? String else {	return }
		
		let data = json.data(using: .utf8)!
		do {
			if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [Dictionary<String,Any>]
			{
				
				for item in jsonArray{
					let phoneNumber = item["phoneNumber"] as! String
					let name = item["name"] as! String
					self.trustNames.append(name)
					self.trustSenders.append(phoneNumber)
//					let contact = UserData(phoneNumber: phoneNumber, name: name)
//					self.contacts.append(contact)
					
					
				}
				

			} else {
				NSLog("badJson")
				
				
			}
		} catch let error as NSError {
			NSLog("\(error)")
			
			
		}
	
	}
	
	func checkIsOnList(messageSender:String){
		for (idx, sender) in self.trustSenders.enumerated(){
			if messageSender.contains(sender.lowercased()){
				
				self.expectSender = sender
				self.expectName = self.trustNames[idx]
//				self.sendNotification(phoneNumber: sender, name: self.trustNames[idx])
//				self.sendNotification(userData: sender)//phone: sender.phoneNumber, name: sender.name)
			}
			
			
		}
	}
	
	func sendNotification(phoneNumber:String, name:String){//phone: String, name: String){

//		let userNotificationCenter = UNUserNotificationCenter.current()
//		let notificationContent = UNMutableNotificationContent()
//
//		notificationContent.title = "투넘버문자"
//		notificationContent.body = "\(name) 으로부터 문자가 수신되었습니다.\n해당 연락처 상세 정보로 이동합니다"
//		notificationContent.sound = UNNotificationSound.default
//		notificationContent.threadIdentifier = "F39-C521-A7A"
//
//		let userData:[String:String] = ["phoneNumber":phoneNumber,
//										"name":name
//										]
//		notificationContent.userInfo = ["userInfo": userData]
//
//
//		let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
//		let nowDouble = NSDate().timeIntervalSince1970
//		let now = Int(nowDouble*1000)
//		let request = UNNotificationRequest(identifier: "\(now)",
//											content: notificationContent,
//											trigger: trigger)
//		
//		userNotificationCenter.removeAllPendingNotificationRequests()
//		userNotificationCenter.add(request) { error in
//			if let error = error {
//
//				NSLog("Notification Error: \(error)")
//			}
//		}

//		let triggerDaily = Calendar.current.dateComponents([.hour,.minute,.second,], from: NSDate() as Date)
//
//		let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDaily, repeats: true)
//
//		let request = UNNotificationRequest(identifier: "\(now)", content: notificationContent, trigger: trigger)


//        let url = "https://httpbin.org/get"
//        AF.request(url,
//                   method: .get,
//                   parameters: nil,
//                   headers: nil)
//            .validate()
//            .responseJSON(completionHandler: { responseData in
//                switch responseData.result{
//                case .success(let data):
//                    if let jsonObject = data as? [String:Any]{
//                        print(jsonObject)
//                    }
//                    let userNotificationCenter = UNUserNotificationCenter.current()
//                    let notificationContent = UNMutableNotificationContent()
//
//                    notificationContent.title = "투넘버문자"
//                    notificationContent.body = "adsf 으로부터 문자가 수신되었습니다.\n해당 연락처 상세 정보로 이동합니다"
//                    notificationContent.sound = UNNotificationSound.default
//                    notificationContent.threadIdentifier = "F39-C521-A7A"
//
//                    let userData:[String:String] = ["phoneNumber":"821034747123",
//                                                    "name":"asdf"
//                                                    ]
//                    notificationContent.userInfo = ["userInfo": userData]
//
//
//                    let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
//                    let nowDouble = NSDate().timeIntervalSince1970
//                    let now = Int(nowDouble*1000)
//                    let request = UNNotificationRequest(identifier: "\(now)",
//                                                        content: notificationContent,
//                                                        trigger: trigger)
//
//            //        let triggerDaily = Calendar.current.dateComponents([.hour,.minute,.second,], from: NSDate() as Date)
//            //
//            //        let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDaily, repeats: true)
//            //
//            //        let request = UNNotificationRequest(identifier: "\(now)", content: notificationContent, trigger: trigger)
//
//                    userNotificationCenter.removeAllPendingNotificationRequests()
//                    userNotificationCenter.add(request) { error in
//                        if let error = error {
//
//                            NSLog("Notification Error: \(error)")
//                        }
//                    }
//                case .failure(let err):
//                    print(err.localizedDescription)
//                }

//            })

		
	}
}

extension MessageFilterExtension: ILMessageFilterQueryHandling {

    func handle(_ queryRequest: ILMessageFilterQueryRequest, context: ILMessageFilterExtensionContext, completion: @escaping (ILMessageFilterQueryResponse) -> Void) {
        // First, check whether to filter using offline data (if possible).
        let offlineAction = self.offlineAction(for: queryRequest)

        switch offlineAction {
        case .allow, .filter:
            // Based on offline data, we know this message should either be Allowed or Filtered. Send response immediately.
            let response = ILMessageFilterQueryResponse()
            response.action = offlineAction

            completion(response)

        case .none:
            // Based on offline data, we do not know whether this message should be Allowed or Filtered. Defer to network.
            // Note: Deferring requests to network requires the extension target's Info.plist to contain a key with a URL to use. See documentation for details.
            context.deferQueryRequestToNetwork() { (networkResponse, error) in
				self.sendNotification(phoneNumber: self.expectSender, name: self.expectName)
                let response = ILMessageFilterQueryResponse()
                response.action = .none

                if let networkResponse = networkResponse {
                    // If we received a network response, parse it to determine an action to return in our response.
					DispatchQueue.main.async {
						response.action = self.action(for: networkResponse)
					}
                    
                } else {
                    NSLog("Error deferring query request to network: \(String(describing: error))")
                }

                completion(response)
            }

//        case .junk:
//            break
//        case .promotion:
//            break
//        case .transaction:
//            break
        default:
            break
//        @unknown default:
//            break
        }
    }

    private func offlineAction(for queryRequest: ILMessageFilterQueryRequest) -> ILMessageFilterAction {
        // Replace with logic to perform offline check whether to filter first (if possible).
//		DispatchQueue.main.async { [weak self] in
//			guard let self = self else { return }
//
//		}
		
		guard let messageSender = queryRequest.sender else {
			return .none
		}
		self.loadItems()
		self.checkIsOnList(messageSender: messageSender)
//        guard let messageBody = queryRequest.messageBody else {
//            return .none
//        }
		
		// check for sender
//		for (idx, sender) in self.trustSenders.enumerated(){
//			if messageSender.contains(sender.lowercased()){
////				self.sendNotification(phoneNumber: sender, name: self.trustNames[idx])
////				self.sendNotification(userData: sender)//phone: sender.phoneNumber, name: sender.name)
//			}
//
//
//		}
//        for sender in trustSenders {
//            if messageSender.contains(sender.lowercased()) {
//				print(sender)
//
//
//
//                return .none
//            }
//        }
//
//        for sender in junk_senders {
//            if messageSender.contains(sender.lowercased()) {
//                return .filter
//            }
//        }
//
//        // check for keywords
//        for word in self.words {
//            if messageBody.contains(word.lowercased()) {
//                return .filter
//            }
//        }
		
		
	
		
		return .none
		
        
    }

    private func action(for networkResponse: ILNetworkResponse) -> ILMessageFilterAction {
        // Replace with logic to parse the HTTP response and data payload of `networkResponse` to return an action.
        return .none
    }

}

