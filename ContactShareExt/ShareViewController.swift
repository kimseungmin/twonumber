//
//  ShareViewController.swift
//  ContactShareExt
//
//  Created by vincent_kim on 27/09/2019.
//  Copyright © 2019 remain. All rights reserved.
//

import UIKit
import Social

class ShareViewController: UIViewController {

   let suiteName = "group.com.remain.ShareExt"
   let keyName = "shareData"
   let contactType = "public.vcard"
   
   override func viewDidLoad() {
       super.viewDidLoad()
    
     
     let extensionItem: NSExtensionItem = self.extensionContext?.inputItems.first as! NSExtensionItem
     guard let itemProvider = extensionItem.attachments?.first else {
         self.extensionContext!.completeRequest(returningItems: [], completionHandler: nil)
         return
     }
     
     if itemProvider.hasItemConformingToTypeIdentifier(contactType) == false{
         self.extensionContext!.completeRequest(returningItems: [], completionHandler: nil)
         
     }else{
         itemProvider.loadItem(forTypeIdentifier: contactType, options: nil, completionHandler: { [weak self] (data, error) in
             guard let self = self else {
                 return
             }
             // 후처리
             defer {

                self.extensionContext!.completeRequest(returningItems: nil, completionHandler: nil)
//                self.extensionContext!.cancelRequest(withError:NSError())
                
                
             }

             if let data = data as? NSData {
                 do {
                    self.redirectToHostApp(data)
                    
                 }

                
             }
             
         })
     }
       
   }
   
   override func viewWillAppear(_ animated: Bool) {
       super.viewWillAppear(animated)
       
       
   }
    
    private func redirectToHostApp(_ data: NSData) {
        var name: String = ""
        let nameTag = "\nFN:"
        
        /// data
        guard let contactData = String(data: data as Data, encoding: String.Encoding.utf8) else { return }
        let nameArray = contactData.components(separatedBy: ";")
        
        /// 이름
        let filteredNameArray = nameArray.filter({
            $0.range(of: nameTag, options: .caseInsensitive) != nil
        })
        
        if !filteredNameArray.isEmpty {
            if let firstItem = filteredNameArray.first {
                let subNameArray = firstItem.components(separatedBy: "\r")
                if let resultName = subNameArray.filter({
                    $0.range(of: nameTag, options: .caseInsensitive) != nil
                }).first {
                    let nameData = resultName.replacingOccurrences(of: nameTag, with: "")
                    name = nameData
                }
                
                
            }
            
        }
        
        /// 연락처
        var tel = ""
        let contactArray = contactData.components(separatedBy: "\n")
        if let filteredContact = contactArray.filter({
            $0.range(of: "TEL;type", options: .caseInsensitive) != nil
        }).first {
            let digitSet = CharacterSet.decimalDigits
            
            
            for (_, char) in filteredContact.unicodeScalars.enumerated() {
                if digitSet.contains(char) {
//                            print("\(index), \(char),  숫자입니다")
                    tel = "\(tel)\(char)"
                }
            }
        }
        
        
        
        let scheme = "twonumsender://"
        let host = "regist" + "?"
        let schemeName = "name=\(name)" + "&"
        let schemePhone = "tel=\(tel)"
        guard let fullURLString = "\(scheme)\(host)\(schemeName)\(schemePhone)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
        guard let url = URL(string: fullURLString) else { return }
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.extensionContext?.completeRequest(returningItems: nil, completionHandler: nil)
        }
                
        var responder = self as UIResponder?
        let selectorOpenURL = sel_registerName("openURL:")


        while (responder != nil) {
            if (responder?.responds(to: selectorOpenURL))! {
                let _ = responder?.perform(selectorOpenURL, with: url)
                
            }
            responder = responder!.next
        }

        
        
        
    }
}
