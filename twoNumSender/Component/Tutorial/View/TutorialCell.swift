//
//  TutorialCell.swift
//  twoNumSender
//
//  Created by seungmin kim on 27/05/2019.
//  Copyright © 2019 remain. All rights reserved.
//

import UIKit

class TutorialCell: UICollectionViewCell {

    @IBOutlet weak var resetbtn: UIButton!
    @IBOutlet weak var backImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        #if DEBUG
        self.resetbtn.isEnabled = true
        self.resetbtn.isHidden = false
        #else
        self.resetbtn.isEnabled = false
        self.resetbtn.isHidden = true
        #endif
        
    }

}

