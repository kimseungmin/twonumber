//
//  TutorialCollectionVC.swift
//  twoNumSender
//
//  Created by seungmin kim on 27/05/2019.
//  Copyright © 2019 remain. All rights reserved.
//

import UIKit
import SnapKit

private let reuseIdentifier = "Cell"

enum TutorialStatus: String {
    case tutorial = "사용자 설명서"
    case callKit  = "발신자 표시 설정방법"
    case premium  = "프리미엄 상품정보"
}

class TutorialCollectionVC: UICollectionViewController {
    
    let pageControl: UIPageControl = {
        let rect = CGRect(origin: .zero, size: CGSize(width: 200, height: 50))
        let pc = UIPageControl(frame: rect)
        pc.currentPage = 0
        pc.pageIndicatorTintColor = .gray
        pc.currentPageIndicatorTintColor = .blue
        pc.isUserInteractionEnabled = false
        pc.hidesForSinglePage = true
        return pc
    }()
    
    var tutorialType:TutorialStatus = .tutorial
    
    var tutoImageUrls: [String] = ["tuto1", "tuto2", "tuto3", "tuto4"]
    var callKitImageUrls: [String] = ["callkit0", "callkit1", "callkit2", "callkit3", "callkit4", "premium2"]
    var premiumImageUrls: [String] = ["premium0", "premium1", "premium2", "premium3"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.drawNavigationBar()
        self.initCollectionViewSetting()
        self.registerCell()
        self.createPageControl()
    }
    
    deinit {
        Debug.print("")
    }
}

//MARK: UICollectionViewDataSource
extension TutorialCollectionVC {
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch self.tutorialType {
        case .tutorial:
            return self.tutoImageUrls.count
        case .callKit:
            return self.callKitImageUrls.count
        case .premium:
            return self.premiumImageUrls.count
        }
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String.init(describing: TutorialCell.self), for: indexPath) as! TutorialCell
        
        switch self.tutorialType {
        case .tutorial:
            cell.backImageView.image = UIImage.init(named: self.tutoImageUrls[indexPath.row])
        case .callKit:
            cell.backImageView.image = UIImage.init(named: self.callKitImageUrls[indexPath.row])
        case .premium:
            cell.backImageView.image = UIImage.init(named: self.premiumImageUrls[indexPath.row])
        }

        #if DEBUG
        cell.resetbtn.addTarget(self, action: #selector(actionReset), for: .touchUpInside)
        cell.resetbtn.isHidden = false
        #else
        cell.resetbtn.isHidden = true
        #endif
        
        return cell
    }
}

// UICollectionViewDelegate
extension TutorialCollectionVC {
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let page = indexPath.row
        self.pageControl.currentPage = page
        
        self.drawNavigationBar()
    }
}

// MARK: UICollectionViewDelegateFlowLayout
extension TutorialCollectionVC: UICollectionViewDelegateFlowLayout {
    
    // item size
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let availableWidth = collectionView.frame.width
        let availableHeight = collectionView.frame.height
        
        return CGSize(width: availableWidth, height: availableHeight)
    }
    
    // collection view padding
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    // cell 간격
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}

// MARK: private
extension TutorialCollectionVC {
    // navigation bar
    private func drawNavigationBar() {

        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithTransparentBackground()
            appearance.backgroundColor = UIColor.init(hexString: ColorInfo.MAIN)
            appearance.shadowColor = .clear
            appearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            self.navigationController?.navigationBar.tintColor = .white
            self.navigationController?.navigationBar.standardAppearance = appearance
            self.navigationController?.navigationBar.scrollEdgeAppearance = appearance
            self.navigationController?.navigationBar.compactAppearance = appearance
        }
        
        self.navigationController?.navigationBar.isTranslucent = false          // 투명도 설정
		self.navigationController?.navigationBar.barTintColor = UIColor.init(hexString: ColorInfo.MAIN)
        
        self.navigationController?.navigationBar.shadowImage = UIImage()        // 네비 하단바 삭제 ?
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.backgroundColor = UIColor.init(hexString: ColorInfo.MAIN)
        self.title = self.tutorialType.rawValue
        
        // set right bar - 메인화면으로 이동
        let rightBarImage = UIImage(named: "icon_navbar_close_sub")?.withRenderingMode(UIImage.RenderingMode.automatic)
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        
        
        let rightBarButtonItem =  UIBarButtonItem(image: rightBarImage,
                                                  style: UIBarButtonItem.Style.done,
                                                  target: self,
                                                  action: #selector(actionPop))
        
        
        if UserDefaults.standard.bool(forKey: .launchedBefore) == true{
            
            rightBarButtonItem.action = #selector(actionPop)
        }else{
            
            rightBarButtonItem.action = #selector(actionStart)
        }
        
        self.navigationItem.rightBarButtonItem = rightBarButtonItem

        
    }
    
    // init colleciton view
    private func initCollectionViewSetting() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        self.collectionView.collectionViewLayout = layout
        
        self.collectionView.isPagingEnabled = true
        self.collectionView.isScrollEnabled = true
    }
    
    // register Cell
    private func registerCell() {
        
        self.collectionView!.register(UINib.init(nibName: String.init(describing: TutorialCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: TutorialCell.self))
    }
    
    private func createPageControl() {
        
        self.view.addSubview(self.pageControl)
        
        self.pageControl.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.view)
            make.bottom.equalTo(self.view).offset(-20)
        }
        
        switch self.tutorialType {
        case .tutorial:
            self.pageControl.numberOfPages = self.tutoImageUrls.count
        case .callKit:
            self.pageControl.numberOfPages = self.callKitImageUrls.count
        case .premium:
            self.pageControl.numberOfPages = self.premiumImageUrls.count
        
        }
        
    }
}

// MARK: @objc
extension TutorialCollectionVC {
    
    @objc func actionPop() {
        //        self.navigationController?.popViewController(animated: true)
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @objc func actionStart(){
        UserDefaults.standard.set(true, forKey: .launchedBefore)
        UserDefaults.standard.synchronize()
//        let vc = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "rootViewController")
//        vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve// 페이지 전환 애니메이션
//
//        self.present(vc, animated: true, completion: nil)
        
        
//        let mainTabBarVC = MainTabBarVC()
//        let navigationController = UINavigationController(rootViewController: mainTabBarVC)
//        navigationController.modalPresentationStyle = .fullScreen
//        self.present(navigationController, animated: true, completion: nil)
        
		RouterManager.shared.dispatcher(.main)
        
        

    }
    
    @objc func actionReset(){
        UserDefaults.standard.set(true, forKey: .launchedBefore)
        UserDefaults.standard.set(false, forKey: .localSync)
    }
}
