//
//  RegistContactsTVCell.swift
//  twoNumSender
//
//  Created by seungmin kim on 08/06/2019.
//  Copyright © 2019 remain. All rights reserved.
//

import UIKit

protocol RegisterTVDelegate: UITextViewDelegate {
    
    func closeKeyboard(memo: String)
	func textViewDidChange(text: String)
}

class RegistContactsTVCell: UITableViewCell {
	
    
    @IBOutlet weak var regContactsTitleLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
	@IBOutlet weak var dividerView: UIView!
	weak var delegate: RegisterTVDelegate?
    
	var content = ""{
		willSet{
			self.textView.text = newValue
			self.setPlaceHolder()
			
		}
	}
	var isTextViewFocused:Bool = false{
		willSet{
			if newValue{
				self.dividerView.backgroundColor =  UIColor(hexString: ColorInfo.MAIN)
			}
			else{
				self.dividerView.backgroundColor = .lightGray
			}
		}
	}
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		self.backgroundColor = .clear
		self.textView.delegate = self
		self.textView.isScrollEnabled = false
		self.textView.placeholder = ("이 곳에 메모를 입력하세요", .left)
		self.setPlaceHolder()
    }
	
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		//MARK: Auto Height
		var textViewString: String{
			get{
				return self.textView.text
			}
			set{
				self.textView.text = newValue
				textViewDidChange(textView)

			}
		}
	}

	override func draw(_ rect: CGRect) {
		super.draw(rect)
		
	}
	override func prepareForReuse() {
		Debug.print("")
	}
    
	deinit {
		Debug.print("")
	}


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
	func setPlaceHolder(){
		//MARK: place holder dimiss
		if let placeholderLabel = self.viewWithTag(100) as? UILabel {
			placeholderLabel.isHidden = self.textView.text.count > 0
			placeholderLabel.isHidden = self.textView.attributedText.length > 0
		}
	}
    
}

extension RegistContactsTVCell{
    class func cellHeight() -> CGFloat {
        return 180
    }
    
    @objc func actionClose() {
        self.delegate?.closeKeyboard(memo: self.textView.text)
        self.textView.resignFirstResponder()
    }
}


extension RegistContactsTVCell: UITextViewDelegate{
	func textViewDidBeginEditing(_ textView: UITextView) {
		if !self.textView.isFirstResponder{
			return
		}
		self.isTextViewFocused = true
	}
	
	func textViewDidEndEditing(_ textView: UITextView) {
		
		self.isTextViewFocused = false
	}
	func textViewDidChange(_ textView: UITextView) {
		self.setPlaceHolder()
		
		if !self.textView.isFirstResponder{
			return
		}
		self.delegate?.textViewDidChange(text: textView.text)
		
		let size = textView.bounds.size
		let newSize = textView.sizeThatFits(CGSize(width: size.width, height: CGFloat.greatestFiniteMagnitude))
		
		//MARK: Resize the cell only when cell's size is changed
		if size.height != newSize.height {
			UIView.setAnimationsEnabled(false)
			tableView?.beginUpdates()
			tableView?.endUpdates()
			UIView.setAnimationsEnabled(true)
			
		}
		
	}
}
