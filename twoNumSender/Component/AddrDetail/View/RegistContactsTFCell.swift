//
//  RegistContactsTFCell.swift
//  twoNumSender
//
//  Created by seungmin kim on 08/06/2019.
//  Copyright © 2019 remain. All rights reserved.
//

import UIKit
enum CellType {
	case phone
	case name
}
protocol RegistContactsTFCellDelegate:AnyObject {
	func phoneTFChanged(text:String)
	func nameTFChanged(text:String)
}
class RegistContactsTFCell: UITableViewCell {

    @IBOutlet weak var regContactsTitleLabel: UILabel!
    @IBOutlet weak var regContactsTF: UITextField!
    
	weak var delegate: RegistContactsTFCellDelegate?
	var cellType:CellType = .name
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		self.regContactsTF.delegate = self
		
		self.backgroundColor = .clear
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
	
	deinit {
		Debug.print("")
	}
	

}

extension RegistContactsTFCell{
    class func cellHeight() -> CGFloat {
        return 112
    }
}


extension RegistContactsTFCell: UITextFieldDelegate{
	@IBAction func textFieldEditingChanged(_ textField: UITextField) {
		if !self.regContactsTF.isFirstResponder{
			return
		}
		if self.cellType == .name{
			self.delegate?.nameTFChanged(text: self.regContactsTF.text ?? "")
		}
		else{
			self.delegate?.phoneTFChanged(text: self.regContactsTF.text ?? "")
		}
    }
	
//	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//		// figure out what the new string will be after the pending edit
//		let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
//
//		// Do whatever you want here
//
//
//
//		// Return true so that the change happens
//		return true
//	}
	
}
