//
//  RegistContactButtonCell.swift
//  twoNumSender
//
//  Created by seungmin kim on 08/06/2019.
//  Copyright © 2019 remain. All rights reserved.
//

import UIKit

class RegistContactButtonCell: UITableViewCell {

    
    @IBOutlet weak var registContactImageView: UIImageView!
    @IBOutlet weak var registContactBtnTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
		self.backgroundColor = .clear
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension RegistContactButtonCell{
    class func cellHeight() -> CGFloat {
        return 50
    }
}
