//
//  AddrDetailVC.swift
//  twoNumSender
//
//  Created by vincent_kim on 2020/10/09.
//  Copyright © 2020 remain. All rights reserved.
//

import UIKit
import CoreData
import Contacts
import ContactsUI
import Alamofire
import SwiftyJSON
import SVProgressHUD
import SafariServices
import MessageUI

enum KeyBoardStatus{
	case up
	case down
}
enum UpdateCellTypes: String{
	case name         = "Name (이름)"
	case phone        = "Contact (연락처)"
	case memo         = "Note (메모)"
	case recieveBtn   = "수신 문자 확인"
	case sendBtn      = "문자 발신"
	case callBtn     = "전화걸기"
	case contactsBtn  = "연락처에서 가져오기"
	case clipboardBtn = "클립보드에서 붙여넣기"
	case delete       = "연락처 삭제"
	
}

enum UpdateValue{
	case name
	case phone
	case memo
	case contactsBtn
	case clipboardBtn
	case delete
	
}

class AddrDetailVC: UIViewController {
	var isDeepLink:Bool = false
	var originY:CGFloat?
	
	var cellTypeArray: [UpdateCellTypes] = []
	
	var mobileType = UserDefaults.standard
	var isRegist:Bool = false
	var model : ContactRealmModel? = nil

	var expectModel: ContactModel = ContactModel(seq: 0, name: "", phone: "", memo: "")
	
	/* 연락처 접근 */
	var updateContact = CNContact()
	var contactStore = CNContactStore()
	/* 연락처 접근 */
	
	@IBOutlet weak var navView: ClearNavView!
	@IBOutlet weak var tableView: UITableView!
	
	
	override func viewDidLoad() {
        super.viewDidLoad()
		
		self.drawNavigationBar()
		self.setUI()
		self.setTableView()
		self.registerNotification()
		self.setArray()
		
    }
	
	override func viewWillAppear(_ animated: Bool) {
		self.navigationController?.setNavigationBarHidden(true, animated: true)
	}
	override func viewWillDisappear(_ animated: Bool) {
//		self.navigationController?.setNavigationBarHidden(false, animated: false)
	}
	
	deinit {
		Debug.print("")
	}


}

// MARK: - UI Setting
extension AddrDetailVC{
	
	
	private func setUI(){
		
		self.navView.backgroundColor = .clear
	}
	
	private func setArray(){
		if self.isRegist{
			self.cellTypeArray = [.name, .phone, .memo, .contactsBtn, .clipboardBtn]
		}else{
			self.cellTypeArray = [.name, .phone, .memo, .recieveBtn, .sendBtn, .callBtn, .contactsBtn, .clipboardBtn, .delete]
		}
		
		if self.isDeepLink{
			let removeArrays:[UpdateCellTypes] = [.memo, .contactsBtn, .clipboardBtn, .delete]
			for item in removeArrays{
				if let index = self.cellTypeArray.firstIndex(of: item) {
					self.cellTypeArray.remove(at: index)
				}
			}
			
		}
		
		self.tableView.reloadSections([0], with: .automatic)
		
	}
	
}

// MARK: - UITextFieldDelegate,UITextViewDelegate, SFSafariViewControllerDelegate
extension AddrDetailVC: UITextFieldDelegate,UITextViewDelegate, SFSafariViewControllerDelegate {
	/* 화면 외부 누르면 키보드 숨김 */
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		self.view.endEditing(true)
	}
	
//	override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
//		self.view.endEditing(true)
//	}
	
	
}

//MARK: - Tableview Setting
extension AddrDetailVC: UITableViewDelegate, UITableViewDataSource{

	
	
	private func setTableView(){
		self.tableView.delegate             = self
        self.tableView.dataSource           = self
        self.tableView.alwaysBounceVertical = false
        self.tableView.separatorStyle       = .none
        self.tableView.backgroundColor      = .clear
		
		// 테이블뷰 버튼 터치 딜레이 제거
		self.tableView.delaysContentTouches = false
		for case let subview as UIScrollView in self.tableView.subviews {
			subview.delaysContentTouches = false
		}
//		self.tableView.tableFooterView = UIView()
		
//		if #available(iOS 11.0, *){
//            self.tableView.rowHeight          = UITableView.automaticDimension
//            self.tableView.estimatedRowHeight = UITableView.automaticDimension
//		}else{
//            self.tableView.rowHeight          = UITableView.automaticDimension
//            self.tableView.estimatedRowHeight = 180
//		}
		self.registerCell()
	}
	
	private func registerCell() {
		self.tableView.register(UINib(nibName: String.init(describing: RegistContactsTFCell.self), bundle: nil), forCellReuseIdentifier: String.init(describing: RegistContactsTFCell.self))
		
		self.tableView.register(UINib(nibName: String.init(describing: RegistContactsTVCell.self), bundle: nil), forCellReuseIdentifier: String.init(describing: RegistContactsTVCell.self))
		
		self.tableView.register(UINib(nibName: String.init(describing: RegistContactButtonCell.self), bundle: nil), forCellReuseIdentifier: String.init(describing: RegistContactButtonCell.self))
	}
	
	func numberOfSections(in tableView: UITableView) -> Int {
        return 1
		
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.cellTypeArray.count
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		
		let row = self.cellTypeArray[indexPath.row]
		switch row {
		case .name, .phone:
			return RegistContactsTFCell.cellHeight()
		case .memo:
			return UITableView.automaticDimension
		case .contactsBtn, .clipboardBtn, .delete, .recieveBtn, .sendBtn, .callBtn:
			return RegistContactButtonCell.cellHeight()
			
		}
	}
	
	func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
		return 180
	}
	
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell: UITableViewCell
		let row = self.cellTypeArray[indexPath.row]
		switch row {
		case .name:
			let tfCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: RegistContactsTFCell.self)) as! RegistContactsTFCell
			tfCell.regContactsTitleLabel.text = UpdateCellTypes.name.rawValue
			tfCell.regContactsTF.keyboardType = .default
			tfCell.regContactsTF.placeholder  = TextInfo.placeHolder_nameTF
			tfCell.selectionStyle             = .none
//            tfCell.regContactsTF.text         = self.name
			tfCell.regContactsTF.text         = self.expectModel.name
			tfCell.cellType = .name
			tfCell.delegate = self
			
			cell = tfCell
			
		case .phone:
			let tfCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: RegistContactsTFCell.self)) as! RegistContactsTFCell
			tfCell.regContactsTitleLabel.text = UpdateCellTypes.phone.rawValue
			tfCell.regContactsTF.keyboardType = .phonePad
			tfCell.regContactsTF.placeholder  = TextInfo.placeHolder_phoneTF
//            tfCell.regContactsTF.text         = self.phone
			tfCell.regContactsTF.text         = self.expectModel.phone
			tfCell.cellType = .phone
			tfCell.selectionStyle             = .none
			tfCell.delegate = self
			
			cell = tfCell
			
		case .memo:
			let tvCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: RegistContactsTVCell.self)) as! RegistContactsTVCell
            tvCell.regContactsTitleLabel.text = UpdateCellTypes.memo.rawValue
//            tvCell.regContactsTextView.text   = self.memo
			tvCell.content = self.expectModel.memo
//            tvCell.textView.text              = self.expectModel.memo
            tvCell.selectionStyle             = .none
            tvCell.delegate                   = self
			
			cell = tvCell
			
		case .contactsBtn:
			let btnCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: RegistContactButtonCell.self)) as! RegistContactButtonCell
			btnCell.registContactBtnTitle.text = UpdateCellTypes.contactsBtn.rawValue
			btnCell.registContactImageView.image = UIImage(named: "img_profile")
			btnCell.selectionStyle             = .default
			btnCell.registContactBtnTitle.textColor = UIColor.black
			cell = btnCell
			
		case .clipboardBtn:
			let btnCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: RegistContactButtonCell.self)) as! RegistContactButtonCell
			btnCell.registContactBtnTitle.text = UpdateCellTypes.clipboardBtn.rawValue
			btnCell.registContactImageView.image = UIImage(named: "img_clipboard")
			btnCell.selectionStyle             = .default
			btnCell.registContactBtnTitle.textColor = UIColor.black
			cell = btnCell
			
			
		case .delete:
			let btnCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: RegistContactButtonCell.self)) as! RegistContactButtonCell
			btnCell.registContactBtnTitle.text = UpdateCellTypes.delete.rawValue
			btnCell.registContactImageView.image = UIImage(named: "img_delete")
			btnCell.selectionStyle             = .default
			btnCell.registContactBtnTitle.textColor = UIColor.red
			
			cell = btnCell
			
		case .recieveBtn:
			let btnCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: RegistContactButtonCell.self)) as! RegistContactButtonCell
			btnCell.registContactBtnTitle.text = UpdateCellTypes.recieveBtn.rawValue
			btnCell.registContactImageView.image = UIImage(named: "img_recieve")
			btnCell.selectionStyle             = .default
			btnCell.registContactBtnTitle.textColor = UIColor.black
			cell = btnCell
		case .sendBtn:
			
			let btnCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: RegistContactButtonCell.self)) as! RegistContactButtonCell
			btnCell.registContactBtnTitle.text = UpdateCellTypes.sendBtn.rawValue
			btnCell.registContactImageView.image = UIImage(named: "img_send")
			btnCell.selectionStyle             = .default
			btnCell.registContactBtnTitle.textColor = UIColor.black
			cell = btnCell
		case .callBtn:
			let btnCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: RegistContactButtonCell.self)) as! RegistContactButtonCell
			btnCell.registContactBtnTitle.text = UpdateCellTypes.callBtn.rawValue
			btnCell.registContactImageView.image = UIImage(named: "img_call")
			btnCell.selectionStyle             = .default
			btnCell.registContactBtnTitle.textColor = UIColor.black
			cell = btnCell
		}
		
		return cell
	}
	

	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: false)
		let row = self.cellTypeArray[indexPath.row]
        switch row {
            case .name:
                break
            case .phone:
                break
            case .memo:
                break
            case .contactsBtn:
                self.presentContactController()
                
            case .clipboardBtn:
                let pasteBoard = UIPasteboard.general
                /* 복사 문자열 체크 */
                //            let copyText = pasteBoard.string
                guard let copyText = pasteBoard.string else { return }
                if copyText.count < 20{
                    self.expectModel.phone = "\(copyText)"
                    self.tableView.reloadData()
                }
                
                /* 복사 문자열 체크 */
                
            case .delete:
                self.deleteBtnTapped()
            case .recieveBtn:
                self.recieveToMsg()
            case .sendBtn:
                self.sendMsg()
            case .callBtn:
                self.callnumber()
        }
        
		
	}
	
	
}


//MARK: - 네비게이션 컨트롤러 제스쳐 추가 및 네비게이션 세팅
extension AddrDetailVC: UINavigationControllerDelegate, UIGestureRecognizerDelegate, ClearNavViewDelegate{
	
	func setTitleText() -> String {
		if self.isRegist{
			return TextInfo.title_regist
		}
		else{
			return TextInfo.title_update
		}
	}
	
	func setRightBtnAction() {
		self.saveButtonTapped()
		Debug.print("")
	}
	
	
	func setRightBtnIsHidden() -> Bool {
		return false
	}
	
	private func drawNavigationBar() {
		self.navigationController?.setNavigationBarHidden(true, animated: true)
		
		self.navigationController?.interactivePopGestureRecognizer!.delegate = self
		self.navigationController?.interactivePopGestureRecognizer!.isEnabled = true
		
		
		self.navView.delegate = self
		self.navView.setTitleText()
	}

	func setBackBtnAction() {
		self.actionPop()
	}
	
	func setBackBtnIsHidden() -> Bool {
		return false
	}
	
}

// MARK: @objc
extension AddrDetailVC {
	
	@objc func actionPop() {
		//        self.navigationController?.popViewController(animated: true)
		self.setUpdateNotification()
		
		if self.isModal == true{
			self.dismiss(animated: true, completion: nil)
		}else{
			self.navigationController?.popViewController(animated: true)
		}

		
	}
	@objc func setUpdateNotification(){
		let userInfo: [String: String] = ["refresh":"refresh"]
		NotificationCenter.default.post(name: Notification.Name.refreshAddrList, object: nil, userInfo: userInfo)
		
	}
	
	func deleteBtnTapped(){
		let title = TextInfo.alert_delete_contacts_title
		let msg = TextInfo.alert_delete_contacts
    
		let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
		
		alert.addAction(UIAlertAction(title: "삭제", style: .destructive) { action in
			
			/* 연락처 삭제 */
			guard let model = self.model else { return }
			RealmManager.shared.deleteContact(seq: model.seq, completion: { (success) in
				self.actionPop()
				
				guard let purchase = IAPManager.shared.getPurchaseStatus() else { return }
				if purchase{
					Toast.makeToast(message: "동기화가 시작되었습니다\n완료 시 푸시알람이 발송됩니다", controller: self)
				}
                CallKitManager.shared.saveCallKitData(success: nil, failure: nil)
			})
			
			/* 연락처 삭제 */
			
		})
		
		alert.addAction(UIAlertAction(title: "취소", style: .default, handler: nil))
		
		self.present(alert, animated: true, completion: nil)
	}
	
	@objc func saveButtonTapped(){
		
		self.view.endEditing(true)

		
		guard let defaultMotype = UserDefaults.standard.string(forKey: .mobileType) else { return }
		let moType:MobileType = MobileType(rawValue: defaultMotype) ?? MobileType.kt

		
		let expectModel = self.expectModel
		
		//MARK: check validation
		if expectModel.name == ""{
			let title = "이름 확인"
			let msg = "이름은 필수 항목입니다"
			let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "확인", style: .default, handler: nil))
			self.present(alert, animated: true, completion: nil)
			return
		}
		//MARK: check validation
		if expectModel.phone == ""{
			
			let title = "연락처 확인"
			let msg = "연락처는 필수 항목입니다"
			let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "확인", style: .default, handler: nil))
			self.present(alert, animated: true, completion: nil)
			return
		}
		
		
		var phone = expectModel.phone.replacingOccurrences(of: "-", with: "")
		
		if isRegist{
			let alert = UIAlertController(title: "저장 방법 선택", message: "전화번호 유형에 따라 변환 방법이 적용됩니다.", preferredStyle: .alert)
			alert.addAction(UIAlertAction(title: "휴대폰 번호 등록", style: .default) { action in
			
				phone = ContactsConvertManager.shared.registerNewContact(convertType: .phone, phone: phone, moType: moType)
				RealmManager.shared.createContatct(name: expectModel.name, phone: phone, memo: expectModel.memo, completion: { (success, mergedName, model) in
					if let model = model{
						
						self.showSuccessAlert(isSuccess: success, isMerged: mergedName, model: model)
					}
				})
			})
			alert.addAction(UIAlertAction(title: "일반 전화 번호 등록", style: .destructive) { action in
				phone = ContactsConvertManager.shared.registerNewContact(convertType: .tel, phone: phone, moType: moType)
				RealmManager.shared.createContatct(name: expectModel.name, phone: phone, memo: expectModel.memo, completion: { (success, mergedName, model) in
					if let model = model{
						
						self.showSuccessAlert(isSuccess: success, isMerged: mergedName, model: model)
					}
				})
			})
			alert.addAction(UIAlertAction(title: "취소", style: .cancel) { action in
				
			})
			
			self.present(alert, animated: true, completion: nil)
			
			
		}
		else{
			guard let currentModel = self.model else { return }
			
			RealmManager.shared.updateContact(seq: currentModel.seq, name: expectModel.name, phone: phone, memo: expectModel.memo, completion: { (success) in
				self.showSuccessAlert(isSuccess: success)
			})
		}
	}
	
	func showSuccessAlert(isSuccess:Bool, isMerged:String? = nil, model: ContactRealmModel? = nil){
		self.isRegist = false
		if let model = model{
			self.model = model
			self.expectModel = ContactModel(seq: model.seq, name: model.name, phone: model.phone, memo: model.memo)
		}
		
		self.drawNavigationBar()
		self.setArray()
		
		
		
		var msg = TextInfo.alert_update_success
		if isSuccess{
			if let mergedName = isMerged{
				msg = "중복된 연락처가 존재합니다.\n\(mergedName) 으로 통합되었습니다"
			}
		}else{
			msg = TextInfo.alert_update_fail
		}
		
		guard let purchase = IAPManager.shared.getPurchaseStatus() else { return }
		if purchase{
			msg = "프리미엄 상품에 따라 발신자 표시에 동기화 됩니다\n완료 시 푸시알람이 발송됩니다"
			
		}
		else{
			msg = "저장되었습니다.\n전화 수신 시 발신자 표시가 되는 상품을 확인하세요"
			
		}
        CallKitManager.shared.saveCallKitData(success: nil, failure: nil)
		

		let alert = UIAlertController(title: TextInfo.alert_title, message: msg, preferredStyle: .alert)
		
		if !purchase{
			alert.addAction(UIAlertAction(title: "상품설명", style: .default) { action in
				self.dismiss(animated: true, completion: nil)
				NotificationCenter.default.post(name: Notification.Name.premiumTuto, object: nil, userInfo: nil)
				
			})
		}
			
		alert.addAction(UIAlertAction(title: TextInfo.alert_confirm, style: .cancel) { action in
			self.setUpdateNotification()
		})
		
		self.present(alert, animated: true, completion: nil)
	}
	
	
	
	

}
// MARK: - CNContactPickerDelegate
extension AddrDetailVC: CNContactPickerDelegate {
    func presentContactController() {
        self.askForContactAccess(completion: { [weak self] (isEnable) in
            guard let self = self else { return }
            if isEnable {
                
                let contactPicker = CNContactPickerViewController()
                contactPicker.delegate = self
                contactPicker.modalPresentationStyle = .fullScreen
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else { return }
//                    self.navigationController?.setNavigationBarHidden(false, animated: true)
                    self.present(contactPicker, animated: true, completion: nil)
                }
            }
            
            
        })
    }
    /* 연락처 접근 */
    func askForContactAccess(completion: @escaping (Bool) -> Void) {
        let authorizationStatus = CNContactStore.authorizationStatus(for: CNEntityType.contacts)
        
        switch authorizationStatus {
        case .denied, .notDetermined:
            self.contactStore.requestAccess(for: CNEntityType.contacts, completionHandler: { (access, accessError) -> Void in
                if !access {
                    if authorizationStatus == CNAuthorizationStatus.denied {
                        DispatchQueue.main.async { [weak self] in
                            guard let self = self else { return }
                        
                            let message = "\(accessError!.localizedDescription)\n\n기능사용을 위해 설정에서 접근권한을 허용하세요."
                            let alertController = UIAlertController(title: "연락처", message: message, preferredStyle: UIAlertController.Style.alert)
                            
                            let dismissAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (action) -> Void in
                                completion(false)
                            }
                            
                            alertController.addAction(dismissAction)
                            
                            self.present(alertController, animated: true, completion: nil)
                        }
                    }
                }
            })
                
        default:
            completion(true)
        }
        
    }
    
    /* 연락처 접근 */
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        
        self.expectModel.name = contact.givenName + "" + contact.familyName
        
        if contact.phoneNumbers.count > 0 {
            self.expectModel.phone = ((contact.phoneNumbers[0].value).value(forKey: "digits") as? String)!
        }
        else {
            self.expectModel.phone = ""
        }
        
        self.tableView.reloadData()
        
    }
}

//MARK: - Notification
extension AddrDetailVC{
	private func registerNotification() {
		// event bus

	}
	
	private func unregisterNotification() {
		// event bus

	}
}



// MARK: - Alert
extension AddrDetailVC{
	func successAlert(){
		let title = "저장성공"
		let msg = "저장이 완료 되었습니다"
		let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
		
		alert.addAction(UIAlertAction(title: "닫기", style: .destructive) { action in
			self.dismiss(animated: true, completion: nil)
		})
		
		let userInfo: [String: String] = ["refresh":"refresh"]
		NotificationCenter.default.post(name: Notification.Name.refreshAddrList, object: nil, userInfo: userInfo)
		
		self.present(alert, animated: true, completion: nil)
	}
	
	/* 경고창 출력 */
	func displayAlertMessage(_ message: String, isAction: Bool) {
		let alertController = UIAlertController(title: TextInfo.alert_title, message: message, preferredStyle: UIAlertController.Style.alert)
		
		let dismissAction = UIAlertAction(title: TextInfo.alert_confirm, style: UIAlertAction.Style.default) { (action) -> Void in
			if isAction == true {
				//                self.navigationController?.popViewController(animated: true)
				self.dismiss(animated: true, completion: nil)
				return
			}
		}
		
		alertController.addAction(dismissAction)
		
		self.present(alertController, animated: true, completion: nil)
	}
	/* 경고창 출력 */
}

//MARK: - duplicate message Delegate
extension AddrDetailVC: MFMessageComposeViewControllerDelegate{
	func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
		self.dismiss(animated: true, completion: nil)
	}
	
	/* 수신 문자 목록 */
	@objc func recieveToMsg(){
		
		if MFMessageComposeViewController.canSendText(){
			
			let controller = MFMessageComposeViewController()
			let num = self.expectModel.phone
			
			
			var subString = ""
			let newString = num.replacingOccurrences(of: " ", with: "", options: .literal, range: nil)
			var finalNum = ""
			if (newString.range(of: "*77") != nil){
				//        if moType == "kt"{
				
				subString = newString.replacingOccurrences(of: "*77", with: "", options: .literal, range: nil)
				
				finalNum = subString
				
			}else if (newString.range(of: "*281") != nil){
				//        }else if moType == "skt"{
				subString = newString.replacingOccurrences(of: "*281", with: "", options: .literal, range: nil)
				
				finalNum = subString
			}else{
				subString = newString.replacingOccurrences(of: "*", with: "", options: .literal, range: nil)
				finalNum = subString
			}
			//            print(finalNum)
			
			controller.body = ""
			// 받는 사람 지정
			controller.recipients = ["\(finalNum)"]
			controller.messageComposeDelegate = self
			
			self.present(controller, animated: true, completion: nil)
		}
		
	}
	
	/* 수신 문자 목록 */
	
	/* 문자 발신*/
	@objc func sendMsg(){
		
		
		
		if MFMessageComposeViewController.canSendText(){
			
			let controller = MFMessageComposeViewController()
			
			controller.body = ""
			// 받는 사람 지정
			let sendNum = self.expectModel.phone
			
			if (sendNum.range(of: "*77") != nil){
				//        if moType == "kt"{
				controller.recipients = ["!\(sendNum)"]
				
				
			}else if (sendNum.range(of: "*281") != nil){
				//        }else if moType == "skt"{
				
				let convertNum = sendNum.replacingOccurrences(of: "*281 010", with: "*281 8210", options: .literal, range: nil)
				controller.recipients = ["\(convertNum)"]
			}else{
				let convertNum = sendNum.replacingOccurrences(of: "", with: "", options: .literal, range: nil)
				controller.recipients = ["\(convertNum)"]

			}
			
			controller.messageComposeDelegate = self
			//            controller.navigationBar.tintColor = UIColor(ColorInfo.MAIN)
			//            controller.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor(ColorInfo.MAIN)!]
			self.present(controller, animated: true, completion: nil)
			
		}
	}
	/* 문자 발신*/
	
	/*전화 걸기*/
	@objc func callnumber(){
		let phone = self.expectModel.phone

		var newString = phone.replacingOccurrences(of: " ", with: "", options: .literal, range: nil)
		//            let conString = newString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
		
//		UIPasteboard.general.string = "\(newString)"
		let title = "전화형식 선택"
		let alert = UIAlertController(title: title, message: nil, preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: "기존번호로 걸기", style: .default) { action in
            if phone.contains("*281"){
                newString = phone.replacingOccurrences(of: "*281", with: "")
            }
            else if phone.contains("*77"){
                newString = phone.replacingOccurrences(of: "*77", with: "")
            }
            else if phone.contains("*"){
                newString = phone.replacingOccurrences(of: "*", with: "")
            }
            else{
                newString = phone
            }
            
            CallActionManager.shared.actionCall(
                name: self.expectModel.name,
                number: newString,
                isOneNumber: true
            )
		})
		
		alert.addAction(UIAlertAction(title: "투넘버로 걸기", style: .default) { action in
            CallActionManager.shared.actionCall(
                name: self.expectModel.name,
                number: newString,
                isOneNumber: false
            )
            
		})
		alert.addAction(UIAlertAction(title: "취소", style: .cancel) { action in
			
		})
		self.present(alert, animated: true, completion: nil)
		
		
	}
	/*전화 걸기*/
	
		
}

// MARK: - Cell Delegate
extension AddrDetailVC: RegistContactsTFCellDelegate, RegisterTVDelegate{
	func closeKeyboard(memo: String) {
		self.expectModel.memo = memo
	}
	
	func textViewDidChange(text: String) {
		self.expectModel.memo = text
		
	}
	
	func nameTFChanged(text: String) {
		Debug.print(text)
		self.expectModel.name = text

	}
	
	func phoneTFChanged(text: String) {
		Debug.print(text)

		self.expectModel.phone = text
		
	}
	

}
