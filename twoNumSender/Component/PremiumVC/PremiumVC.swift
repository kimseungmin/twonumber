//
//  PremiumVC.swift
//  twoNumSender
//
//  Created by vincent_kim on 2020/10/12.
//  Copyright © 2020 remain. All rights reserved.
//

import UIKit
import MessageUI
import SVProgressHUD
import CoreData
import StoreKit
import CallKit
import Alamofire
import SwiftyJSON
import CloudKit
import ObjectMapper

enum PremiumSectionType : String{
	case premiumManage  = "프리미엄 상품관리"
	case callKitManage  = "발신자 표시 관리"
	case backUpManage   = "연락처 백업 관리"
	
//    case reportManage  = "오류 제보"
}

enum PremiumManagers: String{
	case introduce             = "[프리미엄] 프리미엄 상품 설명"
	case purchaseMonth         = "[프리미엄] 상품 구매 또는 복원"
	case purchaseMonthUnlocked = "[프리미엄] 상품 이용 중 - 상품보기"
}
enum CallKitManagers: String{
	case introduce             = "[발신자 표시] 발신자 표시 설정방법"
	case sync                  = "[발신자 표시] 발신자 표시 연락처 적용하기"
	case report                = "[오류 제보] 발신번호 표시 기능오류 신고"
}
enum BackUpManagers: String{
	case backup                = "[백업] 연락처 백업"
	case archiving             = "[백업] 연락처 가져오기"
	case backUpLog             = "[오류 제보] 로그 전송"
}



enum ReportType : String{
	case callkit
	case backup
}


class PremiumVC: UIViewController {
	var isPurchased:Bool? = nil
	var mobileType = UserDefaults.standard
	
//    let appDelegateObj : AppDelegate = UIApplication.shared.delegate as! AppDelegate
//    let managedContext = CoreDataManager.shared.managedObjectContext
	@IBOutlet weak var tableView: UITableView!
	
	var sectionArray :[PremiumSectionType]           = []
	
	var callkitManageArray : [CallKitManagers] = [.introduce, .sync, .report]
	var premiumManageArray : [PremiumManagers] = [.purchaseMonth, .purchaseMonthUnlocked]
	var backUpManageArray  : [BackUpManagers]  = [.backup, .archiving]//, .backUpLog]
	
	
	
	var product:SKProduct?
	var adminCount = 0
	
	
	
	
	var contactsArray = [AnyObject]()
	var errorCnt = 0
	
	let database = CKContainer.default().privateCloudDatabase
	var cloudContact = [CKRecord]()
		
	override func viewDidLoad() {
		super.viewDidLoad()

		self.setTableView()
		
		self.registerNotification()
	}
	deinit {
		self.unregisterNotification()
		Debug.print("")
	}
	
	override func viewWillAppear(_ animated: Bool) {
		self.setSectionArray()
		
	}
	
	override func viewDidAppear(_ animated: Bool) {

	}
}

// SET UI
extension PremiumVC{
	private func registerCell() {
		self.tableView.register(UINib(nibName: String.init(describing: SetListCell.self), bundle: nil), forCellReuseIdentifier: String.init(describing: SetListCell.self))
		self.tableView.register(UINib(nibName: String.init(describing: SettingHeaderViewCell.self), bundle: nil), forHeaderFooterViewReuseIdentifier: String.init(describing: SettingHeaderViewCell.self))
	}
	func setIAPInfo(){
		self.isPurchased = IAPManager.shared.getPurchaseStatus()
		if self.isPurchased == nil{
			Toast.makeToast(message: IAPTextInfo.msg_checking_purchase, controller: self)
		}
	}
	
	@objc func setSectionArray(){
		self.setIAPInfo()
		self.sectionArray = [.premiumManage, .callKitManage, .backUpManage]//, .backUpManageLagacy]
		if let purchased = self.isPurchased{
			if purchased{
				self.premiumManageArray = [.introduce, .purchaseMonthUnlocked]
			}
			else{
				self.premiumManageArray = [.introduce, .purchaseMonth]
			}
			
		}
		else{
			self.premiumManageArray = [.introduce, .purchaseMonth]
		}
		
		
//		self.isPurchased = IAPManager.shared.getPurchaseStatus()
//		if self.isPurchased == nil{
//			Toast.makeToast(message: IAPTextInfo.msg_checking_purchase, controller: self)
//		}
//		if let puchaseStauts = IAPManager.shared.getPurchaseStatus(){
//			if puchaseStauts == false{
//				self.sectionArray = [.premiumManage]
//				self.premiumManageArray = [.introduce, .purchaseMonth]
//			}
//			else{
//				self.sectionArray = [.premiumManage, .callKitManage, .backUpManage]//, .backUpManageLagacy]
//				self.premiumManageArray = [.introduce, .purchaseMonthUnlocked]
//			}
//		}
//		else{
//
//		}
		
		
		UIView.transition(with: self.tableView, duration: 0.5, options: .transitionCrossDissolve, animations: {
			self.tableView.reloadData()
			
		}, completion: nil)
		
	}
}

// table view del
extension PremiumVC: UITableViewDelegate, UITableViewDataSource{
	func setTableView(){
		self.tableView.delegate = self
		self.tableView.dataSource = self
		self.tableView.tableFooterView = UIView()
		self.tableView.alwaysBounceVertical = false
		self.registerCell()
	}
	
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return SettingHeaderViewCell.cellHeight()
	}
	func numberOfSections(in tableView: UITableView) -> Int {
		// #warning Incomplete implementation, return the number of sections
		return self.sectionArray.count
	}
	
	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: String.init(describing: SettingHeaderViewCell.self)) as! SettingHeaderViewCell
		let sectionType = self.sectionArray[section]
		switch sectionType{
		case .premiumManage:
			headerView.titleLabel.text = PremiumSectionType.premiumManage.rawValue
			if headerView.gestureRecognizers == nil {
				let headerTapped = UITapGestureRecognizer(target: self, action: #selector(sectionHeaderTapped))
				headerView.addGestureRecognizer(headerTapped)
			}
		case .callKitManage:
			headerView.titleLabel.text = PremiumSectionType.callKitManage.rawValue
		case .backUpManage:
			headerView.titleLabel.text = PremiumSectionType.backUpManage.rawValue
		
		}
		return headerView
		
	}
	
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		// #warning Incomplete implementation, return the number of rows
		var count = 0
		let sectionType = self.sectionArray[section]
		switch sectionType {
		
		case .callKitManage:
			count = self.callkitManageArray.count
			
		case .premiumManage:
			count = self.premiumManageArray.count
		case .backUpManage:
			count = self.backUpManageArray.count

		}
		
		return count
		
		
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let cell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: SetListCell.self)) as! SetListCell
		cell.setListCheckBoxView.isHidden = true
		cell.contentView.backgroundColor = .white
		cell.accessoryType = .none
		var labelText = ""
		
		let section = self.sectionArray[indexPath.section]
		
		switch section {
			case .premiumManage:
				let row = self.premiumManageArray[indexPath.row]
				switch row{
				case .introduce:
					labelText = PremiumManagers.introduce.rawValue
				case .purchaseMonth:
					labelText = PremiumManagers.purchaseMonth.rawValue
				case .purchaseMonthUnlocked:
					labelText = PremiumManagers.purchaseMonthUnlocked.rawValue
				}
				cell.setListLabel.textColor = UIColor.black

			case .callKitManage:
				if let isPurchased = self.isPurchased{
					if !isPurchased{
						cell.contentView.backgroundColor = UIColor(hexString: "#F7F7F7").withAlphaComponent(0.7)
					}
				}
				
				let row = self.callkitManageArray[indexPath.row]
				switch row{
					case .sync:
						labelText = CallKitManagers.sync.rawValue
						cell.setListLabel.textColor = UIColor.black
					case .introduce:
						labelText = CallKitManagers.introduce.rawValue
						cell.setListLabel.textColor = UIColor.blue
					case .report:
						labelText = CallKitManagers.report.rawValue
						cell.setListLabel.textColor = UIColor.red
				}
					
					
			
				
			case .backUpManage:
				if let isPurchased = self.isPurchased{
					if !isPurchased{
						cell.contentView.backgroundColor = UIColor(hexString: "#F7F7F7").withAlphaComponent(0.7)
					}
				}
				
				let row = self.backUpManageArray[indexPath.row]
				switch row{
				case .backup:
					labelText = BackUpManagers.backup.rawValue
					cell.setListLabel.textColor = UIColor.black
				case .archiving:
					labelText = BackUpManagers.archiving.rawValue
					cell.setListLabel.textColor = UIColor.red
				case .backUpLog:
					labelText = BackUpManagers.backUpLog.rawValue
					cell.setListLabel.textColor = UIColor.blue
				}
		
		}
		
		
		cell.setListLabel.text = labelText
		return cell
		
	}
	
	
	
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		
		if let index = self.tableView.indexPathForSelectedRow{
			self.tableView.deselectRow(at: index, animated: true)
		}
		
		// 통신사 변경
		let sectionType = self.sectionArray[indexPath.section]
		
		switch sectionType {
		
		// 콜킷 관리
		case .callKitManage:

			if !self.checkIsPurchased(){
				return
			}
			let row = callkitManageArray[indexPath.row]
			switch row{
			case .sync:
				Debug.print("getJson")
				Toast.makeToast(message: "동기화가 시작되었습니다\n완료 시 푸시알람이 발송됩니다", controller: self)
                    CallKitManager.shared.saveCallKitData(success: nil, failure: nil)
				
				
			case .introduce:
				self.goToCallKitTuto()
				Debug.print("introduce")
				
			case .report:
				Debug.print("오류 제보")
				self.logFileAlert(type: .callkit)
			}
			
		// MARK: 구매
		case .premiumManage:
			let row = self.premiumManageArray[indexPath.row]
			switch row{
			case .purchaseMonth, .purchaseMonthUnlocked:
				
				let vc   = PurchaseMainVC()
				let navigationController = UINavigationController(rootViewController: vc)
				self.present(navigationController,animated: true, completion: nil)
				Debug.print("purchase")
				
			case .introduce:
				self.goToPremiumTuto()
				Debug.print("introduce")
			}
			
		case .backUpManage:
			if !self.checkIsPurchased(){
				return
			}
			
			let row = self.backUpManageArray[indexPath.row]
			
			switch row{
			case .backup:
				Debug.print("백업")
                guard let _ = RealmManager.shared.getAllContacts() else {
                    self.showEmptySaveAlert()
                    return
                }
				self.backUpStart()
				
			case .archiving:
				Debug.print("가져오기")
				let title = "연락처 가져오기"
				let msg = "앱에 저장되어 있는 \n모든 연락처삭제 후 불러온 데이터로 대체됩니다\n진행 할까요?"
				let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
				
				alert.addAction(UIAlertAction(title: "저장 연락처 초기화", style: .destructive) { action in
					self.showDeleteConfirmAlert()
					
				})
				
				alert.addAction(UIAlertAction(title: "취소", style: .default) { action in
				})
				
				self.present(alert, animated: true, completion: nil)
				
			case .backUpLog:
				Debug.print("백업 로그 전송")
				let title = "백업 로그 전송"
				let msg = "연락처 백업 시 발생한 로그를 개발자에게 전달합니다.\n진행 전 연락처 백업을 한 번 진행해 주세요"
				let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
				
				alert.addAction(UIAlertAction(title: "진행", style: .destructive) { action in
					self.logFileAlert(type: .backup)
				})
				
				alert.addAction(UIAlertAction(title: "취소", style: .default) { action in
				})
				
				self.present(alert, animated: true, completion: nil)
			}


		}
		
	}
	
	func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
		tableView.cellForRow(at: indexPath as IndexPath)?.selectionStyle = .blue
		tableView.cellForRow(at: indexPath as IndexPath)?.accessoryType = .none
		
	}
}


// event bus
extension PremiumVC{
	private func registerNotification() {
		NotificationCenter.default.addObserver(self, selector: #selector(setSectionArray), name: Notification.Name.refreshSetting, object: nil)
		
	}
	
	// unregister notification
	private func unregisterNotification() {
		NotificationCenter.default.removeObserver(self, name: Notification.Name.refreshSetting, object: nil)
		
	}
}

// CloudKit
extension PremiumVC{
	@objc func archivingDatabase() {
		
		CloudManager.shared.getCloudRecord(recordType: CloudKitInfo.cloud_name_old, result: { (records, error) -> Void in
			if let error = error{
				SVProgressHUD.dismiss()
				Debug.print(error)
			}
			else{
				if let records = records {
                    if records.isEmpty{
                        Debug.print("nil")
                        SVProgressHUD.dismiss()
                        let userInfo: [String: String] = ["refresh":"refresh"]
                        NotificationCenter.default.post(name: Notification.Name.refreshAddrList, object: nil, userInfo: userInfo)
						guard let purchase = IAPManager.shared.getPurchaseStatus() else { return }
						if purchase{
							Toast.makeToast(message: "동기화가 시작되었습니다\n완료 시 푸시알람이 발송됩니다", controller: self)
						}
                        CallKitManager.shared.saveCallKitData(success: nil, failure: nil)
                    }
					for model in records{
						
						let name = model.value(forKey: "name") as! String
						let phone = model.value(forKey: "phone") as! String
						let note = model.value(forKey: "note") as! String
						
						RealmManager.shared.createContatct(name: name, phone: phone, memo: note, completion: { (_, _, _) in
									
							if model == records.last{
								
								DispatchQueue.main.async {
									SVProgressHUD.dismiss()
									let userInfo: [String: String] = ["refresh":"refresh"]
									NotificationCenter.default.post(name: Notification.Name.refreshAddrList, object: nil, userInfo: userInfo)
									
									guard let purchase = IAPManager.shared.getPurchaseStatus() else { return }
									if purchase{
										Toast.makeToast(message: "동기화가 시작되었습니다\n완료 시 푸시알람이 발송됩니다", controller: self)
									}
                                    CallKitManager.shared.saveCallKitData(success: nil, failure: nil)
								}
								
							
							}
							
						})
				
						
					}
					
				} else {
					Debug.print("nil")
                    SVProgressHUD.dismiss()
                    let userInfo: [String: String] = ["refresh":"refresh"]
                    NotificationCenter.default.post(name: Notification.Name.refreshAddrList, object: nil, userInfo: userInfo)
                    
					guard let purchase = IAPManager.shared.getPurchaseStatus() else { return }
					if purchase{
						Toast.makeToast(message: "동기화가 시작되었습니다\n완료 시 푸시알람이 발송됩니다", controller: self)
					}
                    CallKitManager.shared.saveCallKitData(success: nil, failure: nil)
				}
			}
		})
		
	}
	
	
	

	

	@objc func lagacyBackUpStart() {
		
		LogManager.sharedInstance.removeFileData()
		let alert = UIAlertController(title: TextInfo.alert_title, message: "현재 앱에 저장되어있는 연락처 목록으로 덮어씌워집니다\n진행할까요?", preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: TextInfo.alert_confirm, style: .default) { action in
			SVProgressHUD.show(withStatus: TextInfo.indicator_backup)
			
			self.deleteLegacyDataBase()
			
			
		})
		alert.addAction(UIAlertAction(title: TextInfo.alert_cancel, style: .cancel) { action in
			
		})
		self.present(alert, animated: true, completion: nil)
		
		
	}
	
	@objc func deleteLegacyDataBase(){
        
//        self.getLocalData()
		
		CloudManager.shared.removeAllDataFromCloud(recordType:CloudKitInfo.cloud_name_old, completion: { (success) in
			
			if success{
				guard let models = RealmManager.shared.getAllContacts() else { return }
				self.saveToCloud(dataArray: models)
			}else{
				
				let okAction = UIAlertAction(title: TextInfo.alert_confirm, style: .default, handler: nil)
				UIAlertController.showAlert(title: TextInfo.alert_title, message: "삭제에 실패하였습니다\n앱 종료 후 다시 시도하세요", actions: [okAction])
			}
		})
		
		
	}

	
	func saveToCloud(dataArray:[ContactRealmModel]) {
//		CloudManager.shared.uploadToCloud(models: dataArray)
//		SVProgressHUD.dismiss()
		
		LogManager.sharedInstance.write("[로컬 데이터]============(count: \(dataArray.count))\n\(dataArray)", self)
		
		var localArray: [CKRecord] = []
		var succeedArray: [CKRecord] = []
		var failedArray: [CKRecord] = []
		let title = TextInfo.alert_title
		var msg = ""
        
		for item in dataArray{
//			let phone = item.value(forKey: kPhoneStr) as! String//dataArray[index].value(forKey: kPhoneStr) as! String
//			let name = item.value(forKey: kNameStr) as! String//dataArray[index].value(forKey: kNameStr) as! String
//			let note = item.value(forKey: kNoteStr) as! String//dataArray[index].value(forKey: kNoteStr) as! String
			
			let phone = item.phone//dataArray[index].value(forKey: kPhoneStr) as! String
			let name = item.name//dataArray[index].value(forKey: kNameStr) as! String
			let note = item.memo ?? ""//dataArray[index].value(forKey: kNoteStr) as! String
			
			//클라우드 킷에 새로운 레코드를 저장한다.
			let newCloudContact = CKRecord(recordType: CloudKitInfo.cloud_name_old)
			newCloudContact.setValue(name, forKey: "name")
			newCloudContact.setValue(note, forKey: "note")
			newCloudContact.setValue(phone, forKey: "phone")
			localArray.append(newCloudContact)
			
		}
		
        for item in localArray{
			database.save(item) { (record, error) in
				if error == nil{
					msg = "백업 완료"
					Debug.print("업로드 성공")
					succeedArray.append(item)
                    
				}
				else{
					Debug.print(error!.localizedDescription)
					failedArray.append(item)
					LogManager.sharedInstance.write("\n[에러발생]============\(error!.localizedDescription)")
					
					if error?.localizedDescription == "CloudKit access was denied by user settings"{
						msg = "설정 -> iCloud -> iCloud Drive를 On 시켜주세요"
						SVProgressHUD.dismiss()
					}else{
						msg = "일부 동기화 되지 않은 데이터가 있을 수 있습니다.\n[로그전송]을 통해 관리자에게 문의해주세요"
					}
					
				}
				
				if item == localArray.last{
					SVProgressHUD.dismiss()
					DispatchQueue.main.async { [weak self] in
						guard let self = self else{
							return
						}
						let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
						alert.addAction(UIAlertAction(title: TextInfo.alert_confirm, style: .cancel) { action in })
						self.present(alert, animated: true, completion: nil)
					}
					LogManager.sharedInstance.write("\n[성공데이터]============(count: \(succeedArray.count))\n\(succeedArray)")
					LogManager.sharedInstance.write("\n[실패데이터]============(count: \(failedArray.count))\n\(failedArray)")
					
				}
				
			}
			
			
		}
		
		
	}
	
	func deleteAllCoreDataForBackUpLegacy(){
		
		let title = TextInfo.alert_delete_contacts_title
		let msg = TextInfo.alert_delete_all_contacts
		let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
		
		alert.addAction(UIAlertAction(title: "삭제", style: .destructive) { action in
			RealmManager.shared.deleteAllContacts(completion: { (success) in
				
				if success{
					
					SVProgressHUD.show(withStatus: "연락처 가져오기 진행 중")
					self.archivingDatabase()
					
				}
                else{
                    SVProgressHUD.dismiss()
                }
                
				
			})
			

		})
		
		alert.addAction(UIAlertAction(title: "취소", style: .default) { action in
			
		})
		
		self.present(alert, animated: true, completion: nil)
		
		
	}
}

// MARK: MFMailComposeViewController Delegate
extension PremiumVC: MFMailComposeViewControllerDelegate {
	
	func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
		switch result.rawValue {
		case MFMailComposeResult.cancelled.rawValue:
			Debug.print("Cancelled")
		case MFMailComposeResult.saved.rawValue:
			Debug.print("Saved")
		case MFMailComposeResult.sent.rawValue:
			Debug.print("Sent")
		case MFMailComposeResult.failed.rawValue:
			Debug.print("Error: \(String(describing: error?.localizedDescription))")
		default:
			break
		}
		controller.dismiss(animated: true, completion: nil)
	}
	
}

// action
extension PremiumVC{
	@objc func sectionHeaderTapped(recognizer: UITapGestureRecognizer) {
		self.adminCount += 1
		
		if self.adminCount == 5{
			// debug for vincent
			self.adminCount = 0
			self.vincentAlert()
		}
		
	}
	
	
	// send Email - log file
	private func sendEmailForLogFile(email:String, type:ReportType) {
		
		if MFMailComposeViewController.canSendMail() {

			let mail = MFMailComposeViewController()
			mail.mailComposeDelegate = self
			mail.setToRecipients([ReportEmailInfo.recipients_address])
			mail.setCcRecipients([ReportEmailInfo.recipients_cc_address])
			mail.setSubject(ReportEmailInfo.email_subject)
			
			switch type {
				
				case .callkit:
					// 파일 첨부하기
					LogManager.sharedInstance.removeFileData()
					LogManager.sharedInstance.write("[\n사용 환경]\nOS 버전 : \(SystemUtils.shared.osVersion())\nApp 버전 : \(SystemUtils.shared.appVersion())\n사용기기 : \(UIDevice.current.modelName)", self)
                    CallKitManager.shared.saveCallKitData(success: nil, failure: nil)
				case .backup:
					LogManager.sharedInstance.removeFileData()
                    LogManager.sharedInstance.write("[\n사용 환경]\nOS 버전 : \(SystemUtils.shared.osVersion())\nApp 버전 : \(SystemUtils.shared.appVersion())\n사용기기 : \(UIDevice.current.modelName)" , self)
			}
			IAPManager.shared.verifyRecipt(withProgress: true)
			SVProgressHUD.show()
			SystemUtils.shared.delay(delay: 5){
				guard let logData = LogManager.sharedInstance.getFileData() else {
					let alertController = UIAlertController(title: nil, message: ReportEmailInfo.alert_cannotfind_file, preferredStyle: UIAlertController.Style.alert)

					//UIAlertActionStye.destructive 지정 글꼴 색상 변경
					let okAction = UIAlertAction(title: TextInfo.alert_confirm, style: UIAlertAction.Style.destructive)
					alertController.addAction(okAction)
					self.present(alertController, animated: true, completion: nil)
					return
				}
				mail.addAttachmentData(logData, mimeType: "text/txt", fileName: "twonumsender_callkit_log.txt")

				// 본문 채우기
				let messageBody = String.init(format: "[사용 환경]\nOS 버전 : %@\nApp 버전 : %@\n사용기기 : %@", SystemUtils.shared.osVersion(), SystemUtils.shared.appVersion(), UIDevice.current.modelName + "\n\n*[필수]답변받을 이메일 : " + email)
				mail.setMessageBody(messageBody, isHTML: false)
				
				
				self.present(mail, animated: true) {
					SVProgressHUD.dismiss()
				}
			}
			
			
		}
		else {
			let alertController = UIAlertController(title: nil, message: ReportEmailInfo.alert_send_email_fail, preferredStyle: UIAlertController.Style.alert)

			let okAction = UIAlertAction(title: TextInfo.alert_confirm, style: UIAlertAction.Style.destructive)
			alertController.addAction(okAction)
			self.present(alertController, animated: true, completion: nil)
		}
	}
}


// move Page
extension PremiumVC{
	@objc func goToCallKitTuto(){
		let tutorialVC = TutorialCollectionVC(collectionViewLayout: UICollectionViewFlowLayout())
		tutorialVC.tutorialType = .callKit
		let navigationController = UINavigationController(rootViewController: tutorialVC)
		tutorialVC.modalTransitionStyle = .crossDissolve
		self.present(navigationController, animated: true, completion: nil)
	}
	
	@objc func goToPremiumTuto(){
		let tutorialVC = TutorialCollectionVC(collectionViewLayout: UICollectionViewFlowLayout())
		tutorialVC.tutorialType = .premium
		let navigationController = UINavigationController(rootViewController: tutorialVC)
		tutorialVC.modalTransitionStyle = .crossDissolve
		self.present(navigationController, animated: true, completion: nil)
	}
}

// alert
extension PremiumVC{
	/*notice Alert*/
	func noticeAlert(){
		
		let title = "안내 사항"
		let msg = "본 앱은 통신사에서 제공하는 투넘버 , 넘버플러스, 듀얼넘버를 이용하실때만 사용 가능 한 앱입니다\n부가서비스가 가입되어있지 않은 경우 서비스를 정상적으로 이용 하실 수 없습니다\n구매 취소 및 구독 취소는 앱스토어 -> 구입내역에서 신청하실 수 있습니다.\n[발신자표시] 는 휴대폰 자체에 저장되어있는 번호의 경우 표시하지 않습니다"
		let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
		
		alert.addAction(UIAlertAction(title: "확인", style: .default) { action in

			self.logFileAlert(type: .callkit)
			
		})
		
		alert.addAction(UIAlertAction(title: "취소", style: .destructive) { action in
			
		})
		
		self.present(alert, animated: true, completion: nil)
	
	}
	
	/*오류 제보 Alert*/
	func logFileAlert(type: ReportType){
		
		var title = ""
		var msg = ""
		
		switch type {
		case .callkit:
			title = "동기화 오류제보"
			msg = "현재 저장되어있는 연락처의 오류를 확인하기 위한 데이터를 전송합니다.\n회신 가능한 메일주소를 입력하세요"
		case .backup:
			title = "백업 오류제보"
			msg = "최근 백업 기록 데이터를 전송합니다.\n회신 가능한 메일주소를 입력하세요"
		}
		
		
		let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
		alert.addTextField()
		alert.addAction(UIAlertAction(title: "확인", style: .default) { action in
			let text = alert.textFields?[0].text
			if text == ""{
				return
			}else{
				self.sendEmailForLogFile(email: text ?? "", type: type)
			}
		})
		
		self.present(alert, animated: true, completion: nil)
	}
	/*vincent alert*/
	func vincentAlert(){
		let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
		alert.addTextField()
		alert.addAction(UIAlertAction(title: "확인", style: .default) { action in
			let text = alert.textFields?[0].text
			if text == IAPManager.adminString{
				
				UserDefaults.standard.set(IAPManager.adminString, forKey: .premium)
				IAPManager.shared.verifyRecipt(withProgress: true)
			}
			if text == "off"{
				
				UserDefaults.standard.set("", forKey: .premium)
				IAPManager.shared.verifyRecipt(withProgress: true)

			}
		})
		
		self.present(alert, animated: true, completion: nil)
	}
	
	
	
	/* 경고 출력 */
	func displayAlertMessage(_ title: String, message: String, preferredStyle: UIAlertController.Style.RawValue, actionStyleType: UIAlertAction.Style.RawValue, isAction: Bool) {
		
		
		let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style(rawValue: preferredStyle)!)
		
		let dismissAction = UIAlertAction(title: TextInfo.alert_confirm, style: UIAlertAction.Style(rawValue: actionStyleType)!) { (action) -> Void in
			if isAction == true {
				//                self.navigationController?.popViewController(animated: true)
				self.dismiss(animated: true, completion: nil)
				return
			}else{
				return
			}
		}
		
		
		alertController.addAction(dismissAction)
		
		self.present(alertController, animated: true, completion: nil)
	}
	/* 경고 출력 */
}

//MARK: - renewal backup
extension PremiumVC{
	@objc func backUpStart(){
		let alert = UIAlertController(title: TextInfo.alert_title, message: "현재 앱에 저장되어있는 연락처 목록으로 덮어씌워집니다\n진행할까요?", preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: TextInfo.alert_confirm, style: .default) { action in
			
			SVProgressHUD.show(withStatus: TextInfo.indicator_backup)
			LogManager.sharedInstance.removeFileData()
			
			CloudManager.shared.removeAllDataFromCloud(recordType:CloudKitInfo.cloud_name, completion: { (finish) in
				guard let models = RealmManager.shared.getAllContacts() else { return }
				CloudManager.shared.uploadToCloud(models: models, completion: { (finish) in
					SVProgressHUD.dismiss()
				})
				
			})
			
			
		})
		alert.addAction(UIAlertAction(title: TextInfo.alert_cancel, style: .cancel) { action in
			
		})
		self.present(alert, animated: true, completion: nil)
		
	}
	
	@objc func showEmptySaveAlert(){
        SVProgressHUD.dismiss()
        let okAction = UIAlertAction(title: TextInfo.alert_confirm, style: .default, handler: nil)
        UIAlertController.showAlert(title: TextInfo.alert_title, message: "백업할 연락처가 없습니다", actions: [okAction])
	}
    
	@objc func showDeleteConfirmAlert(){
		let title = TextInfo.alert_delete_contacts_title
		let msg = TextInfo.alert_delete_all_contacts
		let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: "삭제", style: .destructive) { action in
			self.syncFromCloud()
		})
		alert.addAction(UIAlertAction(title: "취소", style: .default) { action in
			
		})
		
		self.present(alert, animated: true, completion: nil)
			
		
	}
	
	func syncFromCloud(){
		SVProgressHUD.show(withStatus: "기존 연락처 초기화 중")
		RealmManager.shared.deleteAllContacts(completion: { (success) in
			if success{
				SVProgressHUD.dismiss()
				SVProgressHUD.show(withStatus: "연락처 가져오기 진행 중")
				CloudManager.shared.getCloudRecord(recordType: CloudKitInfo.cloud_name, result: { (records, error) -> Void in
					if let error = error{
						SVProgressHUD.dismiss()
						Debug.print(error)
						UIAlertController.showMessage("복원할 연락처가 없습니다")
					}
					else{
						guard let records = records else {
                            SVProgressHUD.dismiss()
                            return
                        }
						
						var models:[ContactModel] = []
						
                        if records.isEmpty{
                            SVProgressHUD.dismiss()

                            CallKitManager.shared.saveCallKitData(success: nil, failure: nil)
                        }
                        else{
                            for model in records{
                                
                                let contacts = model.value(forKey: "contacts") as! String
                                let parseModels = Mapper<ContactModel>().mapArray(JSONString: contacts) ?? []
                                models += parseModels
                                
                            }
                            
                            for (idx, el) in models.enumerated(){
                                RealmManager.shared.createContatct(name: el.name, phone: el.phone, memo: el.memo, completion: { (_, _, _) in
                                    
                                    if idx == models.count - 1{
                                        SVProgressHUD.dismiss()
                                        CallKitManager.shared.saveCallKitData(success: nil, failure: nil)
                                        DispatchQueue.main.async {
                                            let userInfo: [String: String] = ["refresh":"refresh"]
                                            NotificationCenter.default.post(name: Notification.Name.refreshAddrList, object: nil, userInfo: userInfo)
                                            
                                        }
                                        
                                    }
                                })
                            }
                            
                        }
						
					}
				})
//				self.archivingDatabase()
			}
		})
		
	}
	
	
	
}

extension PremiumVC{
	func checkIsPurchased() -> Bool{
		guard let isPurchased = self.isPurchased else {
			Toast.makeToast(message: "구독검증이 진행 중입니다.\n잠시 후 다시 시도하세요", controller: self)
			return false
		}
		if !isPurchased{
			let confirmAction = UIAlertAction(title: "구매", style: .default, handler: { (action) in
				let vc   = PurchaseMainVC()
				let navigationController = UINavigationController(rootViewController: vc)
				self.present(navigationController,animated: true, completion: nil)
			})

			let cancelAction = UIAlertAction(title: TextInfo.alert_cancel, style: .cancel, handler: nil)
			UIAlertController.showAlert(title: TextInfo.alert_title, message: "상품 구매가 필요합니다\n상품 복원은 더보기에서 확인할 수 있습니다", actions: [confirmAction, cancelAction])
			return false
		}
		
		return true
	}
}
