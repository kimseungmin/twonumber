//
//  SetListCell.swift
//  twoNumSender
//
//  Created by seungmin kim on 26/05/2019.
//  Copyright © 2019 remain. All rights reserved.
//

import UIKit

class SetListCell: UITableViewCell {

    @IBOutlet weak var setListLabel: UILabel!
    @IBOutlet weak var setListCheckBoxView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
