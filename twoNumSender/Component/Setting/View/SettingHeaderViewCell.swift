//
//  SettingHeaderViewCell.swift
//  twoNumSender
//
//  Created by seungmin kim on 06/06/2019.
//  Copyright © 2019 remain. All rights reserved.
//

import UIKit

class SettingHeaderViewCell: UITableViewHeaderFooterView {

    @IBOutlet weak var titleLabel: UILabel!
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
}

extension SettingHeaderViewCell {
    class func cellHeight() -> CGFloat {
        return 32
    }
}
