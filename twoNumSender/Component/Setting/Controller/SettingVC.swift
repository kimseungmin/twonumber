//
//  SettingVC.swift
//  twoNumSender
//
//  Created by vincent_kim on 2020/10/12.
//  Copyright © 2020 remain. All rights reserved.
//

import UIKit
import MessageUI
import SVProgressHUD
import CoreData
import StoreKit
import CallKit
import Alamofire
import SwiftyJSON
import CloudKit
import Contacts
import ContactsUI

enum SettingSectionTypes: String{
    case myInfo         = "내 정보"
    case moType         = "통신사 수정"
    case help           = "도움말"
    case share          = "앱 추천"
    case contanctManage = "연락처 관리"
    case info           = "앱 정보"
    case testData       = "테스트 ---------------"
}

enum MyInfos: String{
    case tel     = "나의 투넘버 설정하기"
    
}

enum MoTypes: String{
    case kt      = "KT (투넘버)"
    case skt     = "SKT (넘버플러스)"
    case uplus   = "U+ (톡톡 듀얼넘버)"
    
}

enum Helps: String{
    case notice = "공지사항 / 문의 하기"
    case faq    = "자주 묻는 질문"
    case guide  = "사용자 설명서"
    
}

enum Infos: String{
    case privacy = "개인정보 처리방침"
    case service = "서비스 이용약관"
    case update  = "앱 버전"
}

enum Shares: String {
    case share   = "친구에게 투넘버 문자 공유하기"
    case remain  = "More Apps"
    case review  = "리뷰하기 (여러분의 리뷰가 힘이됩니다)"
}

enum ContactManagers: String{
    case addAll = "휴대폰 연락처 모두 가져오기"
    case delete = "연락처 모두삭제"
}


enum TestDatas:String{
    case callTest = "연락처 적용 완료 여부 가져오기"
}


class SettingVC: UIViewController, MFMessageComposeViewControllerDelegate {
    var mobileType = UserDefaults.standard
    
    //    let appDelegateObj : AppDelegate = UIApplication.shared.delegate as! AppDelegate
    //    let managedContext = CoreDataManager.shared.managedObjectContext
    
    var sectionArray :[SettingSectionTypes]    = [.myInfo, .moType, .contanctManage, .help, .share, .info]
    var myInfoArray : [MyInfos]                = [.tel]
    var moTypeArray : [MoTypes]                = [.kt, .skt, .uplus]
    var helpArray : [Helps]                    = [.notice, .faq, .guide]
    var infoArray : [Infos]                    = [.privacy, .service, .update]
    var shareArray : [Shares]                  = [.share, .remain, .review]
    var contactManageArray : [ContactManagers] = [.delete , .addAll]
    
    var product:SKProduct?
    
    var saveCallKitDataCycle : SaveCallKitDataCycleType = .first
    let purchaseStatus = UserDefaults.standard.string(forKey: .premium)
    
    var testArray : [TestDatas]                = [.callTest]
    
    var contactsArray = [AnyObject]()
    var errorCnt = 0
    
    /* 연락처 접근 */
    var updateContact = CNContact()
    var contactStore = CNContactStore()
    /* 연락처 접근 */
    
    
    let database = CKContainer.default().privateCloudDatabase
    var cloudContact = [CKRecord]()
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setTableView()
        
        self.registerNotification()
        
    }
    deinit {
        self.unregisterNotification()
        Debug.print("")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //        self.setSectionArray()
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
}

// table view del
extension SettingVC: UITableViewDelegate, UITableViewDataSource{
    func setTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tableFooterView = UIView()
        self.tableView.alwaysBounceVertical = false
        self.registerCell()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return SettingHeaderViewCell.cellHeight()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return self.sectionArray.count
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: String.init(describing: SettingHeaderViewCell.self)) as! SettingHeaderViewCell
        let sectionType = self.sectionArray[section]
        switch sectionType{
            case .myInfo:
                headerView.titleLabel.text = SettingSectionTypes.myInfo.rawValue
            case .moType:
                headerView.titleLabel.text = SettingSectionTypes.moType.rawValue
            case .share:
                headerView.titleLabel.text = SettingSectionTypes.share.rawValue
            case .help:
                headerView.titleLabel.text = SettingSectionTypes.help.rawValue
            case .contanctManage:
                headerView.titleLabel.text = SettingSectionTypes.contanctManage.rawValue
            case .testData:
                headerView.titleLabel.text = SettingSectionTypes.testData.rawValue
            case .info:
                headerView.titleLabel.text = SettingSectionTypes.info.rawValue
        }
        return headerView
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        var count = 0
        let sectionType = self.sectionArray[section]
        switch sectionType {
            case .myInfo:
                count = self.myInfoArray.count
            case .moType:
                count = self.moTypeArray.count
            case .help:
                count = self.helpArray.count
            case .share:
                count = self.shareArray.count
            case .contanctManage:
                count = self.contactManageArray.count
            case .testData:
                count = self.testArray.count
            case .info:
                count = self.infoArray.count
        }
        
        return count
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell
        
        var labelText = ""
        
        let section = self.sectionArray[indexPath.section]
        
        switch section {
            case .myInfo:
                let setListCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: SetListCell.self)) as! SetListCell
                setListCell.setListCheckBoxView.isHidden = true
                setListCell.accessoryType = .none
                
                let row = self.myInfoArray[indexPath.row]
                switch row {
                    case .tel:
                        labelText = MyInfos.tel.rawValue
                        
                }
                
                setListCell.setListLabel.textColor = UIColor.black
                setListCell.setListLabel.text = labelText
                cell = setListCell
                
                
            case .moType:
                let setListCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: SetListCell.self)) as! SetListCell
                setListCell.setListCheckBoxView.isHidden = true
                setListCell.accessoryType = .none
                
                let row = self.moTypeArray[indexPath.row]
                
                switch row{
                    case .kt:
                        if mobileType.string(forKey: .mobileType) == "kt"{
                            setListCell.setListCheckBoxView.isHidden = false
                        }
                        labelText = MoTypes.kt.rawValue
                    case .skt:
                        if mobileType.string(forKey: .mobileType) == "skt"{
                            setListCell.setListCheckBoxView.isHidden = false
                        }
                        labelText = MoTypes.skt.rawValue
                    case .uplus:
                        if mobileType.string(forKey: .mobileType) == "uplus"{
                            setListCell.setListCheckBoxView.isHidden = false
                        }
                        labelText = MoTypes.uplus.rawValue
                }
                setListCell.setListLabel.textColor = UIColor.black
                setListCell.setListLabel.text = labelText
                cell = setListCell
                
            case .help:
                let setListCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: SetListCell.self)) as! SetListCell
                setListCell.setListCheckBoxView.isHidden = true
                setListCell.accessoryType = .none
                
                let row = self.helpArray[indexPath.row]
                switch row{
                    case .notice:
                        labelText = Helps.notice.rawValue
                    case .faq:
                        labelText = Helps.faq.rawValue
                    case .guide:
                        labelText = Helps.guide.rawValue
                        
                }
                setListCell.setListLabel.textColor = UIColor.black
                setListCell.setListLabel.text = labelText
                cell = setListCell
                
            case .info:
                let setListCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: SetListCell.self)) as! SetListCell
                setListCell.setListCheckBoxView.isHidden = true
                setListCell.accessoryType = .none
                
                let row = self.infoArray[indexPath.row]
                switch row{
                    case .privacy:
                        labelText = Infos.privacy.rawValue
                    case .service:
                        labelText = Infos.service.rawValue
                    case .update:
                        let appVersion = SystemUtils.shared.appVersion()
                        labelText = "설치된 " + Infos.update.rawValue + " " + appVersion
                }
                setListCell.setListLabel.textColor = UIColor.black
                setListCell.setListLabel.text = labelText
                cell = setListCell
                
            case .contanctManage:
                let setListCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: SetListCell.self)) as! SetListCell
                
                setListCell.setListCheckBoxView.isHidden = true
                setListCell.accessoryType = .none
                
                let row = self.contactManageArray[indexPath.row]
                switch row{
                    case .delete:
                        labelText = ContactManagers.delete.rawValue
                        setListCell.setListLabel.textColor = UIColor.red
                    case .addAll:
                        labelText = ContactManagers.addAll.rawValue
                        setListCell.setListLabel.textColor = UIColor.red
                }
                
                
                setListCell.setListLabel.text = labelText
                cell = setListCell
                
            case .testData:
                let setListCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: SetListCell.self)) as! SetListCell
                setListCell.setListCheckBoxView.isHidden = true
                setListCell.accessoryType = .none
                
                let row = self.testArray[indexPath.row]
                switch row{
                    case .callTest:
                        labelText = TestDatas.callTest.rawValue
                        setListCell.setListLabel.textColor = UIColor.red
                }
                setListCell.setListLabel.text = labelText
                cell = setListCell
            case .share:
                let setListCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: SetListCell.self)) as! SetListCell
                setListCell.setListCheckBoxView.isHidden = true
                setListCell.accessoryType = .none
                
                let row = self.shareArray[indexPath.row]
                switch row{
                    case .share:
                        labelText = Shares.share.rawValue
                    case .remain:
                        labelText = Shares.remain.rawValue
                    case .review:
                        labelText = Shares.review.rawValue
                }
                setListCell.setListLabel.textColor = UIColor.black
                setListCell.setListLabel.text = labelText
                cell = setListCell
        }
        
        return cell
        
    }
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let index = self.tableView.indexPathForSelectedRow{
            self.tableView.deselectRow(at: index, animated: true)
        }
        
        
        //		let downloadLink = "itunes.apple.com/app/id1234443708"
        
        // 통신사 변경
        let sectionType = self.sectionArray[indexPath.section]
        let motypeIndex = self.sectionArray.firstIndex(of: SettingSectionTypes.moType)
        Debug.print(motypeIndex!)
        
        switch sectionType {
            case .myInfo:
                let row = self.myInfoArray[indexPath.row]
                switch row{
                    case .tel:
                        NotificationCenter.default.post(name: Notification.Name.reloadNavBar, object: nil, userInfo: nil)
                }
                
                
            case .moType:

                
//                SystemUtils.shared.delay(delay: 0.5) { [weak self] in
//                    guard let self = self else { return }
                    let row = self.moTypeArray[indexPath.row]
                    let currentType = self.mobileType.string(forKey: .mobileType)!
                    switch row{
                        case .kt:
                            if self.mobileType.string(forKey: .mobileType) == "kt" {
                                SVProgressHUD.dismiss()
                                return
                            }
                            else{
                                DispatchQueue.main.async {
                                    SVProgressHUD.show(withStatus: "통신사 변경 중")
                                }
                                
                                self.mobileType.set("kt", forKey: .mobileType)
                                
                                self.mobileType.synchronize()
                                
                                self.convertMoType(.kt, currentType: currentType)
                                self.tableView.reloadData()
                                
                            }
                        case .skt:
                            if self.mobileType.string(forKey: .mobileType) == "skt" {
                                SVProgressHUD.dismiss()
                                return
                            }
                            else{
                                DispatchQueue.main.async {
                                    SVProgressHUD.show(withStatus: "통신사 변경 중")
                                }
                                
                                self.mobileType.set("skt", forKey: .mobileType)
                                self.mobileType.synchronize()
                                self.convertMoType(.skt, currentType: currentType)
                                self.tableView.reloadData()
                                
                            }
                        case .uplus:
                            if self.mobileType.string(forKey: .mobileType) == "uplus" {
                                SVProgressHUD.dismiss()
                                return
                            }
                            else{
                                DispatchQueue.main.async {
                                    SVProgressHUD.show(withStatus: "통신사 변경 중")
                                }
                                
                                self.mobileType.set("uplus", forKey: .mobileType)
                                self.mobileType.synchronize()
                                self.convertMoType(.uplus, currentType: currentType)
                                self.tableView.reloadData()
                                
                            }
//                    }
                }

                // 도움말
            case .help:
                let row = self.helpArray[indexPath.row]
                switch row{
                    case .notice:
                        let vc   = CommonWebViewVC()
                        let navigationController = UINavigationController(rootViewController: vc)
                        vc.landingUrl = NetInfo.qna
                        vc.vcTitle = TextInfo.title_qna
                        navigationController.modalPresentationStyle = .fullScreen
                        DispatchQueue.main.async { [weak self] in
                            guard let self = self else { return }
                            self.present(navigationController, animated: true, completion: nil)
                        }
                        
                    case .faq:
                        let vc   = CommonWebViewVC()
                        let navigationController = UINavigationController(rootViewController: vc)
                        vc.landingUrl = NetInfo.faq
                        vc.vcTitle = TextInfo.title_faq
                        navigationController.modalPresentationStyle = .fullScreen
                        DispatchQueue.main.async { [weak self] in
                            guard let self = self else { return }
                            self.present(navigationController, animated: true, completion: nil)
                        }
                        
                        
                    case .guide:
                        let tutorialVC = TutorialCollectionVC(collectionViewLayout: UICollectionViewFlowLayout())
                        tutorialVC.tutorialType = .tutorial
                        let navigationController = UINavigationController(rootViewController: tutorialVC)
                        tutorialVC.modalTransitionStyle = .crossDissolve
                        DispatchQueue.main.async { [weak self] in
                            guard let self = self else { return }
                            self.present(navigationController, animated: true, completion: nil)
                        }
                        
                        
                }
                // 정보
            case .info:
                let row = self.infoArray[indexPath.row]
                switch row{
                        
                    case .privacy:
                        
                        let vc   = TermVC()
                        let navigationController = UINavigationController(rootViewController: vc)
                        vc.termText = TermInfo.privacy_term
                        vc.termTitle = TextInfo.title_privacy
                        
                        self.present(navigationController,animated: true, completion: nil)
                    case .service:
                        
                        let vc   = CommonWebViewVC()
                        let navigationController = UINavigationController(rootViewController: vc)
                        vc.landingUrl = "https://smbh.kr/twonum_terms/index.html"
                        vc.vcTitle = TextInfo.title_serviceTerm
                        navigationController.modalPresentationStyle = .fullScreen
                        
                        DispatchQueue.main.async { [weak self] in
                            guard let self = self else { return }
                            self.present(navigationController, animated: true, completion: nil)
                        }
                        
                    case .update:
                        let title = "앱스토어 바로가기"
                        let msg = "앱스토어로 이동할까요?"
                        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "나중에", style: .cancel, handler: nil))
                        
                        alert.addAction(UIAlertAction(title: "이동하기", style: .default) { action in
                            
                            let appLinkUrl = "itms-apps://itunes.apple.com/app/id\(AppInfo.appId)"
                            
                            let URL = NSURL(string : appLinkUrl)!
                            UIApplication.shared.open(URL as URL, options: [:], completionHandler: nil)
                            
                        })
                        
                        DispatchQueue.main.async { [weak self] in
                            guard let self = self else { return }
                            self.present(alert, animated: true, completion: nil)
                        }
                }
                // 연락처 관리
            case .contanctManage:
                let row = contactManageArray[indexPath.row]
                switch row{
                    case .delete:
                        let title = TextInfo.alert_delete_contacts_title
                        let msg = TextInfo.alert_delete_all_contacts
                        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "삭제", style: .destructive) { action in
                            
                            self.deleteAllCoreData(.delete)
                        })
                        
                        alert.addAction(UIAlertAction(title: "취소", style: .default, handler: nil))
                        
                        DispatchQueue.main.async { [weak self] in
                            guard let self = self else { return }
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                    case .addAll:
                        let title = TextInfo.alert_addAll_contacts_title
                        let msg = TextInfo.alert_add_all_contacts
                        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "확인", style: .destructive) { action in
                            DispatchQueue.main.async { [weak self] in
                                guard let self = self else { return }
                                self.deleteAllCoreData(.addAll)
                            }
                            
                        })
                        
                        alert.addAction(UIAlertAction(title: "취소", style: .default, handler: nil))
                        DispatchQueue.main.async { [weak self] in
                            guard let self = self else { return }
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                }
                
                
            case .testData:
                return
            case .share:
                let row = self.shareArray[indexPath.row]
                switch row{
                    case .share:
                        
                        let downloadLink = "itunes.apple.com/app/id\(AppInfo.appId)"
                        
                        //문자열을 클립보드에 복사
                        UIPasteboard.general.string = downloadLink
                        UIAlertController.showMessage("클립보드에 다운로드 링크가 복사되었습니다")
                        
                    case .remain:
                        let id = "https://itunes.apple.com/kr/developer/seungmin-kim/id1225191558"
                        guard let url = URL(string: "\(id)") else { return }
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(url)
                        }
                        
                    case .review:
                        self.showReviewAlert()
                        //				if #available(iOS 10.3, *) {
                        //					SKStoreReviewController.requestReview()
                        //				} else {
                        //					let title = "앱 리뷰"
                        //					let msg = "투넘버 문자를 이용 해 주셔서 감사합니다\n리뷰 화면으로 이동할까요?"
                        //					let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
                        //
                        //					alert.addAction(UIAlertAction(title: "나중에", style: .cancel) { action in
                        //
                        //					})
                        //
                        //					alert.addAction(UIAlertAction(title: "이동하기", style: .default) { action in
                        //
                        //						let appLinkUrl = "itms-apps://itunes.apple.com/app/id\(AppInfo.appId)"
                        //
                        //						let URL = NSURL(string : appLinkUrl)!
                        //						UIApplication.shared.open(URL as URL, options: [:], completionHandler: nil)
                        //
                        //					})
                        //
                        //
                        //					self.present(alert, animated: true, completion: nil)
                        //
                        //				}
                }
        }
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath as IndexPath)?.selectionStyle = .blue
        tableView.cellForRow(at: indexPath as IndexPath)?.accessoryType = .none
        
    }
}
// SET UI
extension SettingVC{
    private func registerCell() {
        self.tableView.register(UINib(nibName: String.init(describing: SetListCell.self), bundle: nil), forCellReuseIdentifier: String.init(describing: SetListCell.self))
        self.tableView.register(UINib(nibName: String.init(describing: SettingHeaderViewCell.self), bundle: nil), forHeaderFooterViewReuseIdentifier: String.init(describing: SettingHeaderViewCell.self))
    }
    
}

//function / logic
extension SettingVC: CNContactPickerDelegate{
    
    
    func convertMoType(_ type:MoTypes, currentType:String) {
        
        let dispatchGroup = DispatchGroup()
//        let dispatchQueue = DispatchQueue(label: "Realm")
        let dispatchQueue = DispatchQueue.main
        let dispatchSemaphore = DispatchSemaphore(value: 0)
        
        let dataArray: [ContactRealmModel] = RealmManager.shared.getAllContacts() ?? []
        
        dispatchQueue.async(group: dispatchGroup) { [weak self] in
            guard self != nil else { return }
            
            dataArray.forEach { el in
                dispatchGroup.enter()
                var item = el.phone
                item = item.replacingOccurrences(of: "‬", with: "")
                item = item.replacingOccurrences(of: "‬", with: "")
                item = item.replacingOccurrences(of: "‭", with: "")
                var replaceEmpty  = ""
                var replaceMotype = ""
                
                /*국가번호 반영*/
                //                if let indexInt = item.firstIndexDistance(of: "0"){
                //                    print(debug: indexInt)
                //                    if indexInt < 6{
                //                        item = String(item.dropFirst(indexInt))
                //                    }
                //                }
                /*국가번호 반영*/
                
                /*특수문자 제거*/
                replaceEmpty = item.removingAllWhitespacesAndNewlines
                
                /*특수문자 제거*/
                
                /*기존 규칙 제거*/
                replaceMotype = replaceEmpty.replacingOccurrences(of: "", with: "")
                replaceMotype = replaceEmpty.replacingOccurrences(of: " ", with: "")
                /*기존 규칙 제거*/
                
                if replaceMotype.contains("*281"){
                    replaceMotype = replaceEmpty.replacingOccurrences(of: "*281", with: "")
                }
                if replaceMotype.contains("*77"){
                    replaceMotype = replaceEmpty.replacingOccurrences(of: "*77", with: "")
                }
                if replaceMotype.contains("*"){
                    replaceMotype = replaceEmpty.replacingOccurrences(of: "*", with: "")
                }
                
                
                /*기존 규칙 제거*/
                
                /*규칙 적용*/
                switch type{
                    case .kt:
                        replaceMotype = "*77 "+replaceMotype
                        UserDefaults.standard.set("kt", forKey: .mobileType)
                        UserDefaults.init(suiteName: CallKitInfo.suite_name)?.setValue("kt", forKey: "mobileType")
                    case .skt:
                        replaceMotype = "*281 "+replaceMotype
                        UserDefaults.standard.set("skt", forKey: .mobileType)
                        UserDefaults.init(suiteName: CallKitInfo.suite_name)?.setValue("skt", forKey: "mobileType")
                    case .uplus:
                        replaceMotype = replaceMotype+"*"
                        UserDefaults.standard.set("uplus", forKey: .mobileType)
                        UserDefaults.init(suiteName: CallKitInfo.suite_name)?.setValue("uplus", forKey: "mobileType")
                        
                }
                /*규칙 적용*/
                RealmManager.shared.updateContact(seq: el.seq, name: el.name, phone: replaceMotype, memo: el.memo, completion: { (_) in
                    dispatchSemaphore.signal()
                    dispatchGroup.leave()
                    
                })
                dispatchSemaphore.wait()
            }
            
        }

        if dataArray.isEmpty {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.showFinishedAlert()
                SVProgressHUD.dismiss()
            }
            return
        }
        
        dispatchGroup.notify(queue: dispatchQueue) {
            let userInfo: [String: String] = ["refresh":"refresh"]
            NotificationCenter.default.post(name: Notification.Name.refreshAddrList, object: nil, userInfo: userInfo)
            guard let purchase = IAPManager.shared.getPurchaseStatus() else { return }
            if purchase{
                Toast.makeToast(message: "동기화가 시작되었습니다\n완료 시 푸시알람이 발송됩니다", controller: self)
            }
            
            CallKitManager.shared.saveCallKitData(success: nil, failure: nil)
                self.showFinishedAlert()
                SVProgressHUD.dismiss()
            
        }
        
    }
    
    private func showFinishedAlert() {
        let title = "연락처 변경"
        let message = "모든 연락처가 변경되었습니다"
        
        self.displayAlertMessage(
            title,
            message: message,
            preferredStyle: .alert,
            actionStyleType: .cancel,
            isAction: false
        )
    }
    
    func deleteAllCoreData(_ type:ContactManagers){
        
        let title = TextInfo.alert_delete_contacts_title
        let msg = TextInfo.alert_delete_all_contacts
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "삭제", style: .destructive) { action in
            SVProgressHUD.show()
            RealmManager.shared.deleteAllContacts(completion: { (success) in
                SVProgressHUD.dismiss()
                
                if success{
                    switch type{
                        case .delete:
                            let title = "삭제 완료"
                            let message = "모든 연락처가 삭제 되었습니다."
                            let okAction = UIAlertAction(title: TextInfo.alert_confirm, style: .default, handler: {action in
                                let userInfo: [String: String] = ["refresh":"refresh"]
                                NotificationCenter.default.post(name: Notification.Name.refreshAddrList, object: nil, userInfo: userInfo)
                                guard let purchase = IAPManager.shared.getPurchaseStatus() else { return }
                                if purchase{
                                    Toast.makeToast(message: "동기화가 시작되었습니다\n완료 시 푸시알람이 발송됩니다", controller: self)
                                }
                                CallKitManager.shared.saveCallKitData(success: nil, failure: nil)
                            })
                            UIAlertController.showAlert(title: title, message: message, actions: [okAction])
                            
                        case .addAll:
                            self.getAllContactsFromDevice()
                    }
                }
                else{
                    let title = "삭제 실패"
                    let message = "연락처 삭제에 실패하였습니다"
                
                    self.displayAlertMessage(
                        title,
                        message: message,
                        preferredStyle: .alert,
                        actionStyleType: .cancel,
                        isAction: false
                    )
                    
                    
                }
            })
            
        })
        
        alert.addAction(UIAlertAction(title: "취소", style: .default,handler: nil))
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.present(alert, animated: true, completion: nil)
        }
        
    }
}


// action
extension SettingVC{
    
    func showReviewAlert(){
        let title = "앱 리뷰하기"
        let msg = String(format: "%@를 이용해 주셔서 감사합니다\n여러분의 소중한 리뷰가 힘이됩니다\n앱스토어로 이동할까요?", SystemUtils.shared.appName())
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: TextInfo.alert_cancel, style: .cancel, handler: nil))
        
        alert.addAction(UIAlertAction(title: TextInfo.alert_confirm, style: .default) { action in
            
            let appLinkUrl = "itms-apps://itunes.apple.com/app/id\(AppInfo.appId)?action=write-review"
            
            let URL = NSURL(string : appLinkUrl)!
            UIApplication.shared.open(URL as URL, options: [:], completionHandler: nil)
            
        })
        
        
        self.present(alert, animated: true, completion: nil)
    }
    
    /* 경고 출력 */
    func displayAlertMessage(_ title: String, message: String, preferredStyle: UIAlertController.Style, actionStyleType: UIAlertAction.Style, isAction: Bool) {
        
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
        
        let dismissAction = UIAlertAction(title: TextInfo.alert_confirm, style: actionStyleType) { (action) -> Void in
            if isAction == true {
                //                self.navigationController?.popViewController(animated: true)
                self.dismiss(animated: true, completion: nil)
                return
            }else{
                return
            }
        }
        
        
        alertController.addAction(dismissAction)
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.present(alertController, animated: true, completion: nil)
        }
    }
    /* 경고 출력 */
    
}

struct ContactInfo {
    var name: String
    var phone: String
}

extension SettingVC{
    private func getAllContactsFromDevice(){
        SVProgressHUD.show()

//        var count = 0
        var phoneNumberArr: [ContactInfo] = []
        
        guard let defaultMotype = UserDefaults.standard.string(forKey: .mobileType) else { return }
        let moType:MobileType = MobileType(rawValue: defaultMotype) ?? MobileType.kt
        
        let contactStore = CNContactStore()
        var contacts = [CNContact]()
        let keys = [
            CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
            CNContactPhoneNumbersKey,
            CNContactEmailAddressesKey
        ] as [Any]
        
        let request = CNContactFetchRequest(keysToFetch: keys as! [CNKeyDescriptor])
        do {
            try contactStore.enumerateContacts(with: request) {
                (contact, stop) in
                // Array containing all unified contacts from everywhere
                contacts.append(contact)
//                var phone = ((contact.phoneNumbers[0].value).value(forKey: "digits") as? String)!
                if let phone = contact.phoneNumbers.filter({ !$0.value.stringValue.isEmpty }).first?.value.stringValue {
                    let name = contact.givenName + "" + contact.familyName
                    
                    let first = phone.components(separatedBy: "-").first
                    let converType: ConvertType = (first == "010") ? .phone : .tel
                    let resultPhoneNumber = ContactsConvertManager.shared.registerNewContact(convertType: converType, phone: phone, moType: moType)
                    let queContact = ContactInfo(name: name, phone: resultPhoneNumber)
                    phoneNumberArr.append(queContact)
                    Debug.print(queContact)
                }
                
            }
            //            print(contacts)
        } catch let (error){
            self.askForContactAccess()
            Debug.print("\(error.localizedDescription)")
        }
        
        self.saveAllContactsToApp(phoneNumberArr: phoneNumberArr)
        
    }
    
    private func saveAllContactsToApp(phoneNumberArr: [ContactInfo]) {
        let dispatchGroup = DispatchGroup()
        let dispatchQueue = DispatchQueue(label: "Realm")
        let dispatchSemaphore = DispatchSemaphore(value: 100)
        
        dispatchQueue.async(group: dispatchGroup) {
            phoneNumberArr.forEach { contact in
                dispatchGroup.enter()
                RealmManager.shared.createContatct(
                    name: contact.name,
                    phone: contact.phone,
                    memo: "",
                    completion: { (success, mergedName, model) in
//                        if success{
//                            count += 1
//                        }
                        dispatchSemaphore.signal()
                        dispatchGroup.leave()
                    })
                dispatchSemaphore.wait()
            }
            
        }

        dispatchGroup.notify(queue: dispatchQueue) {
            let userInfo: [String: String] = ["refresh":"refresh"]
            NotificationCenter.default.post(name: Notification.Name.refreshAddrList, object: nil, userInfo: userInfo)
//            print(debug: count)
            UIAlertController.showMessage("모든 연락처가 동기화되었습니다")
            SVProgressHUD.dismiss()
        }
        
        if phoneNumberArr.isEmpty {
            SVProgressHUD.dismiss()
        }
    }
    
    /* 연락처 접근 */
    func askForContactAccess() {
        let authorizationStatus = CNContactStore.authorizationStatus(for: CNEntityType.contacts)
        
        switch authorizationStatus {
            case .denied, .notDetermined:
                self.contactStore.requestAccess(for: CNEntityType.contacts, completionHandler: { (access, accessError) -> Void in
                    if !access {
                        if authorizationStatus == CNAuthorizationStatus.denied {
                            DispatchQueue.main.async { [weak self] in
                                guard let self = self else { return }
                                let message = "\(accessError!.localizedDescription)\n\n기능사용을 위해 설정에서 접근권한을 허용하세요.\n[설정 -> 투넘버 문자 -> 연락처 설정 On]"
                                let alertController = UIAlertController(title: "연락처", message: message, preferredStyle: UIAlertController.Style.alert)
                                
                                let dismissAction = UIAlertAction(title: "확인", style: UIAlertAction.Style.default) { (action) -> Void in
                                }
                                
                                alertController.addAction(dismissAction)
                                
                                self.present(alertController, animated: true, completion: nil)
                            }
                        }
                    }
                })
                break
            default:
                break
        }
    }
    
    
    
}


// event bus
extension SettingVC{
    private func registerNotification() {

    }
    
    // unregister notification
    private func unregisterNotification() {
        
    }
}

//@objc func
extension SettingVC{
    
    
    @objc func goToCallKitTuto(){
        let tutorialVC = TutorialCollectionVC(collectionViewLayout: UICollectionViewFlowLayout())
        tutorialVC.tutorialType = .callKit
        let navigationController = UINavigationController(rootViewController: tutorialVC)
        tutorialVC.modalTransitionStyle = .crossDissolve
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @objc func goToPremiumTuto(){
        let tutorialVC = TutorialCollectionVC(collectionViewLayout: UICollectionViewFlowLayout())
        tutorialVC.tutorialType = .premium
        let navigationController = UINavigationController(rootViewController: tutorialVC)
        tutorialVC.modalTransitionStyle = .crossDissolve
        self.present(navigationController, animated: true, completion: nil)
    }
}



