//
//  NonSavePopUpVC.swift
//  twoNumSender
//
//  Created by seungmin kim on 08/06/2019.
//  Copyright © 2019 remain. All rights reserved.
//

import UIKit
import MessageUI
import SVProgressHUD

enum NonSaveItmes{
    case description
    case phone
    case btnSend
    case request
    case help
}

class NonSavePopUpVC: UIViewController, UITextFieldDelegate,UITextViewDelegate,MFMessageComposeViewControllerDelegate  {

    var phoneTextTF = ""
    var checkString = ""
    var array = [String]()
    
    var replace1 = ""
    var replace2 = ""
    var replace3 = ""
    
    
    var replace2Sub = ""
    var replace3Sub = ""
    var replace4Sub = ""
    var replace5Sub = ""
    var replace6Sub = ""
    
    var replace010Sub = ""
    var msgType = Int()
    
    var phoneTF = ""
    let loadingSize = CGSize(width: 60, height:60)
    let moType = UserDefaults.standard.string(forKey: .mobileType)!
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var tableView: UITableView!
    let itemArray:[NonSaveItmes] = [.description, .phone, .btnSend, .request, .help]
    override func viewDidLoad() {
        
        super.viewDidLoad()
        view.backgroundColor = UIColor.clear
        backgroundView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        view.isOpaque = false
        self.registerCell()
        let backgroundViewTarget = UITapGestureRecognizer(target: self, action: #selector(actionPop))
        self.backgroundView.addGestureRecognizer(backgroundViewTarget)
		self.tableView.toCornerRound(radius: 16.0)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.phoneTF = ""
        self.tableView.reloadData()
    }

}
extension NonSavePopUpVC:  UITableViewDataSource, UITableViewDelegate{
    private func registerCell() {
        tableView.dataSource = self
        tableView.delegate = self
        self.tableView.tableFooterView = UIView()
        self.tableView.rowHeight = UITableView.automaticDimension
                self.tableView.estimatedRowHeight = 90
        self.tableView.register(UINib(nibName: String.init(describing: RegistContactsTFCell.self), bundle: nil), forCellReuseIdentifier: String.init(describing: RegistContactsTFCell.self))
        self.tableView.register(UINib(nibName: String.init(describing: RegistContactButtonCell.self), bundle: nil), forCellReuseIdentifier: String.init(describing: RegistContactButtonCell.self))
        self.tableView.register(UINib(nibName: String.init(describing: NonSaveDescriptionCell.self), bundle: nil), forCellReuseIdentifier: String.init(describing: NonSaveDescriptionCell.self))
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.itemArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat { // row 높이 지정
        let row = self.itemArray[indexPath.row]
        switch row {
        case .description:
            return 80
        case .phone:
            return RegistContactsTFCell.cellHeight()
        case .btnSend:
            return RegistContactButtonCell.cellHeight()
        case .request:
            return RegistContactButtonCell.cellHeight()
        case .help:
            return 80
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: UITableViewCell
        let row = self.itemArray[indexPath.row]
        
        switch row {
        case .description:
            let descCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: NonSaveDescriptionCell.self)) as! NonSaveDescriptionCell
            descCell.descriptionLabel.text = "번호저장 없이 메시지를 전송할 수 있습니다\n배경을 눌러 이전 화면으로 돌아갈 수 있습니다"
            descCell.selectionStyle             = .none
            cell = descCell
        case .phone:
            let tfCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: RegistContactsTFCell.self)) as! RegistContactsTFCell
            tfCell.regContactsTitleLabel.text = "연락처 : "
            tfCell.regContactsTF.placeholder  = "01012341234"
            tfCell.regContactsTF.keyboardType = .phonePad
            tfCell.selectionStyle             = .none
            cell = tfCell
        case .btnSend:
            let btnCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: RegistContactButtonCell.self)) as! RegistContactButtonCell
            btnCell.registContactBtnTitle.text = "문자 보내기"
            btnCell.registContactImageView.image = UIImage(named: "img_nonSave")
            btnCell.selectionStyle             = .default
            cell = btnCell
        case .request:
            let btnCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: RegistContactButtonCell.self)) as! RegistContactButtonCell
            btnCell.registContactBtnTitle.text = "수신 문자 확인"
            
            btnCell.registContactImageView.image = UIImage(named: "img_recieve")
            btnCell.selectionStyle             = .default
            cell = btnCell
            
        case .help:
            let descCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: NonSaveDescriptionCell.self)) as! NonSaveDescriptionCell
            descCell.selectionStyle             = .none
            descCell.descriptionLabel.text = "*01012345678* 형태가 아닌 경우\n정상적으로 동작하지 않을 수 있습니다"
            
            cell = descCell
        }
        
        return cell
    }
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // touch up 되었을 경우 선택 해제.
        tableView.deselectRow(at: indexPath, animated: false)
        let row = self.itemArray[indexPath.row]
        
        switch row {
        case .description:
            break
        case .phone:
            break
        case .btnSend:
            self.setInputData()
            self.sendButtonTapped()
        case .request:
            self.setInputData()
            self.recieveButtonTapped()
        case .help:
            break
        }
        
    }
}


extension NonSavePopUpVC{
    func convertNumber(){
        
        phoneTextTF = self.phoneTF
        
        replace1 = phoneTextTF.replacingOccurrences(of: "-", with: "", options: .literal, range: nil)
        replace2 = replace1.replacingOccurrences(of: " ", with: "", options: .literal, range: nil)
        replace3 = replace2.replacingOccurrences(of: "+8210", with: "", options: .literal, range: nil)
        checkString = replace3.replacingOccurrences(of: "+82", with: "", options: .literal, range: nil)
        
        if checkString.hasPrefix("8210") {
            checkString = String(checkString.dropFirst(4))
        }
        
        
        if checkString.range(of: "*77") != nil || checkString.range(of: "*281") != nil{
            
            if checkString.range(of: "*77010") != nil{
                replace2Sub = checkString.replacingOccurrences(of: "*77010", with: "")
                array.append(replace2Sub)
                
            }else if checkString.range(of: "*7710") != nil{
                replace3Sub = checkString.replacingOccurrences(of: "*7710", with: "")
                array.append(replace3Sub)
                //
            }else if checkString.range(of: "*281010") != nil{
                replace4Sub = checkString.replacingOccurrences(of: "*281010", with: "")
                array.append(replace4Sub)
                
            }else if checkString.range(of: "*28110") != nil{
                replace5Sub = checkString.replacingOccurrences(of: "*28110", with: "")
                array.append(replace5Sub)
                
            }else if checkString.range(of: "*28182") != nil{
                replace6Sub = checkString.replacingOccurrences(of: "*28182", with: "")
                array.append(replace6Sub)
                
            }else{
                array.append(checkString)
            }
            //    print(array)
            
        }else{
            if checkString.range(of: "010") != nil{
                
                replace010Sub = checkString.replacingOccurrences(of: "010", with: "")
                array.append(replace010Sub)
                
            }else{
                array.append(checkString)
            }
            self.phoneTF = array[0]
        }
        Debug.print(msgType)
        
        let moType = self.moType
        var editText = ""
        
        
        switch moType {
        case "kt":
            
            
            editText = "*77 010\(array[0])"
            self.phoneTF = editText
            
        case "skt":
            
            
            editText = "*281 010\(array[0])"
            self.phoneTF = editText
            
        case "uplus":
            
            
            editText = "010\(array[0])*"
            self.phoneTF = editText
        default:
            break
        }
        
        if msgType == 1{
            self.recieveToMsg()
        }else{
            self.sendMsg()
        }
        
        
    }
    
    func recieveButtonTapped() {
        if self.phoneTF == ""{
            alertView()
        }else{
            self.msgType = 1
            convertNumber()
        }
    }
    
    @objc func applicationDidBecomeActive(){
        self.phoneTF = ""
        self.tableView.reloadData()
    }
    
    func sendButtonTapped() {
        if self.phoneTF == ""{
            alertView()
        }else{
            self.msgType = 2
            convertNumber()
        }
        
        
        
    }
    
    /*번호 유효성*/
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let phoneIndex = IndexPath(row: 0, section: 0)
        let phoneCell: RegistContactsTFCell = self.tableView.cellForRow(at: phoneIndex) as! RegistContactsTFCell
        
        
        if (textField == phoneCell.regContactsTF) {
            
            let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let components = (newString as NSString).components(separatedBy: NSCharacterSet.decimalDigits.inverted)
            
            let decimalString = components.joined(separator: "") as NSString
            let length = decimalString.length
            let hasLeadingOne = length > 0 && decimalString.character(at: 0) == (1 as unichar)
            
            if length == 0 || (length > 11 && !hasLeadingOne) || length > 11 {
                let newLength = (textField.text! as NSString).length + (string as NSString).length - range.length as Int
                
                return (newLength > 11) ? false : true
            }
            var index = 0 as Int
            let formattedString = NSMutableString()
            
            if hasLeadingOne {
                formattedString.append("")
                index += 0
            }
            if (length - index) > 3 {
                let areaCode = decimalString.substring(with: NSMakeRange(index, 3))
                formattedString.appendFormat("%@-", areaCode)
                index += 3
            }
            if (length - index) > 4 {
                let areaCode = decimalString.substring(with: NSMakeRange(index, 4))
                formattedString.appendFormat("%@-", areaCode)
                index += 4
            }
            if length - index > 4 {
                let prefix = decimalString.substring(with: NSMakeRange(index, 4))
                formattedString.appendFormat("%@-", prefix)
                index += 4
            }
            
            let remainder = decimalString.substring(from: index)
            formattedString.append(remainder)
            textField.text = formattedString as String
            return false
            
        } else {
            return true
        }
    }
    
    /*번호 유효성*/
}


extension NonSavePopUpVC{
    /* 수신 문자 목록 */
    func recieveToMsg(){

        
        if MFMessageComposeViewController.canSendText(){
            
            let controller = MFMessageComposeViewController()
            
            var num = String()
            let phoneIndex = IndexPath(row: 1, section: 0)
            let phoneCell: RegistContactsTFCell = self.tableView.cellForRow(at: phoneIndex) as! RegistContactsTFCell
            num = phoneCell.regContactsTF.text!
            
            var subString = ""
            let newString = num.replacingOccurrences(of: " ", with: "", options: .literal, range: nil)
            var finalNum = ""
            if (newString.range(of: "*77") != nil){
                //        if moType == "kt"{
                
                subString = newString.replacingOccurrences(of: "*77", with: "", options: .literal, range: nil)
                
                finalNum = subString
                
            }else if (newString.range(of: "*281") != nil){
                //        }else if moType == "skt"{
                subString = newString.replacingOccurrences(of: "*281", with: "", options: .literal, range: nil)
                
                finalNum = subString
            }else{
                subString = newString.replacingOccurrences(of: "*", with: "", options: .literal, range: nil)
                finalNum = subString
            }
            //            print(finalNum)
            
            controller.body = ""
            // 받는 사람 지정
            controller.recipients = ["\(finalNum)"]
            controller.messageComposeDelegate = self
            
            self.present(controller, animated: true, completion: nil)
        }
        
    }
    
    
    
    /* 수신 문자 목록 */
    
    /* 문자 발신*/
    func sendMsg(){

        if MFMessageComposeViewController.canSendText(){
            
            let controller = MFMessageComposeViewController()
            
            controller.body = ""
            // 받는 사람 지정
            var sendNum = ""
            
            let phoneIndex = IndexPath(row: 1, section: 0)
            let phoneCell: RegistContactsTFCell = self.tableView.cellForRow(at: phoneIndex) as! RegistContactsTFCell
            sendNum = phoneCell.regContactsTF.text!
            switch self.moType{
            case "kt":
                controller.recipients = ["!*77\(sendNum)"]
            case "skt":
                let convertNum = sendNum.replacingOccurrences(of: "010", with: "*281 8210", options: .literal, range: nil)
                controller.recipients = ["\(convertNum)"]
            case "uplus":
                controller.recipients = ["\(sendNum)*"]
            default:
                break
            }
            
            controller.messageComposeDelegate = self
            
            self.present(controller, animated: true, completion: nil)
            
        }
        
    }
    /* 문자 발신*/
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)
    }
    /* 메시지 ui 출력 */
}

// textfield
extension NonSavePopUpVC{
    
    /* 화면 외부 누르면 키보드 숨김 */
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        
    }
    /* 화면 외부 누르면 키보드 숨김 */
    
    func setInputData(){
        
        let phoneIndex = IndexPath(row: 1, section: 0)
        let phoneCell: RegistContactsTFCell = self.tableView.cellForRow(at: phoneIndex) as! RegistContactsTFCell
        self.phoneTF = phoneCell.regContactsTF.text!

    }
}

//alart
extension NonSavePopUpVC{
    func alertView(){
        let title = "연락처 확인"
        let msg = "연락처는 필수 항목입니다"
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
            self.dismiss(animated: true, completion: nil)
            
        }
        self.present(alert, animated: true, completion: nil)
    }
    @objc func actionPop(recognizer: UITapGestureRecognizer) {
        //        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }

}

extension NonSavePopUpVC{
    private func registerNotification() {
        // event bus
        
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        
        
    }
    private func unregisterNotification() {
        // event bus
        NotificationCenter.default.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)
        
    }
}
