//
//  MainTabBarVC.swift
//  twoNumSender
//
//  Created by seungmin kim on 07/06/2019.
//  Copyright © 2019 remain. All rights reserved.
//

import UIKit
import SVProgressHUD

class MainTabBarVC: UITabBarController {

    var newLoad:Bool = true

    override func viewDidLoad() {
        super.viewDidLoad()
		self.view.backgroundColor = .white
        self.drawTabBar()
        // title
//        self.selectedIndex = 1
        
        self.registerNotification()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		self.navigationController?.navigationBar.barStyle = .default
	
        self.drawNavigationBar()
        self.registerNotification()
		self.setKeyBoardManager()
    }
	
    deinit {
        Debug.print("")
    }
    
    override func viewDidAppear(_ animated: Bool) {
		
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            self.applicationDidBecomeActive()
			
        }
        
    }
    

    @objc func applicationDidBecomeActive(){
        // 화면 복귀 시 재 체크
        DispatchQueue.main.async { [weak self] in
            guard let self = self else {
                return
            }
            if let deeplinkData = DeepLinkManager.shared.getDeeplinkInfo() {
                self.registAddrFromContacts(tel: deeplinkData.tel, name: deeplinkData.name)
                DeepLinkManager.shared.clear()
            }
            
        }
    }

    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        let indexOfTab = tabBar.items?.firstIndex(of: item)
        let userContacts = UserDefaults.standard.string(forKey: .userContacts)
        if indexOfTab == 0{
            self.title = userContacts
            
        }else{
            self.title = item.title
        }
        
    }
}
//MARK: private
extension MainTabBarVC {
    
    private func drawNavigationBar() {
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.prefersLargeTitles = false
        
        // 네비 하단바 삭제
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        // set left button
        // padding
        let leftButton = UIButton.init(type: .custom)
        leftButton.setImage(UIImage(named: "img_nonSave"), for: .normal)
        leftButton.frame = CGRect(x: 6, y: 0, width: 44, height: 44)
        leftButton.addTarget(self, action: #selector(goToNonSaveVC), for: .touchUpInside)
        let leftBarButtonItem: UIBarButtonItem = UIBarButtonItem(customView: leftButton)
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
        
        
        //set right button
        let rightButton = UIButton.init(type: .custom)
        rightButton.frame = CGRect(x: 6, y: 0, width: 44, height: 44)
        rightButton.setImage(UIImage(named: "img_register"), for: .normal)
        rightButton.addTarget(self, action: #selector(goToRegistAddrVC), for: .touchUpInside)
        let rightBarButtonItem: UIBarButtonItem = UIBarButtonItem(customView: rightButton)
        self.navigationItem.rightBarButtonItem = rightBarButtonItem

		self.navigationController?.setNavigationBarHidden(true, animated: true)

    }
    
    private func drawTabBar() {
        // font
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13)], for: .normal)
        self.tabBar.tintColor = UIColor.white // tabbar item font color
        self.tabBar.unselectedItemTintColor = UIColor.gray
        self.tabBar.backgroundColor = UIColor(hexString: ColorInfo.MAIN)
        
        // background
        self.tabBar.isTranslucent = false
		self.tabBar.barTintColor = UIColor(hexString: ColorInfo.MAIN)
        
        // 연락처
		let addrVC = MainAddrVC()

        addrVC.tabBarItem = UITabBarItem(title: TextInfo.title_addr, image: UIImage(named: "img_contacts"), selectedImage: nil)
        addrVC.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0.0, vertical: 0.0)
        
        // 프리미엄
        let premiumVC = PremiumVC()
        premiumVC.tabBarItem = UITabBarItem(title: TextInfo.title_premium, image: UIImage(named: "img_premium"), selectedImage: nil)
        premiumVC.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0.0, vertical: 0.0)
        
        
        // 설정
        let settingVC = SettingVC()
        settingVC.tabBarItem = UITabBarItem(title: TextInfo.title_setting, image: UIImage(named: "img_setting"), selectedImage: nil)
        settingVC.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0.0, vertical: 0.0)
        
        
        let controllers = [addrVC, premiumVC ,settingVC]
        self.viewControllers = controllers
    }
    
    // Register Notification
    private func registerNotification(){
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive),
        name: UIApplication.didBecomeActiveNotification,
        object: nil)
    
    }
    
	private func setKeyBoardManager(){
		if #available(iOS 11.0, *) {

			let bottomSafeArea = UIApplication.topViewController()?.view.safeAreaInsets.bottom ?? 0.00
			KeyboardManager.shared.setBottomSafeAreaHeight(height: bottomSafeArea)
			 
		}
		KeyboardManager.shared.setup()
	}

	
}

extension MainTabBarVC{
    
    @objc func goToNonSaveVC(){
        self.navigationController?.definesPresentationContext = false
        let vc = NonSavePopUpVC()
        let transition = CATransition()
        transition.duration = 0.25
        transition.type = CATransitionType.fade
        //        transition.subtype = CATransitionSubtype.fromTop
        self.view.window!.layer.add(transition, forKey: kCATransition)
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: true)
    
    }
    
    @objc func goToRegistAddrVC(){

        var params: [ViewValueKeys: Any] = [:]
        params[.isRegist] = true
        
        RouterManager.shared.dispatcher(.addrDetail, values: params)
        
    }
    
    
    @objc func registAddrFromContacts(tel: String, name: String) {
 
        DispatchQueue.main.async { [weak self] in
			guard let self = self else {
				return
			}
            
			self.navigationController?.popToRootViewController(animated: true)
            
            var params: [ViewValueKeys: Any] = [:]
            params[.expactContactModel] = ContactModel(
                seq: 0,
                name: name,
                phone: tel,
                memo: ""
            )
            params[.isRegist] = true
            
            RouterManager.shared.dispatcher(.addrDetail, values: params)

        }
            
    }
}

// alert
extension MainTabBarVC{
    
}

