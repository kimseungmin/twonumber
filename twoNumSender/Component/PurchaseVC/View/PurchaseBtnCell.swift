//
//  PurchaseBtnCell.swift
//  twoNumSender
//
//  Created by dkt_mac on 12/06/2019.
//  Copyright © 2019 remain. All rights reserved.
//

import UIKit

class PurchaseBtnCell: UITableViewCell {

    
    @IBOutlet weak var btnView: UIView!
    @IBOutlet weak var purchaseTypeLabel: UILabel!
    @IBOutlet weak var purchaseValueLabel: UILabel!
    @IBOutlet weak var purchaseTypeInfoLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		self.btnView.toCornerRound(radius: 8.0)
        self.purchaseTypeInfoLabel.text = TextInfo.purchase_info1 + TextInfo.purchase_info2
        self.purchaseTypeInfoLabel.customLabel(string: TextInfo.purchase_info1, withColor: UIColor.black, underLine: false, bold: false)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}


//extension PurchaseBtnCell{
//    class func cellHeight() -> CGFloat {
//        return 62
//    }
//}
