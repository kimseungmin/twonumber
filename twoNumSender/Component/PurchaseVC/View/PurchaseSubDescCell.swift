//
//  PurchaseSubDescCell.swift
//  twoNumSender
//
//  Created by dkt_mac on 12/06/2019.
//  Copyright © 2019 remain. All rights reserved.
//

import UIKit

class PurchaseSubDescCell: UITableViewCell {

    @IBOutlet weak var descLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.descLabel.text = ""
        
        self.descLabel.sizeToFit()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
