//
//  PurchaseMainVC.swift
//  twoNumSender
//
//  Created by dkt_mac on 12/06/2019.
//  Copyright © 2019 remain. All rights reserved.
//

import UIKit
import SVProgressHUD

enum PurchaseCellTypes{
    case mainDesc
    case background
    case monthBtn
    case yearBtn
    case restore
    case subDesc
    case term
}

class PurchaseMainVC: UITableViewController {
    
    let purchaseCells:[PurchaseCellTypes] = [.mainDesc, .background ,.yearBtn, .monthBtn, .restore, .subDesc, .term]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.drawNavigationBar()
        self.registerCell()
    
    }
    
    class TermTapGesture: UITapGestureRecognizer {
        var type = String()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.purchaseCells.count
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat { // row 높이 지정
        let row = self.purchaseCells[indexPath.row]
        var cellHeight:CGFloat
        switch row {
        case .mainDesc, .subDesc:
            cellHeight = UITableView.automaticDimension
        case .monthBtn, .yearBtn:
            cellHeight = UITableView.automaticDimension//PurchaseBtnCell.cellHeight()
        case .background:
            cellHeight = UITableView.automaticDimension
        case .restore:
            cellHeight = UITableView.automaticDimension
        case .term:
            cellHeight = UITableView.automaticDimension
        }
        return cellHeight
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell
        
        let row = self.purchaseCells[indexPath.row]
        
        switch row {
        case .mainDesc:
            let mainDescCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: PurchaseMainDescCell.self)) as! PurchaseMainDescCell
            
            cell = mainDescCell
            
        case .background:
            let bgCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: PurchaseBackgroundCell.self)) as! PurchaseBackgroundCell
            
            cell = bgCell
            
            
        case .monthBtn:
            let btnCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: PurchaseBtnCell.self)) as! PurchaseBtnCell
            btnCell.purchaseTypeLabel.text  = TextInfo.purchase_month_btn_status
            btnCell.purchaseTypeLabel.textColor = UIColor.white
            btnCell.purchaseValueLabel.text = TextInfo.purchase_month_btn_value
            btnCell.purchaseValueLabel.textColor = UIColor.white
			btnCell.btnView.backgroundColor = UIColor(hexString: ColorInfo.MAIN)
            btnCell.btnView.outline(borderWidth: 0, borderColor: UIColor.white, opacity: 1.0)
            btnCell.purchaseTypeInfoLabel.text = TextInfo.purchase_info_month
            
            let monthTapped = UITapGestureRecognizer(target: self, action: #selector(actionBuyMonth))
            btnCell.btnView.addGestureRecognizer(monthTapped)
            
            
            cell = btnCell
        case .yearBtn:
            let btnCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: PurchaseBtnCell.self)) as! PurchaseBtnCell
            btnCell.purchaseTypeLabel.text  = TextInfo.purchase_year_btn_status
            btnCell.purchaseTypeLabel.textColor = UIColor.black
            btnCell.purchaseValueLabel.text = TextInfo.purchase_year_btn_value
            btnCell.purchaseValueLabel.textColor = UIColor.black
            btnCell.btnView.backgroundColor = UIColor.white
			btnCell.btnView.outline(borderWidth: 1, borderColor: UIColor(hexString: ColorInfo.MAIN), opacity: 1.0)
            btnCell.purchaseTypeInfoLabel.isHidden = false
            
            let monthTapped = UITapGestureRecognizer(target: self, action: #selector(actionBuyYear))
            btnCell.btnView.addGestureRecognizer(monthTapped)
            cell = btnCell
        
        case .restore:
            let restoreCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: PurchaseMainDescCell.self)) as! PurchaseMainDescCell
            restoreCell.descLabel.text = TextInfo.purchase_restore
            restoreCell.descLabel.customLabel(string: "여기", withColor: UIColor.blue, underLine: true, bold: false)
            restoreCell.descLabel.font = UIFont.systemFont(ofSize: 16)
            cell = restoreCell
            
            let restoreTapped = UITapGestureRecognizer(target: self, action: #selector(actionRestore))
            restoreCell.descLabel.isUserInteractionEnabled = true
            restoreCell.descLabel.addGestureRecognizer(restoreTapped)

            
        case .subDesc:
            let subDescCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: PurchaseSubDescCell.self)) as! PurchaseSubDescCell
            subDescCell.descLabel.text = TextInfo.purchase_sub_desc
            
            cell = subDescCell
            
        case .term:
            let termLinkCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: PurchaseMainDescCell.self)) as! PurchaseMainDescCell
            
            termLinkCell.descLabel.text = "서비스 이용약관 보기"
            termLinkCell.descLabel.customLabel(string: termLinkCell.descLabel.text!, withColor: UIColor.blue, underLine: true, bold: false)
            termLinkCell.descLabel.font = UIFont.systemFont(ofSize: 14)
            let tapped = TermTapGesture(target: self, action: #selector(actionTerm(sender:)))
            tapped.type = "term"
            termLinkCell.descLabel.isUserInteractionEnabled = true
            termLinkCell.descLabel.addGestureRecognizer(tapped)
            
            
            cell = termLinkCell
            
        }
        

        // Configure the cell...

        return cell
    }
    
//    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let row = self.purchaseCells[indexPath.row]
//
//        switch row{
//
//        case .mainDesc:
//            break
//        case .background:
//            break
//        case .monthBtn:
//            break
//        case .yearBtn:
//            break
//        case .restore:
//            break
//        case .subDesc:
//            break
//        case .term:
//            self.selectTermType()
//        }
//    }
//
    
    
}
extension PurchaseMainVC{
    private func registerCell() {
        self.tableView.allowsSelection = false
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 90
        
        self.tableView.register(UINib(nibName: String.init(describing: PurchaseMainDescCell.self), bundle: nil), forCellReuseIdentifier: String.init(describing: PurchaseMainDescCell.self))
        
        self.tableView.register(UINib(nibName: String.init(describing: PurchaseSubDescCell.self), bundle: nil), forCellReuseIdentifier: String.init(describing: PurchaseSubDescCell.self))
        
        self.tableView.register(UINib(nibName: String.init(describing: PurchaseBtnCell.self), bundle: nil), forCellReuseIdentifier: String.init(describing: PurchaseBtnCell.self))
        
        self.tableView.register(UINib(nibName: String.init(describing: PurchaseBackgroundCell.self), bundle: nil), forCellReuseIdentifier: String.init(describing: PurchaseBackgroundCell.self))
        
        
    }
    
}

extension PurchaseMainVC{
    private func drawNavigationBar() {
        
        self.navigationController?.navigationBar.isTranslucent = false          // 투명도 설정
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        
        self.navigationController?.navigationBar.shadowImage = UIImage()        // 네비 하단바 삭제 ?
        
        
        self.title = TextInfo.title_purchase
		self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(hexString: ColorInfo.TOPBAR)]
        self.navigationController?.navigationBar.backgroundColor = .white
        
        // set right bar - 메인화면으로 이동
        let rightBarImage = UIImage(named: "icon_navbar_close_sub")?.withRenderingMode(UIImage.RenderingMode.automatic)
        let rightBarButtonItem =  UIBarButtonItem(image: rightBarImage,
                                                  style: UIBarButtonItem.Style.done,
                                                  target: self,
                                                  action: #selector(actionPop))
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
		self.navigationItem.rightBarButtonItem?.tintColor = UIColor.init(hexString: ColorInfo.MAIN)
        
    }
    
    
}

// @objc
extension PurchaseMainVC{

    @objc func actionBuyMonth(sender: UITapGestureRecognizer){
        IAPManager.shared.purchase(.autoRenewableMonthly, atomically: true)
//        InAppPurchase.sharedInstance.buyMonthPromoSubscription()
    }
    
    @objc func actionBuyYear(sender: UITapGestureRecognizer){
        IAPManager.shared.purchase(.autoRenewableYearly, atomically: true)
//        InAppPurchase.sharedInstance.buyYearPromoSubscription()
    }
    
    @objc func actionRestore(sender: UITapGestureRecognizer){
        IAPManager.shared.restore()
//        InAppPurchase.sharedInstance.restoreTransactions("복원 진행 중", indicator: true)
    }
    
    @objc func actionTerm(sender: TermTapGesture) {
        Debug.print(sender.type)
        if sender.type == "term"{
            self.selectTermType()
        }else{
            return
        }
        
    }
    
    func selectTermType(){
        let alert = UIAlertController(title: "약관 보기", message: "약관을 선택 해 주세요", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "서비스 이용약관", style: .default) { action in
            self.moveToTerm()
        })
        
        alert.addAction(UIAlertAction(title: "개인정보 처리방침", style: .default) { action in
            self.moveToPolicy()
        })
        
        alert.addAction(UIAlertAction(title: "닫기", style: .default) { action in
            
        })
        
        self.present(alert, animated: true, completion: nil)
    }

}

// move Page
extension PurchaseMainVC{
    func moveToTerm(){
//        let vc   = TermVC()
//        let navigationController = UINavigationController(rootViewController: vc)
//        vc.termText = TermInfo.service_term
//        vc.termTitle = TextInfo.title_serviceTerm
//
//        self.present(navigationController,animated: true, completion: nil)
        let vc   = CommonWebViewVC()
        let navigationController = UINavigationController(rootViewController: vc)
        vc.landingUrl = "https://smbh.kr/twonum_terms/index.html"
        vc.vcTitle = TextInfo.title_serviceTerm
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController,animated: true, completion: nil)
    }
    
    func moveToPolicy(){
//        let vc   = CommonWebViewVC()
//        let navigationController = UINavigationController(rootViewController: vc)
//        vc.landingUrl = NetInfo.policy_term
//        vc.vcTitle = TextInfo.title_policy
//        self.present(navigationController,animated: true, completion: nil)
        let vc   = TermVC()
        let navigationController = UINavigationController(rootViewController: vc)
        vc.termText = TermInfo.privacy_term
        vc.termTitle = TextInfo.title_privacy
        self.present(navigationController,animated: true, completion: nil)
    }
    
    @objc func actionPop() {
        //        self.navigationController?.popViewController(animated: true)
        self.navigationController?.dismiss(animated: true, completion: nil)
        NotificationCenter.default.post(name: Notification.Name.refreshSetting, object: nil, userInfo: nil)
    }
}
