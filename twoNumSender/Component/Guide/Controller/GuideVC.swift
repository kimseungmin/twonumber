//
//  GuideVC.swift
//  twoNumSender
//
//  Created by vincent_kim on 2020/10/12.
//  Copyright © 2020 remain. All rights reserved.
//

import UIKit


enum GuideCellTypes{
	case title
	case description
	case image
	case nonsave
	case regist
	case multiple
	case search
	case premium
	case recieve
	case send
	case close
	
	var info: (img: String, description:String){
		switch self {
			case .title:
				return ("", "투넘버 문자를 이용전에 꼭 확인하세요 !")
			case .description:
				return ("", "각 통신사에서 제공하는 부가서비스가 필요합니다\n이용 중인 통신사를 선택하여 해당 서비스에 가입하세요")
			case .image:
				return ("", "")
			case .nonsave:
				return ("symbol_non_save.png", "앱에 저장없이\n투넘버 형태로 문자를 바로 발송할 수 있습니다")
			case .regist:
				return ("symbol_reg.png", "새로운 연락처를 추가하세요\n프리미엄 서비스 이용 시 발신자 정보가 표시됩니다")
			case .multiple:
				return ("symbol_multi.png", "연락처를 여러개 선택하여 단체문자를 발송해보세요\n통신사 사정에 따라 발신되지 않을 수 있습니다")
			case .search:
				return ("symbol_search.png", "연락처를 검색할 수 있습니다\n이름 또는 초성, 휴대폰 번호로 검색할 수 있습니다")
			case .premium:
				return ("symbol_premium.png", "프리미엄 서비스는 정기 구독 상품입니다.\n발신자 표시 , 백업 기능을 제공합니다")
			case .recieve:
				return ("symbol_recieve.png", "해당 번호로 수신된 문자를 확인하세요\n아이폰 정책상 하나의 대화에서 사용이 불가합니다")
			case .send:
				return ("symbol_send.png", "아이폰에서 투넘버로 문자를 보내려면\n특수문자를 포함해서 발송해야 합니다.\n아이폰 정책상 하나의 대화에서 사용이 불가합니다")
			case .close:
				return ("", "")
		}
	}
	
}
class GuideVC: UIViewController {

	var models:[GuideCellTypes] = [.title, .description, .image, .nonsave, .regist, .multiple, .search, .premium, .recieve, .send, .close]
	@IBOutlet weak var tableView: UITableView!
	override func viewDidLoad() {
        super.viewDidLoad()

		self.setTableView()
		
        
    }
	deinit {
		Debug.print("")
	}

}


// table view del
extension GuideVC: UITableViewDelegate, UITableViewDataSource{
	func setTableView(){
		self.tableView.delegate = self
		self.tableView.dataSource = self
		self.tableView.tableFooterView = UIView()
		self.tableView.alwaysBounceVertical = false
		self.tableView.separatorStyle = .none

		self.registerCell()
	}
	
	private func registerCell() {
		self.tableView.register(UINib(nibName: String.init(describing: GuideTitleCell.self), bundle: nil), forCellReuseIdentifier: String.init(describing: GuideTitleCell.self))
		self.tableView.register(UINib(nibName: String.init(describing: GuideDescriptionCell.self), bundle: nil), forCellReuseIdentifier: String.init(describing: GuideDescriptionCell.self))
		self.tableView.register(UINib(nibName: String.init(describing: GuideBtnCell.self), bundle: nil), forCellReuseIdentifier: String.init(describing: GuideBtnCell.self))
		self.tableView.register(UINib(nibName: String.init(describing: GuideCommonBtnDescCell.self), bundle: nil), forCellReuseIdentifier: String.init(describing: GuideCommonBtnDescCell.self))
		self.tableView.register(UINib(nibName: String.init(describing: GuideCloseBtnCell.self), bundle: nil), forCellReuseIdentifier: String.init(describing: GuideCloseBtnCell.self))
		
	}
	
	
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return SettingHeaderViewCell.cellHeight()
	}
	
	func numberOfSections(in tableView: UITableView) -> Int {
		// #warning Incomplete implementation, return the number of sections
		return 1
	}
	
	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		let headerView = UIView()
		return headerView
		
	}
	
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		// #warning Incomplete implementation, return the number of rows
		
		return self.models.count
		
		
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		
		if self.models[indexPath.row] == .close{
			return GuideCloseBtnCell.cellHeight()
		}else{
			return UITableView.automaticDimension
		}
		
		
	}
	
	func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
		return 40
	}
	
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell: UITableViewCell
		let row = self.models[indexPath.row]
		switch  row{

			case .title:
				let customCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: GuideTitleCell.self)) as! GuideTitleCell
				customCell.titleLabel.text = row.info.description
				cell = customCell
				
			case .description:
				let customCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: GuideDescriptionCell.self)) as! GuideDescriptionCell
				
				customCell.descriptionLabel.text = row.info.description
				cell = customCell
				
			case .image:
				let customCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: GuideBtnCell.self)) as! GuideBtnCell
				customCell.delegate = self
				cell = customCell
				
			case .nonsave:
				let customCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: GuideCommonBtnDescCell.self)) as! GuideCommonBtnDescCell
				customCell.descLabel.text = row.info.description
				customCell.symbolImageView.image = UIImage(named: row.info.img)
				cell = customCell
			case .regist:
				let customCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: GuideCommonBtnDescCell.self)) as! GuideCommonBtnDescCell
				customCell.descLabel.text = row.info.description
				customCell.symbolImageView.image = UIImage(named: row.info.img)
				cell = customCell
			case .multiple:
				let customCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: GuideCommonBtnDescCell.self)) as! GuideCommonBtnDescCell
				customCell.descLabel.text = row.info.description
				customCell.symbolImageView.image = UIImage(named: row.info.img)
				cell = customCell
			case .search:
				let customCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: GuideCommonBtnDescCell.self)) as! GuideCommonBtnDescCell
				customCell.descLabel.text = row.info.description
				customCell.symbolImageView.image = UIImage(named: row.info.img)
				cell = customCell
			case .premium:
				let customCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: GuideCommonBtnDescCell.self)) as! GuideCommonBtnDescCell
				customCell.descLabel.text = row.info.description
				customCell.symbolImageView.image = UIImage(named: row.info.img)
				cell = customCell
			case .recieve:
				let customCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: GuideCommonBtnDescCell.self)) as! GuideCommonBtnDescCell
				customCell.descLabel.text = row.info.description
				customCell.symbolImageView.image = UIImage(named: row.info.img)
				cell = customCell
			case .send:
				let customCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: GuideCommonBtnDescCell.self)) as! GuideCommonBtnDescCell
				customCell.descLabel.text = row.info.description
				customCell.symbolImageView.image = UIImage(named: row.info.img)
				cell = customCell
			case .close:
				let customCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: GuideCloseBtnCell.self)) as! GuideCloseBtnCell
				customCell.delegate = self
				cell = customCell
		}
//
		cell.accessoryType = .none
		cell.selectionStyle = .none
//
		return cell

		
	}
	
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		
		if let index = self.tableView.indexPathForSelectedRow{
			self.tableView.deselectRow(at: index, animated: true)
		}
	}
	
	
	func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
		
	}
}

extension GuideVC: GuideCloseBtnCellDelegate, GuideBtnCellDelegate{
	func closeBtnTapped() {
		self.dismiss(animated: true, completion: nil)
	}
	
	
	func btnTapped(type: MobileType) {
		
		var url = ""
		switch type {
			
			case .kt:
				url = "https://product.kt.com/wDic/productDetail.do?ItemCode=474"
			case .skt:
				url = "https://m.tworld.co.kr/product/callplan?prod_id=NA00004073"
			case .uplus:
				url = "http://www.uplus.co.kr/css/pord/cosv/cosv/RetrievePsMbSDmsgInfo.hpi?catgCd=51436&prodCdKey=LRZ0000264"
		}
		self.presentSafari(url: url)
	}
	
	
}

extension GuideVC{
	func presentSafari(url :String){
		guard let url = URL(string: url) else { return }
		if #available(iOS 10.0, *) {
			UIApplication.shared.open(url, options: [:], completionHandler: nil)
		} else {
			UIApplication.shared.openURL(url)
		}
	}
}
