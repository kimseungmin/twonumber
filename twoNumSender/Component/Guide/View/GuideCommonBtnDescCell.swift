//
//  GuideCommonBtnDescCell.swift
//  twoNumSender
//
//  Created by vincent_kim on 2020/10/12.
//  Copyright © 2020 remain. All rights reserved.
//

import UIKit

class GuideCommonBtnDescCell: UITableViewCell {

	@IBOutlet weak var symbolImageView: UIImageView!
	@IBOutlet weak var descLabel: UILabel!
	override func awakeFromNib() {
        super.awakeFromNib()
		self.descLabel.font = UIFont.systemFont(ofSize: 13)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
