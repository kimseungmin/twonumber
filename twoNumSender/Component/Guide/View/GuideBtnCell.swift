//
//  GuideBtnCell.swift
//  twoNumSender
//
//  Created by vincent_kim on 2020/10/12.
//  Copyright © 2020 remain. All rights reserved.
//

import UIKit

protocol GuideBtnCellDelegate: AnyObject {
	func btnTapped(type: MobileType)
}


class GuideBtnCell: UITableViewCell {

	weak var delegate: GuideBtnCellDelegate?
	
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension GuideBtnCell{
	@IBAction func sktBtnTapepd(sender: UIButton){
		self.delegate?.btnTapped(type: .skt)
	}
	@IBAction func ktBtnTapepd(sender: UIButton){
		self.delegate?.btnTapped(type: .kt)
	}
	@IBAction func uplusBtnTapepd(sender: UIButton){
		self.delegate?.btnTapped(type: .uplus)
	}
	
	
}
