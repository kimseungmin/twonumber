//
//  GuideCloseBtnCell.swift
//  twoNumSender
//
//  Created by vincent_kim on 2020/10/12.
//  Copyright © 2020 remain. All rights reserved.
//

import UIKit

protocol GuideCloseBtnCellDelegate: AnyObject{
	func closeBtnTapped()
}

class GuideCloseBtnCell: UITableViewCell {

	weak var delegate: GuideCloseBtnCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension GuideCloseBtnCell{
	
	class func cellHeight() -> CGFloat{
		return 70
	}
	
	@IBAction func closeBtnTapped(){
		self.delegate?.closeBtnTapped()
	}
}
