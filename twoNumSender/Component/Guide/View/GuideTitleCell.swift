//
//  GuideTitleCell.swift
//  twoNumSender
//
//  Created by vincent_kim on 2020/10/12.
//  Copyright © 2020 remain. All rights reserved.
//

import UIKit

class GuideTitleCell: UITableViewCell {

	@IBOutlet weak var titleLabel: UILabel!
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		self.titleLabel.font = UIFont.boldSystemFont(ofSize: 20)
		self.titleLabel.textColor = UIColor(hexString: "595959")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
