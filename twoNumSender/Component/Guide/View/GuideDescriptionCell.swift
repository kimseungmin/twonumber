//
//  GuideDescriptionCell.swift
//  twoNumSender
//
//  Created by vincent_kim on 2020/10/12.
//  Copyright © 2020 remain. All rights reserved.
//

import UIKit

class GuideDescriptionCell: UITableViewCell {

	@IBOutlet weak var descriptionLabel: UILabel!
	override func awakeFromNib() {
        super.awakeFromNib()
		self.descriptionLabel.font = UIFont.systemFont(ofSize: 15)
		self.descriptionLabel.textColor = UIColor(hexString: "595959")
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
