//
//  AddrListHeaderCell.swift
//  twoNumSender
//
//  Created by vincent_kim on 2020/10/12.
//  Copyright © 2020 remain. All rights reserved.
//

import Foundation
import UIKit

class AddrListHeaderCell: UITableViewHeaderFooterView {

//	static let reuseIdentifier: String = String(describing: self)
	
	@IBOutlet weak var sectionTitleLabel: UILabel!
	@IBOutlet weak var callTitleLabel: UILabel!
	@IBOutlet weak var sendTitleLabel: UILabel!
	@IBOutlet weak var recieveTitleLabel: UILabel!
	
	var isMultipleSelectedMode:Bool = false{
		willSet{
				
			if newValue{
				self.sendTitleLabel.text = "선택"
			}
			else{
				self.sendTitleLabel.text = "전화"
				
			}
			
			self.callTitleLabel.isHidden = newValue
			self.recieveTitleLabel.isHidden = newValue
		}
		
	}
	
	var isTopVisibleCell:Bool = false{
		willSet{
			if self.isMultipleSelectedMode {
				self.recieveTitleLabel.isHidden = true
				self.callTitleLabel.isHidden = true
				self.sendTitleLabel.isHidden = false
			}
//			else{
//				self.recieveTitleLabel.isHidden = !newValue
//				self.callTitleLabel.isHidden = !newValue
//				self.sendTitleLabel.isHidden = !newValue
//			}
			
			
		}
	}
	
	override func prepareForReuse() {
		self.recieveTitleLabel.isHidden = false
		self.callTitleLabel.isHidden = false
		self.sendTitleLabel.isHidden = false

	}
	override init(reuseIdentifier: String?) {
		super.init(reuseIdentifier: reuseIdentifier)
		
		DispatchQueue.main.async { [weak self] in
			 guard let self = self else {
				 return
			 }
			 self.setUI()
		}
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		DispatchQueue.main.async { [weak self] in
			 guard let self = self else {
				 return
			 }
			 self.setUI()
		}
	}
	
	private func setUI(){
		self.backgroundColor = .white
	}
	
}

extension AddrListHeaderCell {
	class func cellHeight() -> CGFloat {
		return 26
	}
}
