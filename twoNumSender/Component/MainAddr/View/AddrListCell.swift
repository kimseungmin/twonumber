//
//  AddrListCell.swift
//  twoNumSender
//
//  Created by seungmin kim on 07/06/2019.
//  Copyright © 2019 remain. All rights reserved.
//

import UIKit

protocol AddrListCellDelegate: AnyObject{
	func getMsgBtnTapped(phone:String)
    func callBtnTapped(name: String, phone:String)
	func postMsgBtnTapped(phone:String)
	func isSelectedBtnTapped(seq:Int, status:Bool)
}
class AddrListCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var numLabel: UILabel!
    @IBOutlet weak var getMsgBtn: UIButton!
    @IBOutlet weak var callBtn: UIButton!
    @IBOutlet weak var postMsgBtn: UIButton!
	@IBOutlet weak var checkedBtn: UIButton!
	
	var isMultipleSelectMode:Bool = false{
		willSet{
			self.getMsgBtn.isHidden = newValue
			self.callBtn.isHidden = newValue
			self.postMsgBtn.isHidden = newValue
			
			self.checkedBtn.isHidden = !newValue
			
		}
	}
	
	var phoneNumber:String = ""
	var seq:Int = 0
	weak var delegate: AddrListCellDelegate?
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
	
	override func prepareForReuse() {
		self.checkedBtn.isSelected = false
		self.nameLabel?.text = nil
		self.numLabel?.text = nil
	}
    
}

//MARK: -
extension AddrListCell{
	@IBAction func getMsgBtnTapped(sender: UIButton){
        let confirm = UIAlertAction(title: "이동하기", style: .destructive, handler: { [weak self] _ in
            guard let self = self else { return }
            self.delegate?.getMsgBtnTapped(phone: self.phoneNumber)
        })
        let cancel = UIAlertAction(title: "취소", style: .cancel, handler: nil)
        UIAlertController.showAlert(
            title: "[원 넘버]로 수/발신",
            message: "[원 넘버]\n기존 번호로 발송되거나 수신된 내역으로 이동합니다",
            actions: [confirm, cancel]
        )
		
	}
	@IBAction func callBtnTapped(sender: UIButton){
        self.delegate?.callBtnTapped(name: self.nameLabel?.text ?? "", phone: self.phoneNumber)
	}
    
	@IBAction func postMsgBtnTapped(sender: UIButton){
        let confirm = UIAlertAction(title: "투넘버로 발신", style: .destructive, handler: { [weak self] _ in
            guard let self = self else { return }
            self.delegate?.postMsgBtnTapped(phone: self.phoneNumber)
        })
        let cancel = UIAlertAction(title: "취소", style: .cancel, handler: nil)
        
        if self.phoneNumber.hasSuffix("*") {
            let confirmAction = UIAlertAction(title: "안내 사이트로 이동하기", style: .default, handler: { _ in
                
                let url = UplusAlertInfo.url
                let strURL: String = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                UIApplication.shared.open(URL.init(string: strURL)!, options: [:], completionHandler: nil)
            })
            
            let forceAction = UIAlertAction(title: "발송하기", style: .destructive, handler: { _ in
                self.delegate?.postMsgBtnTapped(phone: self.phoneNumber)
            })
            
            let title = UplusAlertInfo.uplusTitle
            let message = UplusAlertInfo.uplusMsg
            UIAlertController.showAlert(
                title: title,
                message: message,
                actions: [forceAction, confirmAction, cancel]
            )
        }
        else {
            UIAlertController.showAlert(
                title: "[투넘버] 문자 발신",
                message: "[투넘버]\n두번째 번호로 발송됩니다\n부가서비스 가입여부를 꼭 확인하세요",
                actions: [confirm, cancel]
            )
        }
        
		
	}
	@IBAction func isSelectedBtnTapped(sender: UIButton){
		self.checkedBtn.isSelected.toggle()
		self.delegate?.isSelectedBtnTapped(seq: self.seq, status: self.checkedBtn.isSelected)
	}
}

extension AddrListCell{
    class func cellHeight() -> CGFloat {
        return 53
    }
}
