

import UIKit


protocol BottomSheetViewDelegate: AnyObject {
	func closeBtnTapped()
	func sendBtnTapped()
}

class BottomSheetView: UIView{
	
	weak var delegate:BottomSheetViewDelegate?
	
	@IBOutlet weak var contentView: UIView!
	@IBOutlet weak var sendBtn: UIButton!
	@IBOutlet weak var dividerView: UIView!
	@IBOutlet weak var closeBtn: UIButton!
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		commonInit()
	}
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		self.commonInit()
	}
	
	private func commonInit(){
		
		let view = UINib.loadNib(self)

		view.frame = self.bounds
		self.addSubview(view)
		view.backgroundColor = .clear
		view.addSubview(contentView)
		
		contentView.frame = self.bounds
		contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
		contentView.backgroundColor = .white
		
		DispatchQueue.main.async { [weak self] in
			self?.setUI()
		}
		
	}
	
	private func setUI(){
		self.sendBtn.toCornerRound(radius: 4.0)
		self.closeBtn.toCornerRound(radius: 4.0)
		self.dividerView.backgroundColor = UIColor.blue.withAlphaComponent(0.5)
		
//		self.contentView.addShadow(shadowColor: .black, offSet: CGSize(width: -2, height: -2), opacity: 0.18, shadowRadius: 2.0, cornerRadius: 0.0, corners: [.topLeft, .topRight])
//		self.contentView.addShadow(shadowColor: .black, offSet: CGSize(width: -2.0, height: -2.0), opacity: 0.18, shadowRadius: 2.0, cornerRadius: 0.0, corners: [.topLeft, .topRight], fillColor: .white)
//		self.addShadow(shadowColor: .black, offSet: CGSize(width: -2.0, height: -2.0), opacity: 0.18, shadowRadius: 2.0, cornerRadius: 0.0, corners: [.topLeft, .topRight], fillColor: .white)
	}
	
	public func showBottomSheet(isShow:Bool){
		if let navigationController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController, let presenting = navigationController.topViewController {
			
			UIView.animate(withDuration: 0.5, delay: 0.0, options: .transitionCrossDissolve, animations: {
				
				if isShow{
					self.removeFromSuperview()
					presenting.view.addSubview(self)
					presenting.view.bringSubviewToFront(self)
					self.translatesAutoresizingMaskIntoConstraints = false
					
					let viewTopConst = NSLayoutConstraint()
					
					let viewLeftConst = NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: presenting.view, attribute: .leading, multiplier: 1, constant: 0)
					let viewRightConst = NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: presenting.view, attribute: .trailing, multiplier: 1, constant: 0)
					
					var viewbottomConst = NSLayoutConstraint()
					
					if #available(iOS 11.0, *) {
						viewbottomConst = NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: presenting.view.safeAreaLayoutGuide, attribute: .bottom, multiplier: 1, constant: 0)
					} else {
						viewbottomConst = NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: presenting.view, attribute: .bottom, multiplier: 1, constant: 0)
					}
					
					let viewHeight = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 64)
					
					presenting.view.addConstraints([viewTopConst, viewLeftConst, viewRightConst, viewbottomConst, viewHeight])
					
				}else{
					self.removeFromSuperview()
				}
				
			},completion: nil)
		}
	}
	
}

extension BottomSheetView{
	@IBAction func closeBtnTapped(sender: UIButton){
		self.delegate?.closeBtnTapped()
	}
	
	@IBAction func sendBtnTapped(sender:UIButton){
		self.delegate?.sendBtnTapped()
	}
}


extension BottomSheetView{
	class func viewHeight() -> CGFloat {
		return 62
	}
}
