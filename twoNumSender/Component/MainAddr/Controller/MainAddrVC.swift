
import UIKit
import CoreData
import MessageUI
import Contacts
import ContactsUI
import SVProgressHUD
import CallKit
import SwiftyJSON
import Alamofire
import IQKeyboardManagerSwift

class MainAddrVC: UIViewController{

	let defaults = UserDefaults.standard
	
	var isMultiSelectedMode: Bool = false{
		willSet{
			
			UIViewPropertyAnimator(duration: 0.5, curve: .linear) {

				if let tabBarController = self.tabBarController{
					if newValue{
						tabBarController.tabBar.barTintColor = .white
					}
					else{
						tabBarController.tabBar.barTintColor = UIColor(hexString: ColorInfo.MAIN)
					}
					
				}
				self.bottomSheetView.showBottomSheet(isShow: newValue)
				
			}.startAnimation()
//
		}
	}
	
	@IBOutlet weak var saveNumberEditImageView: UIImageView!
	var bottomSheetView = BottomSheetView()
	
	@IBOutlet weak var serviceTitleLabel: UILabel!
	@IBOutlet weak var multiSelectBtn: UIButton!
	@IBOutlet weak var saveNumberDivider: UIView!
	@IBOutlet weak var savedNumberTF: UITextField!
	@IBOutlet weak var searchBarView: SearchBarView!
	var models: [String:[ContactModel]] = ["":[]]
	var currentModels: [String:[ContactModel]] = ["":[]]
	var dataArray: [ContactModel] = []
	var rightAreaTitles:[String] = ["ㄱ", "•", "ㄹ", "•", "ㅂ", "•", "ㅈ", "•", "ㅋ", "•", "C", "•", "E", "•", "H", "•", "J", "•", "M", "•", "P", "•", "R", "•", "U", "•", "W", "•", "Z", "#"]

	var sectionTitles:[String] = []
	let moType = UserDefaults.standard.string(forKey: .mobileType)!
	
	
	
	
	@IBOutlet weak var tableView: UITableView!
	override func viewDidLoad() {
        super.viewDidLoad()

		
		self.setTabBarDelegate()
		self.registerNotification()
		self.setUI()
		self.setTextField()
		self.setSavedNumber()
		self.setSearchView()
		self.setTableView()
		self.registerCell()
        self.getData()
		self.setBottomView()
		IAPManager.shared.verifyRecipt(withProgress: false)
		
    }

	
	
	
	deinit {
        self.unregisterNotification()
        Debug.print("")
    }

}

extension MainAddrVC{
	
	func setUI(){
		
		self.hidesBottomBarWhenPushed = true
		self.saveNumberEditImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(editSavedPhoneNumber)))
		
		self.serviceTitleLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(serviceTitleLabelTapped)))
		
		
	}
	
	func setSavedNumber(){
		let userContacts = UserDefaults.standard.string(forKey: .userContacts)
		if userContacts == nil{
			DispatchQueue.main.async { [weak self] in
				guard let self = self else { return }
				self.setUserContactsAlert()
			}


		}else{
			if userContacts == ""{
				self.savedNumberTF.text = nil
			}
			else{
				self.savedNumberTF.text = userContacts
			}
			
		}
	}
}

extension MainAddrVC{
	private func registerNotification() {
        
        /*멀티태스킹 상태에서 복귀했을 때*/
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive),
                                               name: UIApplication.didBecomeActiveNotification,
                                               object: nil)
		
		NotificationCenter.default.addObserver(self, selector: #selector(getData), name: Notification.Name.refreshAddrList, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(goToPremiumTuto), name: Notification.Name.premiumTuto, object: nil)
		
		NotificationCenter.default.addObserver(self, selector: #selector(setUserContactsAlert), name: Notification.Name.reloadNavBar, object: nil)
		
    }
    
    private func unregisterNotification() {
        
		NotificationCenter.default.removeObserver(self, name: Notification.Name.refreshAddrList, object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name.premiumTuto, object: nil)
		
    }
	
	@objc func applicationDidBecomeActive(){
//        self.viewWillAppear(true)
	}
}


extension MainAddrVC: UITableViewDelegate, UITableViewDataSource{
	
	private func setTableView() {
		/* 커스텀 버튼 딜레이제거 */
		self.tableView.delaysContentTouches = false
		for case let subview as UIScrollView in self.tableView.subviews {
			subview.delaysContentTouches = false
		}
		/* 커스텀 버튼 딜레이제거 */
		
		self.tableView.delegate = self
		self.tableView.dataSource = self
		self.tableView.separatorStyle = .none
//		self.tableView.sectionIndexColor = .systemBlue
		
		self.tableView.sectionIndexColor = .clear
		self.tableView.sectionIndexBackgroundColor = .clear
		self.tableView.alwaysBounceVertical = false
		
	}
	
	
	func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
		return self.dataArray.isEmpty ? 0 : self.models.keys.count
		
    }
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		
		if self.dataArray.isEmpty{
			return 0
		}
		else{
			let key = self.sectionTitles[section]
			if let values = self.models[key]{
				return values.count
			}
			return 0
		}
		
	}
	
	func sectionIndexTitles(for tableView: UITableView) -> [String]? {
		return self.rightAreaTitles
	}
	
	
	private func registerCell() {
        self.tableView.register(UINib(nibName: String.init(describing: AddrListCell.self), bundle: nil), forCellReuseIdentifier: String.init(describing: AddrListCell.self))
		self.tableView.register(UINib(nibName: String.init(describing: AddrListHeaderCell.self), bundle: nil), forHeaderFooterViewReuseIdentifier: String.init(describing: AddrListHeaderCell.self))
    }
	
	
	
	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		var view = UIView()
		
		let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: String.init(describing: AddrListHeaderCell.self)) as! AddrListHeaderCell
//			headerView.delegate = self
		
		if !dataArray.isEmpty{
			
			headerView.isMultipleSelectedMode = self.isMultiSelectedMode
			if section == 0{
				headerView.isTopVisibleCell = true
			}
			else{
				headerView.isTopVisibleCell = false
			
			}
			
			
			
			headerView.sectionTitleLabel.text = self.sectionTitles[section]
			
			view = headerView
		}
		
		
		return view
		
	}
	
	func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
		
		if let idx = sectionTitles.firstIndex(of: title){
			return idx
		}else{
			var expectString = title
			
			if title == "•"{
				var isPassed:Bool = false
				for (idx, v) in rightAreaTitles.enumerated().reversed(){
					
					
					if (isPassed) && (v != "•") && (expectString == "•"){
						expectString = v
					}
					
					if (idx <= index) && (isPassed == false){
						isPassed = true
					}
					
					
				}
			}
			
			let nearbyIndex = expectString.getStringIndex()
			
			for (idx, title) in sectionTitles.enumerated().reversed(){
				
				if nearbyIndex >= title.getStringIndex(){
					
					return idx
				}
			}
			print(expectString)
			return 0
			
			
			
			
			
		}

	}
	
//	func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//		if !self.dataArray.isEmpty{
//			return self.sectionTitles[section]
//		}
//		else{
//			return nil
//		}
//		
//	}
	
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return 24
		
    }
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		
		return AddrListCell.cellHeight()
        
    }
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: AddrListCell.self)) as! AddrListCell
		cell.selectionStyle = .none
		if self.dataArray.isEmpty{
            cell.nameLabel?.text = "저장된 연락처가 없습니다"
            cell.numLabel?.text = "새로운 연락처를 등록 해 주세요"
			cell.callBtn.isHidden = true
			cell.checkedBtn.isHidden = true
			cell.postMsgBtn.isHidden = true
			cell.getMsgBtn.isHidden = true
			
			
        }else{
			cell.callBtn.isHidden = false
			cell.checkedBtn.isHidden = false
			cell.postMsgBtn.isHidden = false
			cell.getMsgBtn.isHidden = false
			
			let key = self.sectionTitles[indexPath.section]
			if let values = self.models[key]{
				
				cell.nameLabel?.text = values[indexPath.row].name
				cell.numLabel?.text = values[indexPath.row].phone
				cell.phoneNumber = values[indexPath.row].phone
				cell.seq = values[indexPath.row].seq
				cell.checkedBtn.isSelected = values[indexPath.row].isSelected
				
				cell.isMultipleSelectMode = self.isMultiSelectedMode
				
			}
			if self.isMultiSelectedMode{
				cell.selectionStyle = .none
			}
			else{
				cell.selectionStyle = .default
			}
        }
        
        cell.delegate = self
        return cell
    }
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: false)
		if !dataArray.isEmpty{
			
			let key = self.sectionTitles[indexPath.section]
			
			if let values = self.models[key]{
				if self.isMultiSelectedMode{
					values[indexPath.row].isSelected.toggle()
					self.tableView.reloadRows(at: [indexPath], with: .none)
				}
				else{
					guard let realmModel = RealmManager.shared.getContact(seq: values[indexPath.row].seq) else { return }
                    
                    var params: [ViewValueKeys: Any] = [:]
                    params[.realmContactModel] = realmModel
                    params[.expactContactModel] = ContactModel(
                        seq: realmModel.seq,
                        name: realmModel.name,
                        phone: realmModel.phone,
                        memo: realmModel.memo
                    )
                    params[.isRegist] = false
					
                    RouterManager.shared.dispatcher(.addrDetail, values: params)
				}
			}
			
			
			
			

		}
    }
	
	func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if indexPath.row == 0{
            if self.dataArray.isEmpty{
                return nil
            }
        }
        return indexPath
    }
	
	func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?{
		let deleteAction = UITableViewRowAction(style: .destructive, title: "삭제") { (action, indexpath) in
			
			let key = self.sectionTitles[indexPath.section]
			if let values = self.models[key]{
				self.deleteAddr(seq: values[indexPath.row].seq)
			}
			
		}
		
		deleteAction.backgroundColor = .red
		return [deleteAction]
	}
}

extension MainAddrVC: UITabBarControllerDelegate {
	func setTabBarDelegate(){
		tabBarController?.delegate = self
	}
	func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
	//        let tabBarIndex = tabBarController.selectedIndex
	//        if tabBarIndex == 0 {
	//            self.getData()
	//        }
	//        if tabBarIndex == 1 {
	//            self.searchController.isActive = false
	//        }
	}
	
}

extension MainAddrVC{
    /* 코어 데이터 주소록 불러오기*/
    @objc func getData() {
		let initialString = InitialString()
        self.models.removeAll()
        self.dataArray.removeAll()
//		var titleGroupArray:[String] = []
		if let models = RealmManager.shared.getAllContacts(){
			for model in models{
				
				let initial = initialString.getStringConsonant(string: model.name, consonantType: .Initial)
				let firstValue = String(initial.prefix(1))
//				titleGroupArray.append(firstValue)
				
				let expectModel = ContactModel(seq: model.seq, name: model.name, phone: model.phone, memo: model.memo, initial: initial)
				
				var key = firstValue
				if let _ = Int(firstValue){
					key = "#"
				}
				
				if var values = self.models[key]{
					values.append(expectModel)
					self.models[key] = values
				}
				else{
					self.models[key] = [expectModel]
				}
				
				self.dataArray.append(expectModel)
			}
		}
		else{
			self.dataArray = []
		}
		
		self.currentModels = self.models
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            if self.currentModels.isEmpty{
                self.tableView.isHidden = true
            }
            else{
                self.tableView.isHidden = false
            }
        }
		
		
		
		
//		let expectRightItemsCount = 46
//		let emptyElementCount = Int(expectRightItemsCount / self.sectionTitles.count)
		
//		var rightAreaTitleItems:[String] = []
		
//		for title in sectionTitles{
//
//			rightAreaTitleItems.append(title)
//			if title != sectionTitles.last{
//				for _ in 1...emptyElementCount{
//					rightAreaTitleItems.append("・")
//				}
//
//			}
//
//		}
//		self.rightAreaTitles = rightAreaTitleItems
		self.setSections()
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.tableView.reloadData()
        }
		

		
	}

	func setSections(){
		var sectionTitleModels = [String](self.models.keys)
		sectionTitleModels = sectionTitleModels.sorted(by: <)//.sorted(by: { $0.lowercased() < $1.lowercased() })
		
		var needAddCharacter:Bool = false
		var needAddCharacterEmpty:Bool = false
		
		if sectionTitleModels.contains("#"){
			if let index = sectionTitleModels.firstIndex(of: "#"){
				sectionTitleModels.remove(at: index)
			}
			needAddCharacter = true
		}
		
		
		if sectionTitleModels.contains(""){
			if let index = sectionTitleModels.firstIndex(of: ""){
				sectionTitleModels.remove(at: index)
			}
			needAddCharacterEmpty = true
		}
		
		var koreanArray:[String] = []
		var englishArray:[String] = []
		
		for v in sectionTitleModels{
			if v.isEnglish(){
				englishArray.append(v)
			}
			else{
				koreanArray.append(v)
			}
		}
		sectionTitleModels = koreanArray + englishArray
		if needAddCharacter{
			sectionTitleModels.append("#")
		}
		
		
		if needAddCharacterEmpty{
			sectionTitleModels.append("")
		}
		
		self.sectionTitles = sectionTitleModels
	}


	
	func deleteAddr(seq:Int){
		let title = TextInfo.alert_delete_contacts_title
        let msg = TextInfo.alert_delete_contacts
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "삭제", style: .destructive) { action in
            
            /* 연락처 삭제 */
			RealmManager.shared.deleteContact(seq: seq, completion: { (finished) in
				if finished{
					self.getData()
					guard let purchase = IAPManager.shared.getPurchaseStatus() else { return }
					if purchase{
						Toast.makeToast(message: "동기화가 시작되었습니다\n완료 시 푸시알람이 발송됩니다", controller: self)
					}
                    CallKitManager.shared.saveCallKitData(success: nil, failure: nil)
					
				}
				else{
					
					let okAction = UIAlertAction(title: TextInfo.alert_confirm, style: .default, handler: nil)
					UIAlertController.showAlert(title: TextInfo.alert_title, message: "삭제에 실패하였습니다\n앱 종료 후 다시 시도하세요", actions: [okAction])
				}
				
			})

            
        })
        
        alert.addAction(UIAlertAction(title: "취소", style: .default) { action in
            
        })
        
        self.present(alert, animated: true, completion: nil)
	}
	
	
}


// action
extension MainAddrVC: MFMessageComposeViewControllerDelegate{
    /* 메시지 ui 출력 */
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch(result) {
        case .cancelled:
//            print("Message was cancelled")
            dismiss(animated: true, completion: nil)
        case .failed:
//            print("Message failed")
            dismiss(animated: true, completion: nil)
        case .sent:
//            print("Message was sent")
            dismiss(animated: true, completion: nil)
        default:
            break
        }
    }
    /* 메시지 ui 출력 */
    
}


// MARK: View
extension MainAddrVC{
	
    @objc func goToPremiumTuto(){
        let tutorialVC = TutorialCollectionVC(collectionViewLayout: UICollectionViewFlowLayout())
        tutorialVC.tutorialType = .premium
        let navigationController = UINavigationController(rootViewController: tutorialVC)
        tutorialVC.modalTransitionStyle = .crossDissolve
        self.present(navigationController, animated: true, completion: nil)
    }
	
	func goToRegistAddrVC(){

        var params: [ViewValueKeys: Any] = [:]
        params[.isRegist] = true
        
        RouterManager.shared.dispatcher(.addrDetail, values: params)
		
	}
	
	func goToNonSaveVC(){
		self.navigationController?.definesPresentationContext = false
		let vc = NonSavePopUpVC()
		let transition = CATransition()
		transition.duration = 0.25
		transition.type = CATransitionType.fade
		//        transition.subtype = CATransitionSubtype.fromTop
		self.view.window!.layer.add(transition, forKey: kCATransition)
		vc.modalPresentationStyle = .overFullScreen
		vc.modalTransitionStyle = .crossDissolve
		self.present(vc, animated: true)
	
	}
	
	func setSearchBarViewIsVisible(_ status:Bool){
		DispatchQueue.main.async { [weak self] in
			guard let self = self else { return }
			UIView.transition(with: self.searchBarView, duration:0.5, options: .transitionCrossDissolve, animations: {
				self.searchBarView.isHidden = !status
				
			}, completion: nil)
		}
		
		self.savedNumberTF.isHidden = status
		self.saveNumberEditImageView.isHidden = status
		
		
	}
}

//MARK: - Alert
extension MainAddrVC{
	@objc func setUserContactsAlert(){
		let alert = UIAlertController(title: "나의 투넘버", message: "내 투넘버가 상단에 표시됩니다\n본 앱은 통신사에서 제공하는 아래 부가서비스\n(넘버플러스, 투넘버, 듀얼넘버)를 이용시에만 사용할 수 있습니다.\n구매취소는 앱스토어 -> 구매 내역에서 진행할 수 있습니다.", preferredStyle: .alert)
		alert.addTextField()
		alert.addAction(UIAlertAction(title: "확인", style: .default) { action in
			if let text = alert.textFields?[0].text{
				UserDefaults.standard.set(text, forKey: .userContacts)
				self.savedNumberTF.text = text
			}
			
			
		})
		
		self.present(alert, animated: true, completion: nil)
		
		
		
	}
}

//MARK: - btn event
extension MainAddrVC: AddrListCellDelegate{
	func isSelectedBtnTapped(seq: Int, status: Bool) {
		for key in self.currentModels.keys{
			if let array = self.currentModels[key]{
				array.filter({ $0.seq == seq }).first?.isSelected = status
			}
		}
	}
	
	
	//MARK: 문자 수신
	func getMsgBtnTapped(phone: String) {
		if self.models.isEmpty{
			return
		}
		if MFMessageComposeViewController.canSendText(){
			let controller = MFMessageComposeViewController()
			var subString = ""
			let newString = phone.replacingOccurrences(of: " ", with: "", options: .literal, range: nil)
			var finalNum = ""
			if (newString.range(of: "*77") != nil){
				//        if moType == "kt"{
				
				subString = newString.replacingOccurrences(of: "*77", with: "", options: .literal, range: nil)
				
				finalNum = subString
				
			}else if (newString.range(of: "*281") != nil){
				//        }else if moType == "skt"{
				subString = newString.replacingOccurrences(of: "*281", with: "", options: .literal, range: nil)
				
				finalNum = subString
			}
            else{
				subString = newString.replacingOccurrences(of: "*", with: "", options: .literal, range: nil)
				finalNum = subString
			}
			//            print(finalNum)
			
			controller.body = ""
			// 받는 사람 지정
			controller.recipients = ["\(finalNum)"]
			
			controller.modalPresentationStyle = .fullScreen
			
			controller.messageComposeDelegate = self
	//            self.present(controller, animated: true, completion: nil)
			
			let sms: String = "sms:\(finalNum)"
			let strURL: String = sms.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
			UIApplication.shared.open(URL.init(string: strURL)!, options: [:], completionHandler: nil)
		}
		
		
	}
	
	//MARK: 전화
    func callBtnTapped(name: String, phone: String) {
		if self.models.isEmpty{
			return
		}
		var newString = phone.replacingOccurrences(of: " ", with: "", options: .literal, range: nil)
        //            let conString = newString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        
//        UIPasteboard.general.string = "\(newString)"
        let title = "전화형식 선택"
        let alert = UIAlertController(title: title, message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "기존번호로 걸기", style: .default) { action in
            if phone.contains("*281"){
                newString = phone.replacingOccurrences(of: "*281", with: "")
            }
            else if phone.contains("*77"){
                newString = phone.replacingOccurrences(of: "*77", with: "")
            }
            else if phone.contains("*"){
                newString = phone.replacingOccurrences(of: "*", with: "")
            }
            else{
                newString = phone
            }
            
            CallActionManager.shared.actionCall(
                name: name,
                number: newString,
                isOneNumber: true
            )
        })
        
        alert.addAction(UIAlertAction(title: "투넘버로 걸기", style: .default) { action in
            CallActionManager.shared.actionCall(
                name: name,
                number: newString,
                isOneNumber: false
            )
    
        })
        
        
        alert.addAction(UIAlertAction(title: "취소", style: .cancel) { action in
            
        })
        self.present(alert, animated: true, completion: nil)
	}
	
	//MARK: 문자 발신
	func postMsgBtnTapped(phone: String) {
		if self.models.isEmpty{
			return
		}
		if MFMessageComposeViewController.canSendText(){
		
		let controller = MFMessageComposeViewController()
		
		controller.body = ""
		
		let sendNum = phone
		var convertNum = ""

        if (sendNum.range(of: "*77") != nil) {
            //        if moType == "kt"{
            convertNum = "!\(sendNum)"
            controller.recipients = ["\(convertNum)"]
            
            
        }
        else if (sendNum.range(of: "*281") != nil){
            //        }else if moType == "skt"{
            
            convertNum = sendNum.replacingOccurrences(of: "*281 010", with: "*281 8210", options: .literal, range: nil)
            controller.recipients = ["\(convertNum)"]
        }
        else {
            convertNum = sendNum.replacingOccurrences(of: "", with: "", options: .literal, range: nil)
            controller.recipients = ["\(convertNum)"]
        }
		
		controller.messageComposeDelegate = self
		let sms: String = "sms:\(convertNum)"
		let strURL: String = sms.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
		UIApplication.shared.open(URL.init(string: strURL)!, options: [:], completionHandler: nil)

		
		}
	}
	
	
}

extension MainAddrVC: SearchBarViewDelegate{
	func isSearchMode(status: Bool) {
	
		if !status{
			self.setSearchBarViewIsVisible(false)
			
		}
	}
	
	func setSearchView(){
		self.setSearchBarViewIsVisible(false)
		
		self.searchBarView.backgroundColor = .white
		self.searchBarView.delegate = self
		self.searchBarView.isSearchMode = false
	}
	
	func textFieldChanged(text: String) {
		searchData(searchText: text)
	}
	
    /* 검색 로직 */
    func searchData(searchText: String) {
		DispatchQueue.main.async { [weak self] in
			guard let self = self else { return }
			self.models = self.currentModels
			if searchText == ""{
				self.models = self.currentModels
			}else{
				for key in self.models.keys{
					self.models[key] = self.currentModels[key]?.filter({ $0.name.contains("\(searchText)") || $0.phone.contains("\(searchText)") || $0.initial.contains("\(searchText)") })
					
					
				}
			}
			
			for key in self.models.keys{
				if let array = self.models[key]{
					if array.isEmpty{
						self.models.removeValue(forKey: key)
					}

				}
			}
			
			self.setSections()
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.tableView.reloadData()
            }
		}
		

    }
    /* 검색 로직 */
}

extension MainAddrVC: UIScrollViewDelegate{
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		let fadeInDistance: CGFloat = 100
//		var alpha: CGFloat = 1
		if scrollView.contentOffset.y < 1 {
//			alpha = 0
		} else if scrollView.contentOffset.y >= 1 && scrollView.contentOffset.y < fadeInDistance {
//			alpha = scrollView.contentOffset.y / fadeInDistance
		}
		tableView.sectionIndexColor = UIColor(hexString: ColorInfo.DARK).withAlphaComponent(0.6)
		tableView.sectionIndexBackgroundColor = .clear
	}

}
extension MainAddrVC: UITextFieldDelegate{
	func setTextField(){
		self.savedNumberTF.delegate = self
		let config = IQBarButtonItemConfiguration(title: "취소", action: #selector(cancelButtonTapped))
		self.savedNumberTF.addKeyboardToolbarWithTarget(target: self, titleText: nil, rightBarButtonConfiguration: config)

		//  any color you like
//		self.savedNumberTF.keyboardToolbar.doneBarButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.red], for: UIControl.State.normal)
		
	}
	
	@objc func cancelButtonTapped(){
		self.view.endEditing(true)
		self.setSavedNumber()
	}
	
	func textFieldDidEndEditing(_ textField: UITextField) {
		if textField == self.savedNumberTF {
			self.saveNumberDivider.backgroundColor = UIColor(hexString: "ADB5BD")
		}
		
	}
	func textFieldDidBeginEditing(_ textField: UITextField) {
		if textField == self.savedNumberTF {
			self.saveNumberDivider.backgroundColor = UIColor(hexString: ColorInfo.MAIN)
		}
	}
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		if textField == self.savedNumberTF {
			guard let text = self.savedNumberTF.text else { return false }
			UserDefaults.standard.set(text, forKey: .userContacts)
			self.savedNumberTF.text = text
			self.savedNumberTF.resignFirstResponder()
		}
		
		return true
	}
	
	@objc func editSavedPhoneNumber(sender: UITapGestureRecognizer){
		self.savedNumberTF.becomeFirstResponder()
	}
}

//MARK: - Btn Event
extension MainAddrVC{
	@IBAction func searchBtnTapped(){
		self.setSearchBarViewIsVisible(true)
		
		
	}
	
	@objc func serviceTitleLabelTapped(sender: UITapGestureRecognizer){
		self.moveToGuideVC()
	}
	
	private func moveToGuideVC(){
		let vc = GuideVC()
		vc.modalPresentationStyle = .fullScreen
		self.present(vc, animated: true, completion: nil)

	}
	
	@IBAction func guideBtnTapped(){
		self.moveToGuideVC()
	}
	
	@IBAction func registAddrBtnTapped(){
		self.goToRegistAddrVC()
	}
	
	@IBAction func nonSaveBtnTapped(){
		self.goToNonSaveVC()
	}
	
	@IBAction func multiSelectBtnTapped(){
		self.multiBtnToggle()
	}
	
	func multiBtnToggle(){
		self.multiSelectBtn.isSelected.toggle()
		self.isMultiSelectedMode.toggle()
		if !self.multiSelectBtn.isSelected{
			for key in self.currentModels.keys{
				if let array = self.currentModels[key]{
					for item in array{
						item.isSelected = false
					}
				}
			}
		}
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.tableView.reloadData()
        }
	}
    
	//MARK: 단체 문자 발신
	func postMsgBtnTapped() {
        if let firstValue = models.values.first,
           let firstItem = firstValue.first {
            if firstItem.phone.hasSuffix("*") {
                let cancel = UIAlertAction(title: "취소", style: .cancel, handler: nil)
                let confirmAction = UIAlertAction(title: "안내 사이트로 이동하기", style: .default, handler: { _ in
                    
                    let url = UplusAlertInfo.url
                    let strURL: String = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                    UIApplication.shared.open(URL.init(string: strURL)!, options: [:], completionHandler: nil)
                })
                
                let forceAction = UIAlertAction(title: "발송하기", style: .destructive, handler: { _ in
                    self.openMessageVC()
                })
                
                let title = UplusAlertInfo.uplusTitle
                let message = UplusAlertInfo.uplusMsg
                UIAlertController.showAlert(
                    title: title,
                    message: message,
                    actions: [confirmAction, forceAction, cancel]
                )
                return
            }
        }
        
        if self.models.isEmpty{
			return
		}
        self.openMessageVC()
	}
    
    private func openMessageVC() {
        if MFMessageComposeViewController.canSendText(){
            
            var contactArray:[ContactModel] = []
            var phoneArray:[String] = []
            let controller = MFMessageComposeViewController()
                
            
            controller.body = ""
            
            for key in self.models.keys{
                if let array = self.models[key]{
                    let models = array.filter{ $0.isSelected == true }
                    contactArray.append(contentsOf: models)
                }
            }
            
            for contact in contactArray {
                let sendNum = contact.phone
                
                var convertNum = ""

                if (sendNum.range(of: "*77") != nil){
                    //        if moType == "kt"{
                    convertNum = "!\(sendNum)"
                    controller.recipients = ["\(convertNum)"]
                    
                    
                }else if (sendNum.range(of: "*281") != nil){
                    //        }else if moType == "skt"{
                    
                    convertNum = sendNum.replacingOccurrences(of: "*281 010", with: "*281 8210", options: .literal, range: nil)
                    
                    
                }else{
                    convertNum = sendNum.replacingOccurrences(of: "", with: "", options: .literal, range: nil)
                    
                }
                phoneArray.append("\(convertNum)")
                
            }
            
            controller.recipients = phoneArray
            Debug.print(phoneArray)
            controller.messageComposeDelegate = self
            
            if phoneArray.isEmpty{
                return
            }
            self.present(controller, animated: true, completion: nil)
            
            
//            let sms: String = "sms:\(phoneArray.joined(separator: ", "))"
//            let strURL: String = sms.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
//            UIApplication.shared.open(URL.init(string: strURL)!, options: [:], completionHandler: nil)

        
        }
    }
	
}

extension MainAddrVC: BottomSheetViewDelegate{
	func closeBtnTapped() {
		self.multiBtnToggle()
	}
	
	func sendBtnTapped() {
        self.postMsgBtnTapped()
    
	}
	
	func setBottomView(){
		self.bottomSheetView.delegate = self
		
		
	}
	
}

extension UITabBarController {
	func setTabBarVisible(visible:Bool, duration: TimeInterval, animated:Bool) {
		if (tabBarIsVisible() == visible) { return }
		let frame = self.tabBar.frame
		let height = frame.size.height
		let offsetY = (visible ? -height : height)

		// animation
		UIViewPropertyAnimator(duration: duration, curve: .linear) {
//			self.tabBar.frame.offsetBy(dx:0, dy:offsetY)
			self.view.frame = CGRect(x:0,y:0,width: self.view.frame.width, height: self.view.frame.height + offsetY)
			self.view.setNeedsDisplay()
			self.view.layoutIfNeeded()
		}.startAnimation()
	}

	func tabBarIsVisible() ->Bool {
		return self.tabBar.frame.origin.y < UIScreen.main.bounds.height
	}
}


extension UITabBar {
	func tabBarIsVisible(_ isVisiblty: Bool = true){
		if isVisiblty {
			self.isHidden = false
			self.layer.zPosition = 0
		} else {
			self.isHidden = true
			self.layer.zPosition = -1
		}
	}
}
