

import UIKit

protocol ClearNavViewDelegate: AnyObject {
	
	func setTitleText() -> String
	func setBackBtnIsHidden() -> Bool
	func setBackBtnAction()
	func setRightBtnAction()
	func setRightBtnIsHidden() -> Bool
	
}


class ClearNavView: UIView {
    
    weak var delegate: ClearNavViewDelegate?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var rightBtn: UIButton!
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    private func commonInit(){
		
		let view = UINib.loadNib(self)
//		let view = Bundle.main.loadNibNamed(NSStringFromClass(self.classForCoder).components(separatedBy: ".").last!, owner: self, options: nil)?.first as! UIView
		view.frame = self.bounds
		self.addSubview(view)
		view.backgroundColor = .clear
		view.addSubview(contentView)
		
		contentView.frame = self.bounds
		contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
		contentView.backgroundColor = .clear
        
        DispatchQueue.main.async { [weak self] in
            self?.setUI()
        }
    }
    private func setUI(){
		self.titleLabel.textColor = .white//UIColor(hexString: ColorInfo.MAIN)
		self.backBtn.setBackgroundImage(UIImage(named: "ic_back_white.png"), for: .normal)
		self.rightBtn.setBackgroundImage(UIImage(named: "ic_save.png"), for: .normal)
		
        self.setTitleText()
        self.setBackBtnIsHidden()
        self.setRightBtnIsHidden()
    }
    
    func setTitleText() {
		let text = self.delegate?.setTitleText()
        self.titleLabel.text = text
		
        
    }
    func setBackBtnIsHidden(){
		self.backBtn.isHidden = (self.delegate?.setBackBtnIsHidden())!
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        delegate?.setBackBtnAction()
        
    }
    
    func setRightBtnIsHidden(){
		self.rightBtn.isHidden = (self.delegate?.setRightBtnIsHidden())!
    }
    
    @IBAction func rightBtnTapped(_ sender: Any) {
        delegate?.setRightBtnAction()
    }
    
    
}

