//
//  SplashVC.swift
//  twoNumSender
//
//  Created by seungmin kim on 07/06/2019.
//  Copyright © 2019 remain. All rights reserved.
//

import UIKit
import UIKit
import CoreData

import MessageUI
import Contacts
import ContactsUI
import SVProgressHUD
import CallKit
import SwiftyJSON
import Alamofire


class SplashVC: UIViewController {

    let defaults = UserDefaults.standard
    /* 코어 데이터 주소록 불러오기*/
    
//    var currnetContactsArray = [AnyObject]()
//	let managedContext = CoreDataManager.shared.managedObjectContext
	
    /*버전*/
    let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
    var versionConvert = ""
    var versionToInt = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    
//    @objc func applicationDidBecomeActive(){
//        // 화면 복귀 시 재 체크
//        self.updateChecker()
//    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        // 옵저버 초기화
        NotificationCenter.default.removeObserver(self)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
//		SystemUtils.shared.delay(delay: 0.5){
			self.updateChecker()
//		}
//        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive),
//                                               name: UIApplication.didBecomeActiveNotification,
//                                               object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    deinit {
        Debug.print("")
    }
}

//MARK:- Method
extension SplashVC{
    
    //MARK: 업데이트 체크
    func updateChecker(){
        /* 버전 관리 */
		let needUpdate = SystemUtils.shared.isUpdateAvailable()
		
//		#if DEBUG
//		Timer.scheduledTimer( timeInterval: 0.5 , target:self, selector: #selector(verifyUserInfo), userInfo: nil, repeats: false)
//		#else
		if needUpdate == true{
            showUpdateAlert(alertType: .force)
            
        }else{
			self.verifyUserInfo()
        }
		
//		#endif
        
        
        
        /* 버전 관리 */
    }
    
    //MARK: 유저 정보 확인 및 화면 이동
    @objc func verifyUserInfo(){
       let launchedBefore = UserDefaults.standard.bool(forKey: .launchedBefore)
       //        let launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore")
       switch launchedBefore {
           
		   case true:
				// 실행이력 있는 경우 메인으로 이동
			//		self.validateData()
				self.moveToMain()

			
			
		   case false, nil:
				self.defaults.set(true, forKey: .localSync)
				self.selectMotype()
			  
		  default:
				Debug.print(launchedBefore as Any)
      }
   }
	
	
	
    //MARK: 통신사 선택 (최초 실행 기준)
    func selectMotype(){
        // 최초 실행
        let title = "통신사 선택"
        let msg = "통신사 별 저장할 번호 형태가 설정됩니다\n추 후 설정 화면에서 변경할 수 있습니다"
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: TextInfo.kt, style: .default) { action in
		//            self.convertMoType(.kt, currentType: "")
            //            UserDefaults.init(suiteName: "group.com.remain.twoNumSender.twoNumWidget")?.setValue("kt", forKey: "mobileType")
            UserDefaults.standard.set("kt", forKey: .mobileType)

			self.moveToTutorial()

        })
        
        alert.addAction(UIAlertAction(title: TextInfo.skt, style: .default) { action in
            //            UserDefaults.init(suiteName: "group.com.remain.twoNumSender.twoNumWidget")?.setValue("skt", forKey: "mobileType")
            UserDefaults.standard.set("skt", forKey: .mobileType)
//            self.convertMoType(.skt, currentType: "")
            self.moveToTutorial()
        })
        
        alert.addAction(UIAlertAction(title: TextInfo.uplus, style: .default) { action in
            //            UserDefaults.init(suiteName: "group.com.remain.twoNumSender.twoNumWidget")?.setValue("skt", forKey: "mobileType")
            UserDefaults.standard.set("uplus", forKey: .mobileType)
//            self.convertMoType(.uplus, currentType: "")
			self.moveToTutorial()
        })
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func moveToMain(){
		RouterManager.shared.dispatcher(.main)
        
        // 영수증 검증
//		IAPManager.shared.verifyRecipt()
//        IAPManager.shared.verifySubscriptions([.autoRenewableMonthly, .autoRenewableYearly])
    }
	func moveToTutorial(){
		
		
		guard let window = UIApplication.shared.keyWindow else {
			return
		}

		guard let rootViewController = window.rootViewController else {
			return
		}

		let tutorialVC = TutorialCollectionVC(collectionViewLayout: UICollectionViewFlowLayout())
		tutorialVC.tutorialType = .tutorial
		let navigationController = UINavigationController(rootViewController: tutorialVC)
		navigationController.view.frame = rootViewController.view.frame
		navigationController.view.layoutIfNeeded()
		
		

		UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
			window.rootViewController = navigationController
		}, completion: { completed in
			// maybe do something here
		})
		

	}
}



