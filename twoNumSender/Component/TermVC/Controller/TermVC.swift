//
//  TermVC.swift
//  twoNumSender
//
//  Created by dkt_mac on 19/06/2019.
//  Copyright © 2019 remain. All rights reserved.
//

import UIKit

class TermVC: UITableViewController {
    var termTitle = ""
    var termText = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.drawNavigationBar()
        self.registerCell()
        self.drawTableView()
        
    }

    // MARK: - Table view data source
    
    

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: TermCell.self)) as! TermCell
        cell.termLabel.text = self.termText
        
        return cell
    }
    

}

// cell
extension TermVC{
    private func registerCell() {
        self.tableView.register(UINib(nibName: String.init(describing: TermCell.self), bundle: nil), forCellReuseIdentifier: String.init(describing: TermCell.self))
    }
    
}

// UI
extension TermVC{
    private func drawNavigationBar() {
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithTransparentBackground()
            appearance.backgroundColor = UIColor.init(hexString: ColorInfo.MAIN)
            appearance.shadowColor = .clear
            appearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            self.navigationController?.navigationBar.tintColor = .white
            self.navigationController?.navigationBar.standardAppearance = appearance
            self.navigationController?.navigationBar.scrollEdgeAppearance = appearance
            self.navigationController?.navigationBar.compactAppearance = appearance
        }
        self.navigationController?.navigationBar.isTranslucent = false          // 투명도 설정
		self.navigationController?.navigationBar.barTintColor = UIColor.init(hexString: ColorInfo.MAIN)
        
        self.navigationController?.navigationBar.shadowImage = UIImage()        // 네비 하단바 삭제 ?
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.backgroundColor = .white
        self.title = self.termTitle
        
        // set right bar - 메인화면으로 이동
        let rightBarImage = UIImage(named: "icon_navbar_close_sub")?.withRenderingMode(UIImage.RenderingMode.automatic)
        let rightBarButtonItem =  UIBarButtonItem(image: rightBarImage,
                                                  style: UIBarButtonItem.Style.done,
                                                  target: self,
                                                  action: #selector(actionPop))
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
        
    }
    
    private func drawTableView(){
        self.tableView.allowsSelection = false
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 90
    }
}

extension TermVC{
    @objc func actionPop() {
        //        self.navigationController?.popViewController(animated: true)
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
}
