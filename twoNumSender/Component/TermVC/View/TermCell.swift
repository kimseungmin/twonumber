//
//  TermCell.swift
//  twoNumSender
//
//  Created by dkt_mac on 19/06/2019.
//  Copyright © 2019 remain. All rights reserved.
//

import UIKit

class TermCell: UITableViewCell {

    @IBOutlet weak var termLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.termLabel.text = TermInfo.service_term
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
