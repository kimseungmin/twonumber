//
//  CommonWebViewVC.swift
//  twoNumSender
//
//  Created by seungmin kim on 27/05/2019.
//  Copyright © 2019 remain. All rights reserved.
//

import UIKit
import WebKit
import SVProgressHUD

class CommonWebViewVC: UIViewController  {
    @IBOutlet weak var webView: WKWebView!
    
    var landingUrl = ""
    var vcTitle = ""
    
    
        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.drawNavigationBar()
        self.setWebview()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

// MARK: WKUIDelegate
extension CommonWebViewVC : WKUIDelegate {
    
    open func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        Debug.print("window.open()", self)
        
        guard let url = navigationAction.request.url  else {
            return nil
        }
        
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
        return nil
    }
    
    public func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
        let alertController = UIAlertController(title: TextInfo.alert_title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: TextInfo.alert_confirm, style: .default, handler: { action in
            completionHandler()
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
}

// MARK: WKNavigationDelegate
extension CommonWebViewVC : WKNavigationDelegate {
    
    open func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        SVProgressHUD.show()
        
    }
    
    open func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        
    }
    
    open func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        SVProgressHUD.dismiss()
        Debug.print("탐색 중 오류: \(error)", self)
    }
    
    open func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        SVProgressHUD.dismiss()
        Debug.print("콘텐츠 로드 중 오류: \(error)", self)
    }
    
    open func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        Debug.print("", self)
        SVProgressHUD.dismiss()
    }
    
    open func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        Debug.print("", self)
        decisionHandler(.allow)
        
    }
    
}

//extension CommonWebViewVC{
//    func settingWebview(){
//
////
////        let url = NSURL(string: self.landingUrl)
////        let request = NSURLRequest(url: url! as URL)
////
////
////        // init and load request in webview.
////        webView = WKWebView(frame: self.view.frame)
////        webView.navigationDelegate = self
////        webView.load(request as URLRequest)
////
////        self.view.addSubview(webView)
////        self.view.sendSubviewToBack(webView)
////
//
//
//
//        let preferences = WKPreferences()
//        preferences.javaScriptEnabled = true
//
//        let configuration = WKWebViewConfiguration()
//        configuration.preferences = preferences
//
//        self.webView = WKWebView(frame: self.view.frame, configuration: configuration)
//        // init and load request in webview.
//
//        self.webView.navigationDelegate = self
//
//        self.view.addSubview(webView)
//        self.view.sendSubviewToBack(webView)
//
//        self.webView.topAnchor.constraint(equalTo: view.topAnchor, constant: 6).isActive = true
//        self.webView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 6).isActive  = true
//        self.webView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 6).isActive = true
//        self.webView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 6).isActive = true
//
//        let url = URL(string: self.landingUrl)!
//        var request = URLRequest(url: url)
//
//        request.timeoutInterval = 10
//
//        self.webView.load(request)
//    }
//
//    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
//        print(error.localizedDescription)
//    }
//    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
//        self.startAnimating(self.loadingSize, message: "", type: .ballScaleRippleMultiple)
//    }
//    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
//
//        stopAnimating()
//    }
//}


extension CommonWebViewVC{
    private func drawNavigationBar() {
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithTransparentBackground()
            appearance.backgroundColor = UIColor.init(hexString: ColorInfo.MAIN)
            appearance.shadowColor = .clear
            appearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            self.navigationController?.navigationBar.tintColor = .white
            self.navigationController?.navigationBar.standardAppearance = appearance
            self.navigationController?.navigationBar.scrollEdgeAppearance = appearance
            self.navigationController?.navigationBar.compactAppearance = appearance
        }
        self.navigationController?.navigationBar.isTranslucent = false          // 투명도 설정
		self.navigationController?.navigationBar.barTintColor = UIColor.init(hexString: ColorInfo.MAIN)
        
        self.navigationController?.navigationBar.shadowImage = UIImage()        // 네비 하단바 삭제 ?
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.backgroundColor = UIColor.init(hexString: ColorInfo.MAIN)
        self.title = self.vcTitle
        
        // set right bar - 메인화면으로 이동
        let rightBarImage = UIImage(named: "icon_navbar_close_sub")?.withRenderingMode(UIImage.RenderingMode.automatic)
        let rightBarButtonItem =  UIBarButtonItem(image: rightBarImage,
                                                  style: UIBarButtonItem.Style.done,
                                                  target: self,
                                                  action: #selector(actionPop))
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
        
    }
    
    private func setWebview(){
        self.webView.uiDelegate = self
        self.webView.navigationDelegate = self
        
        
        self.webView.load(URLRequest(url: URL(string: self.landingUrl)!))
    }
    
    
}


extension CommonWebViewVC{
    @objc func actionPop() {
        //        self.navigationController?.popViewController(animated: true)
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
}
