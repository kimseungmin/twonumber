//
//  UIFont+.swift
//  
//
//  Created by vincent_kim on 2020/11/09.
//

import Foundation
import UIKit

//MARK: - UIFont
extension UIFont {
	var checkIsBold: Bool {
		return fontDescriptor.symbolicTraits.contains(.traitBold)
	}

	var checkIsItalic: Bool {
		return fontDescriptor.symbolicTraits.contains(.traitItalic)
	}
	
	class func systemFont(ofSize fontSize: CGFloat, symbolicTraits: UIFontDescriptor.SymbolicTraits) -> UIFont? {
		return UIFont.systemFont(ofSize: fontSize).including(symbolicTraits: symbolicTraits)
	}

	func including(symbolicTraits: UIFontDescriptor.SymbolicTraits) -> UIFont? {
		var _symbolicTraits = self.fontDescriptor.symbolicTraits
		_symbolicTraits.update(with: symbolicTraits)
		return withOnly(symbolicTraits: _symbolicTraits)
	}

	func excluding(symbolicTraits: UIFontDescriptor.SymbolicTraits) -> UIFont? {
		var _symbolicTraits = self.fontDescriptor.symbolicTraits
		_symbolicTraits.remove(symbolicTraits)
		return withOnly(symbolicTraits: _symbolicTraits)
	}

	func withOnly(symbolicTraits: UIFontDescriptor.SymbolicTraits) -> UIFont? {
		guard let fontDescriptor = fontDescriptor.withSymbolicTraits(symbolicTraits) else { return nil }
		return .init(descriptor: fontDescriptor, size: pointSize)
	}


}
