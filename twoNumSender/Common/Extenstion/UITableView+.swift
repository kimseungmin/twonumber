
import Foundation
import UIKit

//MARK:- UITableView
extension UITableView {
	
	func reloadWithoutAnimation() {
		let lastScrollOffset = contentOffset
		beginUpdates()
		endUpdates()
		layer.removeAllAnimations()
		setContentOffset(lastScrollOffset, animated: false)
	}
	
	func isCellVisible(indexPath: IndexPath) -> Bool {
		guard let indexes = self.indexPathsForVisibleRows else {
			return false
		}
		return indexes.contains(indexPath)
	}

}

extension UITableView {

	/// The table section headers that are visible in the table view. (read-only)
	///
	/// The value of this property is an array containing UITableViewHeaderFooterView objects, each representing a visible cell in the table view.
	///
	/// Derived From: [http://stackoverflow.com/a/31029960/5191100](http://stackoverflow.com/a/31029960/5191100)
	var visibleSectionHeaders: [UITableViewHeaderFooterView] {
		get {
			var visibleSects = [UITableViewHeaderFooterView]()

			for sectionIndex in indexesOfVisibleSections() {
				if let sectionHeader = self.headerView(forSection: sectionIndex) {
					visibleSects.append(sectionHeader)
				}
			}

			return visibleSects
		}
	}

	private func indexesOfVisibleSections() -> Array<Int> {
		// Note: We can't just use indexPathsForVisibleRows, since it won't return index paths for empty sections.
		var visibleSectionIndexes = Array<Int>()

		
		for i in 0..<self.numberOfSections{
			
		
//		for (var i = 0; i < self.numberOfSections; i++) {
			var headerRect: CGRect?
			// In plain style, the section headers are floating on the top,
			// so the section header is visible if any part of the section's rect is still visible.
			// In grouped style, the section headers are not floating,
			// so the section header is only visible if it's actual rect is visible.
			if (self.style == .plain) {
				headerRect = self.rect(forSection: i)
			} else {
				headerRect = self.rectForHeader(inSection: i)
			}

			if headerRect != nil {
				// The "visible part" of the tableView is based on the content offset and the tableView's size.
				let visiblePartOfTableView: CGRect = CGRect(
					x: self.contentOffset.x,
					y: self.contentOffset.y,
					width: self.bounds.size.width,
					height: self.bounds.size.height
				)

				if (visiblePartOfTableView.intersects(headerRect!)) {
					visibleSectionIndexes.append(i)
				}
			}
		}

		return visibleSectionIndexes
	}
}

