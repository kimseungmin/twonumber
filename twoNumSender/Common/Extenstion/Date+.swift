//
//  Date+.swift
//  
//
//  Created by vincent_kim on 2020/11/09.
//

import Foundation
import UIKit

extension Date {
	func getMillisecond() -> Int64! {
		return Int64(self.timeIntervalSince1970 * 1000)
	}

	init(millis: Int64) {
		self = Date(timeIntervalSince1970: TimeInterval(millis / 1000))
		self.addTimeInterval(TimeInterval(Double(millis % 1000) / 1000 ))
	}
	
	var millisecondsSince1970:Int64 {
		return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
	}

	init(milliseconds:Int64) {
		self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
	}
	
	public var year: Int{
		return Calendar.current.component(.year, from: self)
	}
	
	public var month: Int{
		return Calendar.current.component(.month, from: self)
	}
	
	public var day: Int{
		return Calendar.current.component(.day, from: self)
	}
	
	public var monthName: String{
		let nameFormatter = DateFormatter()
		nameFormatter.dateFormat = "MMMM"
		return nameFormatter.string(from: self)
	}
	
	public var localDate: String {
		let f = ISO8601DateFormatter()
		f.formatOptions = [.withFullDate, .withDashSeparatorInDate]
		f.timeZone = TimeZone.current
		
		let s = f.string(from: self)
		return s
	}
	
	public var dateString: String{
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
		let dateInFormat = dateFormatter.string(from: self)
		return dateInFormat
	}
	
	public var dateStringKR: String{
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
		dateFormatter.locale = Locale(identifier: "ko")
		let dateInFormat = dateFormatter.string(from: self)
		return dateInFormat
	}
	
}

extension Date {

	func isEqual(to date: Date, toGranularity component: Calendar.Component, in calendar: Calendar = .current) -> Bool {
		calendar.isDate(self, equalTo: date, toGranularity: component)
	}

	func isInSameYear(as date: Date) -> Bool { isEqual(to: date, toGranularity: .year) }
	func isInSameMonth(as date: Date) -> Bool { isEqual(to: date, toGranularity: .month) }
	func isInSameWeek(as date: Date) -> Bool { isEqual(to: date, toGranularity: .weekOfYear) }

	func isInSameDay(as date: Date) -> Bool { Calendar.current.isDate(self, inSameDayAs: date) }

	var isInThisYear:  Bool { isInSameYear(as: Date()) }
	var isInThisMonth: Bool { isInSameMonth(as: Date()) }
	var isInThisWeek:  Bool { isInSameWeek(as: Date()) }

	var isInYesterday: Bool { Calendar.current.isDateInYesterday(self) }
	var isInToday:     Bool { Calendar.current.isDateInToday(self) }
	var isInTomorrow:  Bool { Calendar.current.isDateInTomorrow(self) }

	var isInTheFuture: Bool { self > Date() }
	var isInThePast:   Bool { self < Date() }
}
