//
//  UIButton+.swift
//  twoNumSender
//
//  Created by seungmin kim on 08/06/2019.
//  Copyright © 2019 remain. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    //loginBtn.setBackgroundColor(color: UIColor(ColorInfo.MAIN)!, forState: UIControlState.highlighted)
    func setBackgroundColor(color: UIColor, forState: UIControl.State) {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        UIGraphicsGetCurrentContext()!.setFillColor(color.cgColor)
        UIGraphicsGetCurrentContext()!.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        self.setBackgroundImage(colorImage, for: forState)
    }
    
}


//MARK: - UIButton Extension (UIControl Extension - add action)
//for  UIButton AddAction Every where
extension UIControl: Actionable {}

protocol Actionable {
	associatedtype T = Self
	func addAction(for controlEvent: UIControl.Event, action: ((T) -> Void)?)
}

private class ClosureSleeve<T> {
	let closure: ((T) -> Void)?
	let sender: T

	init (sender: T, _ closure: ((T) -> Void)?) {
		self.closure = closure
		self.sender = sender
	}

	@objc func invoke() {
		closure?(sender)
	}
}

extension Actionable where Self: UIControl {
	/**
	actionBtn.addAction(for: .touchUpInside, action: { _ in
		self.btnHandler?(item.type, item.value)
	})
	*/
	func addAction(for controlEvent: UIControl.Event, action: ((Self) -> Void)?) {
		
		let previousSleeve = objc_getAssociatedObject(self, String(controlEvent.rawValue))
		objc_removeAssociatedObjects(previousSleeve as Any)
		removeTarget(previousSleeve, action: nil, for: controlEvent)

		let sleeve = ClosureSleeve(sender: self, action)
		addTarget(sleeve, action: #selector(ClosureSleeve<Self>.invoke), for: controlEvent)
		objc_setAssociatedObject(self, String(controlEvent.rawValue), sleeve, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
	}
}


