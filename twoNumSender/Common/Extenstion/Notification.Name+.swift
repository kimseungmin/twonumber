//
//  Notification.Name+.swift
//  styletransfer
//
//  Created by rex.kim on 2018. 7. 10..
//  Copyright © 2018년 김강훈. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let refreshAddrList = Notification.Name(rawValue: "refreshAddrList")
    static let refreshSetting  = Notification.Name(rawValue: "refreshSetting")
    
    static let premiumTuto     = Notification.Name(rawValue: "premiumTuto")
//    static let shareDataSave   = Notification.Name(rawValue: "shareDataSave")
    static let reloadNavBar    = Notification.Name(rawValue: "reloadNavBar")

    

}
