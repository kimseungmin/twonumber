
import Foundation

extension Array where Element: Equatable{
	mutating func remove (_ element: Element) {
		if let i = self.firstIndex(of: element) {
			self.remove(at: i)
		}
	}
}



extension Array {
    func chunked(into size: Int) -> [[Element]] {
        return stride(from: 0, to: count, by: size).map {
            Array(self[$0 ..< Swift.min($0 + size, count)])
        }
    }
}



/**
array.filterDuplicate
*/
extension Array where Element: Hashable {
	var filterDuplicate: Array {
		var buffer = Array()
		var added = Set<Element>()
		for elem in self {
			if !added.contains(elem) {
				buffer.append(elem)
				added.insert(elem)
			}
		}
		return buffer
	}
}


extension Array {
	/**
	array.filterDuplicate{ $0($1.phone) }
	*/
	func filterDuplicate(_ keyValue:((AnyHashable...)->AnyHashable,Element)->AnyHashable) -> [Element]
	   {
		   func makeHash(_ params:AnyHashable ...) -> AnyHashable
		   {
			  var hash = Hasher()
			  params.forEach{ hash.combine($0) }
			  return hash.finalize()
		   }
		   var uniqueKeys = Set<AnyHashable>()
		   return filter{uniqueKeys.insert(keyValue(makeHash,$0)).inserted}
	   }
}
