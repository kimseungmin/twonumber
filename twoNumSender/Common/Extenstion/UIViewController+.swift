//
//  UIViewController+.swift
//  KFHHumidifier
//
//  Created by vincent_kim on 2020/01/20.
//  Copyright © 2020 vincent.kim. All rights reserved.
//


import UIKit
extension UIViewController {

    func dismissAll() {
        self.view.window?.rootViewController?.dismiss(animated: true)
    }
    
}

// MARK: @objc
extension UIViewController {
	@objc open func showAlert(_ notification: Notification) {
        guard let userInfo = notification.userInfo else {
            return
        }

        let title = userInfo["title"] as! String
        let msg = userInfo["msg"] as! String
        
//        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: I18N.system_alert_confirm, style: .default))
//        self.present(alert, animated: true, completion: nil)
		let alertAction = UIAlertAction(title: "알림", style: .default)
		UIAlertController.showAlert(title: title, message: msg, actions: [alertAction])
        
    }
    
    @objc func topMostViewController() -> UIViewController {
        // Handling Modal views
        if let presentedViewController = self.presentedViewController {
            return presentedViewController.topMostViewController()
        }
        // Handling UIViewController's added as subviews to some other views.
        else {
            for view in self.view.subviews
            {
                // Key property which most of us are unaware of / rarely use.
                if let subViewController = view.next {
                    if subViewController is UIViewController {
                        let viewController = subViewController as! UIViewController
                        return viewController.topMostViewController()
                    }
                }
            }
            return self
        }
    }

    
    // modal or present 여부
    var isModal: Bool {
        if let index = navigationController?.viewControllers.firstIndex(of: self), index > 0 {
            return false
        } else if presentingViewController != nil {
            return true
        } else if navigationController?.presentingViewController?.presentedViewController == navigationController {
            return true
        } else if tabBarController?.presentingViewController is UITabBarController {
            return true
        } else {
            return false
        }
        /**
         if self.isModal == true{
             self.dismiss(animated: true, completion: nil)
         }else{
             self.navigationController?.popViewController(animated: true)
         }
         */
    }
    
    // dismiss with count
    func dismissVC(withCount: Int, animate:Bool! = true) {
        guard let navigationController = navigationController else {
            for _ in 1...withCount{
                presentingViewController?.dismiss(animated: animate, completion: nil)
            }
            return
        }
        let viewControllers = navigationController.viewControllers
        let index = withCount + 1
        if viewControllers.count >= index {
            navigationController.popToViewController(viewControllers[viewControllers.count - index], animated: animate)
        }
    }
}


extension UIApplication {
    
    class func topViewController(base: UIViewController? = UIWindow.key?.rootViewController) -> UIViewController? {

        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)

        }
        else if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
            return topViewController(base: selected)

        }
        else if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
    
}
