//
//  SVProgressHUD+.swift
//  twoNumSender
//
//  Created by vincent_kim on 2020/05/26.
//  Copyright © 2020 remain. All rights reserved.
//

import Foundation
import SVProgressHUD

extension SVProgressHUD {
    open class func show(_ status: String, maxTime: TimeInterval) {
        SVProgressHUD.show(withStatus: status)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + maxTime) {
            SVProgressHUD.dismiss()
        }
    }
}
