//
//  Int+.swift
//
//
//  Created by vincent_kim on 2020/11/09.
//

import Foundation
extension Int{
	var intToMBFloat: Float{
		let float = Float(self)
		let byteToMB = float / 1000 / 1000
		if byteToMB < 0.01{
			return 0.00
		}else{
			return byteToMB
		}
		
		
	}
	
	var intToTime: String{
		if self < 60 {
			let v = String(format: "%02d", self)
			return "00:\(v)"
			
		} else if self >= 60 {
			let min = String(format: "%02d", (self % 60))
			let hour = String(format: "%02d", (self / 60))
			return "\(hour):\(min)"
		} else{
			return "00:00"
		}
	}
}

