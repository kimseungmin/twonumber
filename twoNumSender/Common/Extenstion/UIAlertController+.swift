//
//  UIAlertController+.swift
//
//
//  Created by vincent_kim on 2020/11/09.
//

import Foundation
import UIKit


extension UIAlertController{
	
	static func commonDebugAlert(_ errorMsg: String) {
		let keyWindow = UIWindow.key
		
		
		let title = "DEBUG Error"
		let msg = errorMsg
		let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
		
		alert.addAction(UIAlertAction(title: "confirm", style: .default) { action in
			
		})
		if let navigationController = keyWindow?.rootViewController as? UINavigationController, let presenting = navigationController.topViewController {
			presenting.present(alert, animated: true, completion: nil)
		}
		
//		if let navigationController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController, let presenting = navigationController.topViewController {
//			presenting.present(alert, animated: true, completion: nil)
//		}
		
		
	}
	static func showMessage(_ message: String) {
		showAlert(title: "", message: message, actions: [UIAlertAction(title: "OK", style: .cancel, handler: nil)])
	}
	
	/**
	let alertAction = UIAlertAction(title: "ok", style: .cancel)
	UIAlertController.showAlert(title: "Notice", message: "Save Draft Btn Tapped", actions: [alertAction])
	*/
	static func showAlert(title: String?, message: String?, actions: [UIAlertAction], style: UIAlertController.Style = .alert) {
		DispatchQueue.main.async {
			let alert = UIAlertController(title: title, message: message, preferredStyle: style)
			for action in actions {
				alert.addAction(action)
			}
			
			if let topVC = UIApplication.topViewController() {
				topVC.present(alert, animated: true, completion: nil)
			}
			
		}
	}


}

// alert 에 이미지 추가
extension UIAlertController{
	func addImage(image: UIImage){
		
		let maxSize = CGSize(width: 245, height: 300)
		let imgSize = image.size
		
		var ratio: CGFloat!
		if (imgSize.width > imgSize.width){
			ratio = maxSize.width / imgSize.width
		}else{
			ratio = maxSize.height / imgSize.height
		}
		
		let scaledSize = CGSize(width: imgSize.width * ratio, height: imgSize.height * ratio)
		
		var resizedImage = image.imageWithSize(scaledSize)
		
		if(imgSize.height > imgSize.width){
			let left = (maxSize.width - resizedImage.size.width)/2
			resizedImage = resizedImage.withAlignmentRectInsets(UIEdgeInsets(top: 0, left: -left, bottom: 0, right: 0))
		}
		
		
		let imgAction = UIAlertAction(title : "", style: .default, handler: nil)
		imgAction.isEnabled = false
		imgAction.setValue(resizedImage.withRenderingMode(.alwaysOriginal), forKey: "image")
		self.addAction(imgAction)
		
	}
}


