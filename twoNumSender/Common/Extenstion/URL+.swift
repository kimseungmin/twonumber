//
//  URL+.swift
//  twoNumSender
//
//  Created by vincent_kim on 2020/11/11.
//  Copyright © 2020 remain. All rights reserved.
//

import Foundation
import UIKit

extension URL {
	var attributes: [FileAttributeKey : Any]? {
		do {
			return try FileManager.default.attributesOfItem(atPath: path)
		} catch let error as NSError {
			print("FileAttribute error: \(error)")
		}
		return nil
	}

	var fileSize: UInt64 {
		return attributes?[.size] as? UInt64 ?? UInt64(0)
	}
	
	var fileSizeFloat: Float {
		let mb = self.fileSizeString
		let float = Float(NSString(string: mb).floatValue) / 1000 / 1000
		return float
	}

	var fileSizeString: String {
		return ByteCountFormatter.string(fromByteCount: Int64(fileSize), countStyle: .file)
	}

	var creationDate: Date? {
		return attributes?[.creationDate] as? Date
	}
}

