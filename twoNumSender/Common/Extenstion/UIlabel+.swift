//
//  UIlabel+.swift
//  twoNumSender
//
//  Created by seungmin kim on 19/05/2019.
//  Copyright © 2019 remain. All rights reserved.
//

import UIKit

// 라벨 패딩
class PaddingUiLabel: UILabel
{
    var topInset: CGFloat = 10.0
    var bottomInset: CGFloat = 10.0
    var leftInset: CGFloat = 10.0
    var rightInset: CGFloat = 10.0
    
    override func drawText(in rect: CGRect)
    {
        let insets: UIEdgeInsets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        let rectContainer: CGRect
        
        func rectangleFrame (forFrames frames: CGRect) -> CGRect {
            return rectContainer.inset(by: insets)
        }
    }
    
    //    override func drawText(in rect: CGRect) {
    //        let insets: UIEdgeInsets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
    //        super.drawText(in: (rect, insets))
    //    }
    //
    override public var intrinsicContentSize: CGSize
    {
        var contentSize = super.intrinsicContentSize
        contentSize.height += topInset + bottomInset
        contentSize.width += leftInset + rightInset
        return contentSize
    }
    
    func setPadding(top: CGFloat, left: CGFloat, bottom: CGFloat, right: CGFloat){
        self.topInset = top
        self.bottomInset = bottom
        self.leftInset = left
        self.rightInset = right
        let insets: UIEdgeInsets = UIEdgeInsets(top: top, left: left, bottom: bottom, right: right)
        let rectContainer: CGRect
        
        func rectangleFrame (forFrames frames: CGRect) -> CGRect {
            return rectContainer.inset(by: insets)
        }
    }
    

    
}

extension UILabel {
    func customLabel(string: String, withColor: UIColor?, underLine:Bool, bold:Bool) {
        
        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: self.text!)
        
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: withColor!, range: (text! as NSString).range(of:string))
        
        
        if underLine == true{
            attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.thick.rawValue, range: (text! as NSString).range(of:string))
            attributedString.addAttribute(NSAttributedString.Key.underlineColor, value: withColor!, range: (text! as NSString).range(of:string))
        }
        
        if bold == true{

            let boldFontAttribute: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: font.pointSize)]
            let range = (string as NSString).range(of: string)
            attributedString.addAttributes(boldFontAttribute, range: range)
        }
        
        
        
        
        self.attributedText = attributedString
        
    }
}


extension UILabel {
	func setLetterSpacing(spacing: CGFloat, underline: Bool? = false){
		let attributedString = NSMutableAttributedString(string: self.text!)
		attributedString.addAttribute(NSAttributedString.Key.kern, value: spacing, range: NSRange(location: 0, length: self.text!.count))
		
		if underline == true{
			attributedString.addAttribute(NSAttributedString.Key.underlineStyle,
										  value: NSUnderlineStyle.single.rawValue,
										  range: NSRange(location: 0, length: self.text!.count))
		}else{
			
			attributedString.removeAttribute(NSAttributedString.Key.underlineStyle, range: NSRange(location: 0, length: self.text!.count))
		}
		
		self.attributedText = attributedString
	}
}


