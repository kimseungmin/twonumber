//
//  NSAttributedString+.swift
//  twoNumSender
//
//  Created by vincent_kim on 2020/11/11.
//  Copyright © 2020 remain. All rights reserved.
//
import UIKit

//MARK: - NSAttributedString Extension
extension NSAttributedString {
	
	/// replace attribute subString for whole self
	func replaceString(stringToReplace: String, replacedWithString newStringPart: String) -> NSMutableAttributedString
	{
		let mutableAttributedString = mutableCopy() as! NSMutableAttributedString
		let mutableString = mutableAttributedString.mutableString
		
		while mutableString.contains(stringToReplace) {
			let rangeOfStringToBeReplaced = mutableString.range(of: stringToReplace)
			mutableAttributedString.replaceCharacters(in: rangeOfStringToBeReplaced, with: newStringPart)
			
		}
		return mutableAttributedString
	}
	
	//MARK: attribute to html
	var attributeToHtml: String? {
		
		var unicodeWhiteSpace = ""
		
		do {
			if let whiteSpace = UnicodeScalar(160){
				unicodeWhiteSpace = String(whiteSpace)
			}

			let string = self.replaceString(stringToReplace: " ", replacedWithString: "&nbsp;")
			let v = string.replaceString(stringToReplace: unicodeWhiteSpace, replacedWithString: "&nbsp;")
			
			let htmlData = try v.data(from: NSRange(location: 0, length: v.length), documentAttributes:[.documentType: NSAttributedString.DocumentType.html]);

			let input = NSString(data: htmlData, encoding: String.Encoding.utf8.rawValue)
			let lines = (input?.components(separatedBy: "\n"))! as [String]
			var result = ""
			var tagArray:[String:String] = [:]
			

			
			for line in lines {
				let replacedLine = line.replacingOccurrences(of: "}", with: ";}")
//				if line.hasPrefix("span.") || line.hasPrefix("p."){
//
//					let tag = line.capturedGroups(withRegex: "\\{([^}]*?)\\}")
//					let value = tag.first ?? ""
//					let className = line.capturedGroups(withRegex: "\\.([^}]*?)\\{")
//					let key = className.first?.dropLast() ?? ""
//
//					tagArray[String(key)] = value
//				}
				if replacedLine.hasPrefix("span."){
					// 스타일 태그의 본문만 수집
					let tag = replacedLine.capturedGroups(withRegex: "\\{([^}]*?)\\}")
					let value = tag.first ?? ""
					
					// key 값
					let className = replacedLine.capturedGroups(withRegex: "\\.([^}]*?)\\{")
					let key = className.first?.dropLast() ?? ""
					
					let a = value.replacingOccurrences(of: "font-kerning: none;", with: "")
					let b = a.replacingOccurrences(of: "font-weight: normal;", with: "")
					let c = b.replacingOccurrences(of: "font-style: normal;", with: "")
					
					let d = c.replacingOccurrences(of: "font-kerning: none", with: "")
					let e = d.replacingOccurrences(of: "font-weight: normal;", with: "")
					let f = e.replacingOccurrences(of: "font-style: normal;", with: "")
					
					// 스타일 태그 내 stroke제거
					let storkeRegex = "-webkit(.*?);"
					let strokeColor = f.capturedGroups(withRegex: storkeRegex)
					
					let regexResult = "-webkit" + (strokeColor.first ?? "") + ";"
					let v = f.replacingOccurrences(of: regexResult, with: "")
					print(v)
					
					tagArray[String(key)] = v
				}
				if replacedLine.hasPrefix("p."){
					
					// 스타일 태그의 본문만 수집
					let tag = replacedLine.capturedGroups(withRegex: "\\{([^}]*?)\\}")
					let value = tag.first ?? ""
					let className = replacedLine.capturedGroups(withRegex: "\\.([^}]*?)\\{")
					let key = className.first?.dropLast() ?? ""
//					tagArray[String(key)] = value

					
					var filterdAttributes = ""
					// 스타일 태그 내 color 수집
					let colorRegex = "color:(.*?);"
					let color = value.capturedGroups(withRegex: colorRegex)
					if let res = color.first{
						filterdAttributes += "color:\(res); "
					}
					
					let backgroundColorRegex = "background-color:(.*?);"
					let backgroundColor = value.capturedGroups(withRegex: backgroundColorRegex)
					if let res = backgroundColor.first{
						filterdAttributes += " background-color:\(res);"
					}
					
					let fontRegex = "font:(.*?);"
					let font = value.capturedGroups(withRegex: fontRegex)
					if let res = font.first{
						filterdAttributes += " font:\(res);"
					}
					
					
					tagArray[String(key)] = filterdAttributes

					
				}

			}
	
			for line in lines {
				if !line.hasPrefix("<meta")
					&& !line.hasPrefix("<!DOCTYPE ")
					&& !line.hasPrefix("<html")
					&& !line.hasPrefix("p.")
					&& !line.hasPrefix("span.")
					&& !line.hasPrefix("<body")
					&& !line.hasPrefix("</body")
					&& !line.hasPrefix("<style")
					&& !line.hasPrefix("</style")
					&& !line.hasPrefix("<head")
					&& !line.hasPrefix("<title")
					&& !line.hasPrefix("</head")
					&& !line.hasPrefix("</html")
				{
					
					var removeClassResult = ""
					let regexV = "\"(.*?)\""
					let regexMathes = regexMatches(for: regexV, in: line)
					
					var removeClass = line
					for value in regexMathes{
						
						let expactValue = value.replacingOccurrences(of: "\"", with: "")
						
						if let dictValue = tagArray[expactValue]{
							removeClass = removeClass.replacingOccurrences(of: "class=\"\(expactValue)", with: "style=\"\(dictValue)")
							
						}
						
						removeClassResult = removeClass
					}
					
					
					/// 중간 공백을 자체적으로 기본폰트로 변환해버리는 현상
//					let appleRegex = try! NSRegularExpression(pattern: "<span class=\"Apple-converted-space\">(.*?)</span>", options: NSRegularExpression.Options.caseInsensitive)
////					let regex = try! NSRegularExpression(pattern: "[^A-Z]</span><span class=\"(.*?)\"><span class=\"Apple-converted-space\">(.*?)", options: NSRegularExpression.Options.caseInsensitive)
//					let appleRange = NSMakeRange(0, removeClassResult.count)
//					removeClassResult = appleRegex.stringByReplacingMatches(in: removeClassResult, options: [], range: appleRange, withTemplate: "")
					
					let fontRegex = try! NSRegularExpression(pattern: "</span>[^>]*(?=> +)>", options: NSRegularExpression.Options.caseInsensitive)
//					let regex = try! NSRegularExpression(pattern: "[^A-Z]</span><span class=\"(.*?)\"><span class=\"Apple-converted-space\">(.*?)", options: NSRegularExpression.Options.caseInsensitive)
					let fontRange = NSMakeRange(0, removeClassResult.count)
					removeClassResult = fontRegex.stringByReplacingMatches(in: removeClassResult, options: [], range: fontRange, withTemplate: "")
					
					removeClassResult = removeClassResult.convertSpecialCharacters
					removeClassResult = removeClassResult.replacingOccurrences(of: "‭", with: "")
					
//					removeClassResult = removeClassResult.replacingOccurrences(of: "<span style", with: "<span style")
//					removeClassResult = removeClassResult.replacingOccurrences(of: "</span>", with: "</span>")
					
					/**
					to. @dave.kn
					https://dkt.agit.in/g/300042597/wall/333482415#comment_panel_333500295
					1) html, head, body 까지는 기존처럼 없애주세요.
					=> 제거 이슈 없음
					
					2) text-align은 paragraph의 속성인바 저희가 보여줄 때 가령 웹이라면 div로 감싸면서 거기에 스타일로 넣을것입니다. 그러므로 각 span에서 text-align 속성은 필요가 없게 되겠습니다.
					=> key, value 로 json 에 정의 되어있으니 제거 이슈 없음
					
					3) 엔터를 치면 <p></p> 대신 끝에 <br>을 붙여주세요.
					=> p style에 들어가있던 색상은 어떻게 ?
					=> p style의 속성이 상위 하이라키와 동일하지 않으면 -- <span> </span><br> ?
					=> p style의 속성이 상위 하이라키와 동일하면 -- <span> </span> 삭제 후 뒤에 <br> 만?
					=> 이미 span이 제거 된 경우 바깥을 어떻게 구분?
					=> span 개수에 비례해서 0번 글자까지 for loop?
					
					4) 바깥 span에서 속성이 변하지 않은 font-size, font-weight, font-style, font-family를 안쪽 span에서 제거해 주세요. 자동으로 inherit되어 작동합니다.
					=> 3번에서는 이미 span이 없는데 바깥을 어떻게 구분?
					=> span 개수에 비례해서 0번 글자까지 for loop?
					
					5) margin이 필요없습니다.
					=> 제거 이슈없음
					
					6) 이를 위 코드에 적용하면 다음이 될 것 같습니다. (기본 폰트 notnosans, font-size 16px, 검정이라 가정 - 이는 에디터 로드시 기본값 설정할 수 있어야 합니다. )
					=> 커스터마이즈 정의서에 기술되어있지 않음
					
					기존 output:
					<p style="color: #212529;"><span style="font-family: 'NotoSansKR-Regular';   font-size: 16.00px; ">테스트&nbsp;데이터&nbsp;룰</span></p><p style="color: #212529;"><span style="font-family: 'NotoSansKR-Regular';   font-size: 16.00px; ">만들어</span></p><p style="color: #212529;"><span style="font-family: 'Times New Roman';   font-size: 12.00px; "></span><br></p><p style="color: #212529;"><span style="font-family: 'Times New Roman';   font-size: 12.00px; "></span><br></p><p style="color: #d6336c;"><span style="font-family: 'Noto Sans KR'; font-weight: bold;  font-size: 16.00px; ">봅시다&nbsp;&nbsp;&nbsp;&nbsp;</span><span style="font-family: 'NotoSansKR-Regular';   font-size: 16.00px;  color: #0057fe; ">볼드도&nbsp;넣교&nbsp;데이터&nbsp;룰</span><span style="font-family: 'NotoSansKR-BoldItalic'; font-weight: bold; font-style: italic; font-size: 16.00px; text-decoration: underline ;  color: #212529; ">언더라인도&nbsp;데이터&nbsp;룰</span><span style="font-family: 'NotoSansKR-Regular';   font-size: 16.00px;  color: #212529; ">넣어보고</span></p>
					
					
					===========
					하단 두 줄 주석 해제 시 output
					<span style="color: #212529;"><span style="font-family: 'NotoSansKR-Regular';   font-size: 16.00px; ">테스트&nbsp;데이터&nbsp;룰</span><br><span style="color: #212529;"><span style="font-family: 'NotoSansKR-Regular';   font-size: 16.00px; ">만들어</span><br><span style="color: #212529;"><span style="font-family: 'Times New Roman';   font-size: 12.00px; "></span><br><br><span style="color: #212529;"><span style="font-family: 'Times New Roman';   font-size: 12.00px; "></span><br><br><span style="color: #d6336c;"><span style="font-family: 'Noto Sans KR'; font-weight: bold;  font-size: 16.00px; ">봅시다&nbsp;&nbsp;&nbsp;&nbsp;</span><span style="font-family: 'NotoSansKR-Regular';   font-size: 16.00px;  color: #0057fe; ">볼드도&nbsp;넣교&nbsp;데이터&nbsp;룰</span><span style="font-family: 'NotoSansKR-BoldItalic'; font-weight: bold; font-style: italic; font-size: 16.00px; text-decoration: underline ;  color: #212529; ">언더라인도&nbsp;데이터&nbsp;룰</span><span style="font-family: 'NotoSansKR-Regular';   font-size: 16.00px;  color: #212529; ">넣어보고</span><br>
				
					
					=========
					preview 참고
					https://html-online.com/editor/
					*/
					// p 태그 제거
//					removeClassResult = removeClassResult.replacingOccurrences(of: "<p", with: "<span")
//					removeClassResult = removeClassResult.replacingOccurrences(of: "</p>", with: "</span><br>")
//					removeClassResult = removeClassResult.replacingOccurrences(of: "</p>", with: "<br>")
					
					result += removeClassResult
				}
			

			}
			
//			result = "<div>" + result + "</div>"
			
		
			return result
			
		} catch {
			print("error:", error)
			return nil
		}
		
	}
	
	func regexMatches(for regex: String, in text: String) -> [String] {

		do {
			let regex = try NSRegularExpression(pattern: regex)
			let nsString = text as NSString
			let results = regex.matches(in: text, range: NSRange(location: 0, length: nsString.length))
			return results.map { nsString.substring(with: $0.range)}
		} catch let error {
			print("invalid regex: \(error.localizedDescription)")
			return []
		}
	}
	
}
