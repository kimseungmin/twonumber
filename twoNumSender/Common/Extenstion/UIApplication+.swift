import UIKit

extension UIApplication {
	/**
	UIApplication.statusBarUIView?.backgroundColor = .blue
	*/
	class var statusBarBackgroundColor: UIColor? {
		get {
			return statusBarUIView?.backgroundColor
		} set {
			statusBarUIView?.backgroundColor = newValue
		}
	}
	class var statusBarUIView: UIView? {
		if #available(iOS 13.0, *) {
			let tag = 987654321
			if let statusBar = UIApplication.shared.keyWindow?.viewWithTag(tag) {
				return statusBar
			}
			else {
				let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
				statusBarView.tag = tag
				UIApplication.shared.keyWindow?.addSubview(statusBarView)
				return statusBarView
			}
		} else {
			if responds(to: Selector(("statusBar"))) {
				return value(forKey: "statusBar") as? UIView
			}
		}
		return nil
	}
	
}

public extension UIWindow {
    static var key: UIWindow? {
        
        if #available(iOS 13, *) {
            if #available(iOS 15, *) {
                return UIApplication.shared.connectedScenes
                // Keep only active scenes, onscreen and visible to the user
                    .filter { $0.activationState == .foregroundActive || $0.activationState == .foregroundInactive }
                // Keep only the first `UIWindowScene`
                    .first(where: { $0 is UIWindowScene })
                // Get its associated windows
                    .flatMap({ $0 as? UIWindowScene })?.windows
                // Finally, keep only the key window
                    .first(where: \.isKeyWindow)
            }
            else {
                return UIApplication.shared.windows.first { $0.isKeyWindow }
            }
            
        }
        else {
            return UIApplication.shared.keyWindow
        }
        
    }
}
