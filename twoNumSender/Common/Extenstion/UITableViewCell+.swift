
import Foundation
import UIKit

extension UITableViewCell {
	/// Search up the view hierarchy of the table view cell to find the containing table view
	var tableView: UITableView? {
		get {
			var table: UIView? = superview
			while !(table is UITableView) && table != nil {
				let newSuperview = table?.superview
				table = newSuperview
			}
			return table as? UITableView
		}
	}
	
	//Reuse Identifier String
	public class var reuseIdentifier: String {
		return "\(self.self)"
	}
	
	// Registers the Nib with the provided table
	public static func registerWithTable(_ table: UITableView) {
		let bundle = Bundle(for: self)
		let nib = UINib(nibName: self.reuseIdentifier , bundle: bundle)
		table.register(nib, forCellReuseIdentifier: "\(self.reuseIdentifier)")
	}
}
