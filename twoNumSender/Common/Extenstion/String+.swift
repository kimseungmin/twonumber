//
//  String+.swift
//  twoNumSender
//
//  Created by seungmin kim on 12/07/2019.
//  Copyright © 2019 remain. All rights reserved.
//

import Foundation
import UIKit

// 특수문자 전체 제거
extension StringProtocol where Self: RangeReplaceableCollection {
    var removingAllWhitespacesAndNewlines: Self {
        return filter { !$0.isNewline && !$0.isWhitespace }
    }
    mutating func removeAllWhitespacesAndNewlines() {
        removeAll { $0.isNewline || $0.isWhitespace }
    }
}
extension StringProtocol {
    func firstIndexDistance(of string: Self) -> Int? {
        guard let index = range(of: string)?.lowerBound else { return nil }
        return distance(from: startIndex, to: index)
    }
	
    func convertToDictionary() -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }

}

extension String{
	func hasCharacters() -> Bool{
		do{
			let regex = try NSRegularExpression(pattern: "^[a-zA-Z가-힣ㄱ-ㅎㅏ-ㅣ\\s]$", options: .caseInsensitive)
			if let _ = regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions.reportCompletion, range: NSMakeRange(0, self.count)){
				return true
			}
		}catch{
			print(error.localizedDescription)
			return false
		}
		return false
	}
	func isEnglish() -> Bool{
		do{
			let regex = try NSRegularExpression(pattern: "^[a-zA-Z\\s]$", options: .caseInsensitive)
			if let _ = regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions.reportCompletion, range: NSMakeRange(0, self.count)){
				return true
			}
		}catch{
			print(error.localizedDescription)
			return false
		}
		return false
	}
	
	func getStringIndex() -> Int{
		let array = ["ㄱ","ㄴ","ㄷ","ㄸ","ㄹ","ㅁ","ㅂ","ㅃ","ㅅ","ㅇ","ㅈ","ㅊ","ㅋ","ㅌ","ㅍ","ㅎ","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z", "#"]
		
		if let idx = array.firstIndex(of: self){
			return idx
		}else{
			return 0
		}
		
	}
	
}

//MARK:- String Extension (HTML Dependency)
extension String {

	var convertSpecialCharacters: String {
		var newString = self
		let char_dictionary = [
			"&amp;" : "&",
			"&lt;" : "<",
			"&gt;" : ">",
			"&quot;" : "\"",
			"&apos;" : "'"
		];
		for (escaped_char, unescaped_char) in char_dictionary {
			newString = newString.replacingOccurrences(of: escaped_char, with: unescaped_char, options: NSString.CompareOptions.literal, range: nil)
		}
		return newString
	}
	
	// Html To Attributed String When Parse Data
	var htmlToAttributed: NSAttributedString? {
		guard let data = data(using: .utf8) else { return NSAttributedString() }
		do {
			return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html.rawValue, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
				
		} catch {
			return NSAttributedString()
		}
	}
	var htmlToString: String {
		return htmlToAttributed?.string ?? ""
	}

	func capturedGroups(withRegex pattern: String) -> [String] {
		var results = [String]()

		var regex: NSRegularExpression
		do {
			regex = try NSRegularExpression(pattern: pattern, options: [])
		} catch {
			return results
		}

		let matches = regex.matches(in: self, options: [], range: NSRange(location:0, length: self.count))

		guard let match = matches.first else { return results }

		let lastRangeIndex = match.numberOfRanges - 1
		guard lastRangeIndex >= 1 else { return results }

		for i in 1...lastRangeIndex {
			let capturedGroupIndex = match.range(at: i)
			let matchedString = (self as NSString).substring(with: capturedGroupIndex)
			results.append(matchedString)
		}

		return results
	}
		
}



//MARK:- String Extension (size Dependency)
extension String{
	func widthOfString(usingFont font: UIFont) -> CGFloat {
		let fontAttributes = [NSAttributedString.Key.font: font]
		let size = self.size(withAttributes: fontAttributes)
		return size.width
	}

	func heightOfString(usingFont font: UIFont) -> CGFloat {
		let fontAttributes = [NSAttributedString.Key.font: font]
		let size = self.size(withAttributes: fontAttributes)
		return size.height
	}

	func sizeOfString(usingFont font: UIFont) -> CGSize {
		let fontAttributes = [NSAttributedString.Key.font: font]
		return self.size(withAttributes: fontAttributes)
	}
}

// MARK:- String Extension (Utils Dependency)
extension String {
	
	init(bytes: [UInt8]) {
		self.init()
		self  = bytes.reduce("", { $0 + String(format: "%c", $1)})
	}
	
	var localized: String {
		return NSLocalizedString(self, comment: "")
	}
	
	func contains(find: String) -> Bool{
		return self.range(of: find) != nil
	}
	
	func containsIgnoringCase(find: String) -> Bool{
		return self.range(of: find, options: .caseInsensitive) != nil
	}
	
	
	func split(by length: Int) -> [String] {
		var startIndex = self.startIndex
		var results = [Substring]()
		
		while startIndex < self.endIndex {
			let endIndex = self.index(startIndex, offsetBy: length, limitedBy: self.endIndex) ?? self.endIndex
			results.append(self[startIndex..<endIndex])
			startIndex = endIndex
		}
		
		return results.map { String($0) }
	}
	
	/// random
	static func random(length: Int = 20) -> String {
		let base = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
		var randomString: String = ""
		
		for _ in 0..<length {
			let randomValue = arc4random_uniform(UInt32(base.count))
			randomString += "\(base[base.index(base.startIndex, offsetBy: Int(randomValue))])"
		}
		return randomString
	}
	
	/// toBool
	func toBool() -> Bool {
		switch self {
		case "True", "true", "yes", "1":
			return true
		case "False", "false", "no", "0":
			return false
		default:
			return false
		}
	}
	
	/// toInt
	func toInt() -> Int{
		let newString = self.replacingOccurrences(of: ".", with: "")
		let value: Int = Int(newString) ?? 0
		
		return value
		
	}
	/// toDict
	func toDict() -> [String: Any]? {
		if let data = self.data(using: .utf8) {
			do {
				return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
			} catch {
				print(error.localizedDescription)
			}
		}
		return nil
	}
	
	
}

