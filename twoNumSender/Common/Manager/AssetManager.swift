//
//  AssetManager.swift
//  CallRecorder
//
//  Created by vincent_kim on 2020/11/09.
//

import Foundation
import UIKit
import Photos
import MobileCoreServices
import AVFoundation

public enum MediaTypes:String{
	case image = "public.image"
	case movie = "public.movie"
}

public class AssetManager {
	
	static let shared = AssetManager()
	
	
	var resizeBaseWidth = 2400//(KE_OptionValueInfo.VALUE_MAX_IMAGE_RESIZE_WIDTH)
	var selectedImagePHAssets: [PHAsset] = []
	var selectedVideoPHAssets: [PHAsset] = []
	var selectedVideosDict: [Int:PHAsset] = [:]
	var selectedAttachs:[URL] = []
	/// 기존 선택한 Asset Contain 여부 삭제 2020.08.07
	func checkIsAssetExist(asset: PHAsset, type: MediaTypes) -> Bool{
		if type == .movie{
			
//			if selectedVideosDict.values.contains(asset){
//				return true
//			}else{
				return false
//			}
		}else{
//			if selectedImagePHAssets.contains(asset){
//				return true
//			}else{
				return false
//			}
		}
		
	}
	
	func appendImage(asset:PHAsset){
		self.selectedImagePHAssets.append(asset)
	}
	
	func appedVideo(paragraphID:Int, asset: PHAsset, isReplace:Bool = false, at:Int = 0) -> Bool{
	/// 기존 선택한 Asset Contain 여부 삭제 2020.08.07
//		if self.selectedVideosDict.values.contains(asset){
//			return false
//		}else{
			
			self.selectedVideosDict[paragraphID] = asset
			if isReplace{
				if selectedVideoPHAssets.count + 1 == at{
					self.selectedVideoPHAssets.append(asset)
				}else{
					self.selectedVideoPHAssets.insert(asset, at: at)
				}
				
			}else{
				self.selectedVideoPHAssets.append(asset)
			}
			
			
			return true
//		}
		
		
	}
	
	func getVideoPHAssetFrom(id:Int) -> PHAsset?{
		if let asset = self.selectedVideosDict[id]{
			return asset
		}else{
			return nil
		}
	}
	
	func getIDFromVideoPHAsset(asset:PHAsset) -> Int?{
		var id:Int?
		for item in self.selectedVideosDict{
			if item.value == asset{
				id = item.key
				return item.key
			}
		}
		
		return id
	}
	
	
	func getPHAssetList(type:PHAssetMediaType) -> [PHAsset]{
		if type == .video{
			return self.selectedVideoPHAssets
		}else{
			return self.selectedImagePHAssets
		}
		
	}
	
	func getSelectedVideos() -> [Int:PHAsset]{
		return self.selectedVideosDict
	}
	
	func isArrayAppendedComplete() -> Bool{
		return self.selectedVideoPHAssets.count == self.selectedVideosDict.count
	}
	
	func removeAsset(id:Int){
		guard let item = self.selectedVideosDict[id] else{
			return
		}
		self.selectedVideoPHAssets.remove(item)
		self.selectedVideosDict.removeValue(forKey: id)
	}
}

//MARK: - Image
extension AssetManager{
	
	func checkIsGif(asset: PHAsset) -> Bool{
		let options = PHContentEditingInputRequestOptions()
		options.isNetworkAccessAllowed = true //for icloud backup assets

		var isGif = false
		if let identifier = asset.value(forKey: "uniformTypeIdentifier") as? String
		{
			 if identifier == kUTTypeGIF as String
			 {
			   isGif = true
			 }
		}
		return isGif
		
	}
	
	func checkFileExtension(asset: PHAsset) -> String{
//		let options = PHContentEditingInputRequestOptions()
//		options.isNetworkAccessAllowed = true //for icloud backup assets
//
//		var fileExtension = ""
//		if let identifier = asset.value(forKey: "uniformTypeIdentifier") as? String{
//			fileExtension = identifier
//		}
		return URL(fileURLWithPath: (asset.value(forKey: "filename") as! String)).pathExtension
	}
	
	func requestImageFromAsset(with asset: PHAsset?, completion: @escaping (UIImage?, _ gifData:Data? , _ isGif: Bool?, _ fileExtension:String, _ isResized:Bool) -> Void) {
		var resultImage = UIImage()
		let imageManager = PHImageManager()
		var isResized:Bool = false
		guard let asset = asset else {
			completion(nil, nil, nil, "", false)
			return
		}
		
		let width:CGFloat = CGFloat(asset.pixelWidth)
		let height:CGFloat = CGFloat(asset.pixelHeight)
		
		var ratio:CGFloat = 1
		var actualSize = CGSize(width: width, height: height)
		// 긴 변검사
		if width > height{
			// 가로가 더 긴 케이스
			if width > CGFloat(self.resizeBaseWidth){
				ratio = width / CGFloat(self.resizeBaseWidth)
				actualSize = CGSize(width: CGFloat(self.resizeBaseWidth), height: height / ratio)
				isResized = true
			}
			
		}else{
			// 세로가 더 긴 케이스 & 같은 경우
			if CGFloat(height) > CGFloat(self.resizeBaseWidth){
				// 최대 길이 초과 시
				ratio = height / CGFloat(self.resizeBaseWidth)
				actualSize = CGSize(width: width / ratio, height: CGFloat(self.resizeBaseWidth))
				isResized = true
			}
			
		}
		
		
		let option = PHImageRequestOptions()
		option.resizeMode = PHImageRequestOptionsResizeMode.fast
		option.deliveryMode = PHImageRequestOptionsDeliveryMode.highQualityFormat
		option.isNetworkAccessAllowed = true
		option.isSynchronous = true
		
		if self.checkIsGif(asset: asset){
			/// gif
			
			if #available(iOS 13.0, *) {
				imageManager.requestImageDataAndOrientation(for: asset, options: option, resultHandler: { (imageData, UTI, _, _) in
					if let uti = UTI, let data = imageData ,
						// you can also use UTI to make sure it's a gif
					   UTTypeConformsTo(uti as CFString, kUTTypeGIF) {
						// save data here
						completion(nil, data , true, "gif", isResized)
						
					}
				})
			}
			else{
				imageManager.requestImageData(for: asset, options: option, resultHandler: { (imageData, UTI, _, _) in
					if let uti = UTI, let data = imageData ,
						// you can also use UTI to make sure it's a gif
					   UTTypeConformsTo(uti as CFString, kUTTypeGIF) {
						// save data here
						completion(nil, data , true, "gif", isResized)
						
					}
				})
			}
			
			
		}else{
			/// image
			imageManager.requestImage(for: asset, targetSize: actualSize, contentMode: .aspectFit, options: option, resultHandler: { image, info in
				// UIKit may have recycled this cell by the handler's activation time.
				  
				// Set the cell's thumbnail image only if it's still showing the same asset.
				guard let image = image else{
					return
				}
				resultImage = image
				
				let fileExtension = self.checkFileExtension(asset: asset)
				
				completion(resultImage, nil ,false, fileExtension, isResized)

			})
			
	
		}
		
		
	}
	
	
	
	func getSavedImage(assetUri: String) -> UIImage? {
		let dirUrl:String = URL(fileURLWithPath: self.getDir().absoluteString).appendingPathComponent(assetUri).path

		let image: UIImage? = UIImage(contentsOfFile: dirUrl)
		return image

	}
	

	
	func saveImage(image: UIImage, isGif: Bool, fileExtension:String, isCompress:Bool, onSuccess: @escaping ((Bool, String) -> Void)) {
		
//		let name = self.uniqueFileName(fileExtension: "jpg")
		let name = self.uniqueFileName(fileExtension: fileExtension)
		
		
		var quality:CGFloat = 0.55
		if isCompress{
			quality = 0.9//KE_OptionValueInfo.VALUE_COMPRESS_IMAGE_QUALITY
		}
		guard let data = image.jpegData(compressionQuality: quality) ?? image.pngData() else { return }
		let dirUrl:NSURL = URL(fileURLWithPath: self.getDir().absoluteString) as NSURL
		do {
			try data.write(to: dirUrl.appendingPathComponent(name)!)
			onSuccess(true, name)
		
		} catch let error as NSError {
			print("Could not saveImage🥺: \(error), \(error.userInfo)")
			onSuccess(false, "")
		}

	}
	
	func saveGifImage(gifData: Data, isGif: Bool, onSuccess: @escaping ((Bool, String) -> Void)) {
		let name = self.uniqueFileName(fileExtension: "gif")
		
		let dirUrl:NSURL = URL(fileURLWithPath: self.getDir().absoluteString) as NSURL
		do {
			try gifData.write(to: dirUrl.appendingPathComponent(name)!)
			onSuccess(true, name)
		
		} catch let error as NSError {
			print("Could not saveImage🥺: \(error), \(error.userInfo)")
			onSuccess(false, "")
		}
		

	}
	
}

//MARK: - Video
extension AssetManager{
	func resolutionSizeForLocalVideo(url:NSURL) -> CGSize? {
		guard let track = AVAsset(url: url as URL).tracks(withMediaType: AVMediaType.video).first else { return nil }
		let size = track.naturalSize.applying(track.preferredTransform)
		return CGSize(width: abs(size.width), height: abs(size.height))
		
	}
	
	func getVideoInfo(path: String, completion: @escaping ((_ length:Int, _ height:Float, _ width: Float, _ fileSize: Int, _ bitRate:Int) -> Void)){
		guard let videoURL = self.getURLFromPath(path: path) else { return }
		
		let duration = AVURLAsset(url: videoURL).duration.seconds
		print("getVideoInfo duration : \(duration)")
		let time: String
		if duration > 3600 {
			time = String(format:"%dh %dm %ds",
				Int(duration/3600),
				Int((duration/60).truncatingRemainder(dividingBy: 60)),
				Int(duration.truncatingRemainder(dividingBy: 60)))
		} else {
			time = String(format:"%dm %ds",
				Int((duration/60).truncatingRemainder(dividingBy: 60)),
				Int(duration.truncatingRemainder(dividingBy: 60)))
		}
		print("getVideoInfo time : \(time)")
		
		let length = Int(duration)
		let frameSize = AVURLAsset(url: videoURL).screenSize
		let width = Float(frameSize?.width ?? 0.0)
		let height = Float(frameSize?.height ?? 0.0)
		let fileSize = self.getFileSizeForByte(url: videoURL)
		
		var bps = 0
		
		let tracks = AVURLAsset(url: videoURL).tracks
		for track in tracks{
			if track.mediaType == .video{
				bps = Int(track.estimatedDataRate)
			}
			
		}
		
		
		completion(length, height, width, fileSize, bps)

	}
	

	func getURLForTempVideo(asset: PHAsset, completionHandler : @escaping ((_ responseURL : URL?) -> Void)) {
		let imageManager = PHImageManager()

		let option = PHVideoRequestOptions()
		option.version = .original
		option.isNetworkAccessAllowed = true
		option.deliveryMode = .automatic
		option.progressHandler = nil

		imageManager.requestAVAsset(forVideo: asset, options: option, resultHandler: { (asset, audioMix, info) -> Void in

				if let asset = asset as? AVURLAsset {
					completionHandler(asset.url)

				}else{
					completionHandler(nil)

			}

		})
	}
	
	// 동영상 압축
	/*
	Options
	AVAssetExportPresetLowQuality
	AVAssetExportPresetMediumQuality
	AVAssetExportPresetHighestQuality
	AVAssetExportPreset640x480
	AVAssetExportPreset960x540
	*/
	func compressVideo(inputURL: URL, outputURL: URL, paragraphID:Int, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
		let urlAsset = AVURLAsset(url: inputURL, options: nil)
		guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetMediumQuality) else {
			handler(nil)

			return
		}
		exportSession.outputURL = outputURL
		exportSession.outputFileType = AVFileType.mp4
		exportSession.shouldOptimizeForNetworkUse = true
		var exportProgressBarTimer = Timer() // initialize timer
		
		DispatchQueue.main.async { // [weak self] in
//			guard let self = self else { return }
			exportProgressBarTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { timer in
				let progress = exportSession.progress
				
				if (progress < 0.99) {
					
					let userInfo: [String: Any] = ["progress":progress,
												   "paragraphID":paragraphID
												   ]
					NotificationCenter.default.post(name: NSNotification.Name(rawValue: "compressProgress"), object: nil, userInfo: userInfo)
					
					
				}
				
			}
		}
		
		exportSession.exportAsynchronously { () -> Void in
			
			if exportSession.status == .completed {
				exportProgressBarTimer.invalidate()
			}
			
			handler(exportSession)
		}
	}

	
	func saveVideo(with asset: PHAsset?, paragraphID:Int, completion: @escaping (Bool, String?) -> Void) {
		
		let name = self.uniqueFileName(fileExtension: "mp4")
		let imageManager = PHImageManager()
		
		
		guard let asset = asset else {
			completion(false, nil)
			return
		}
		

//		print(debug: asset.assetFileSizeMB)
		let option = PHVideoRequestOptions()
		option.version = .original
		option.isNetworkAccessAllowed = true
		option.deliveryMode = .automatic
		option.progressHandler = nil
		
		
		imageManager.requestAVAsset(forVideo: asset, options: option, resultHandler: { (asset, audioMix, info) -> Void in
			
			if let asset = asset as? AVURLAsset {
				do {
//					let videoData = try  Data.init(contentsOf: asset.url)
					let _ = try  Data.init(contentsOf: asset.url)
//					print(asset.url)
//					print("File size before compression: \(Double(videoData.count / 1048576)) mb")
					
					print("File size before compression: \(asset.url.fileSizeString)")
					guard let compressedURL = self.getURLFromPath(path: name) else { return }
					
					self.compressVideo(inputURL: asset.url , outputURL: compressedURL, paragraphID: paragraphID) { (exportSession) in
						guard let session = exportSession else {
							return
						}
						
						switch session.status {
						case .unknown:
							print("unknown")
							break
						case .waiting:
							print("waiting")
							break
						case .exporting:
							print("exporting")
							break
						case .completed:
							
							do {
								let compressedData = try  Data.init(contentsOf: compressedURL)
								
									print("File size AFTER compression Byte: \(compressedData)")
									print("File size AFTER compression: \(compressedURL.fileSizeString)")
									
									completion(true, name)
								}
							catch{
							   print(error)
							}


						case .failed:
							print("failed")
							break
						case .cancelled:
							print("cancelled")
							break
						@unknown default:
							fatalError("somethings Wrong 🥺")
						}
					}
					
					
				} catch {
					print(error)
					//return
				}
				
			}
			
		})

	}
	func getThumbnailFromPath(path: String) -> UIImage?{
		
		var image : UIImage!
		
		// 동영상 인코딩 중인 경우
		if path.hasPrefix("file:"){
			guard let url = URL(string: path) else {
				return nil
			}
			
			guard let getImage = self.generateThumbnailFromUrl(path: url) else {
				return nil
			}
			image = getImage
			
		}
		else{
			guard let getImage = self.generateThumbnail(path: path) else {
				return nil
			}
			image = getImage
		}
		
		return image
	}
	
	func generateThumbnailFromUrl(path: URL) -> UIImage? {
		do {
			let asset = AVURLAsset(url: path, options: nil)
			let imgGenerator = AVAssetImageGenerator(asset: asset)
			imgGenerator.appliesPreferredTrackTransform = true
			let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
			let thumbnail = UIImage(cgImage: cgImage)
			return thumbnail
		} catch let error {
			print("*** Error generating thumbnail: \(error.localizedDescription)")
			return nil
		}
	}
	
	func generateThumbnail(path: String) -> UIImage? {
		var bundleURL:URL?
		if path.hasPrefix("http"){
			bundleURL = URL(string: path)
		}
		else{
			bundleURL = self.getURLFromPath(path: path)
		}
//		guard let bundleURL = self.getURLFromPath(path: path) else { return nil }
		guard let bundle = bundleURL else { return nil }
		
		do {
			let asset = AVURLAsset(url: bundle, options: nil)
			let imgGenerator = AVAssetImageGenerator(asset: asset)
			imgGenerator.appliesPreferredTrackTransform = true
			let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
			let thumbnail = UIImage(cgImage: cgImage)
			return thumbnail
		} catch let error {
			print("*** Error generating thumbnail: \(error.localizedDescription)")
			return nil
		}
		
	}
	

	
}


//MARK: - Common
extension AssetManager{
	func uniqueFileName(fileExtension: String) -> String {
		
		let uniqueString: String = ProcessInfo.processInfo.globallyUniqueString
		let formatter = DateFormatter()
		formatter.dateFormat = "yyyyMMddhhmmsss"
		let dateString: String = formatter.string(from: Date())
		let uniqueName: String = "\(uniqueString)_\(dateString)"
		if fileExtension.count > 0 {
			let fileName: String = "\(uniqueName).\(fileExtension)"
			return fileName
		}

		return uniqueName
	}
	
	func getFileSizeForByte(url: URL?) -> Int {
		guard let filePath = url?.path else {
			return 0
		}
		do {
			let attribute = try FileManager.default.attributesOfItem(atPath: filePath)
			if let size = attribute[FileAttributeKey.size] as? NSNumber {
				return size.intValue
			}
		} catch {
			print("Error: \(error)")
		}
		return 0
	}
	func getURLFromPath(path: String) -> URL?{
		
		let bundle = URL(fileURLWithPath: AssetManager.shared.getDir().absoluteString).appendingPathComponent(path)
		return bundle

	}
	
	func getFileName(path: String) -> String{
		return URL(fileURLWithPath: path).deletingPathExtension().lastPathComponent
	}
	
	func getFileExtension(path: String) -> String{
		URL(fileURLWithPath: path).pathExtension
	}


}


//MARK: - Attach
extension AssetManager{
	func appendAttach(path: URL) -> Bool{
		if self.selectedAttachs.count > 5{
			return false
		}else{
			self.selectedAttachs.append(path)
			return true
		}
	}
}

//MAR: - Common
extension AssetManager{
	func getAllAssetUrls(skipsHiddenFiles: Bool = true) -> [URL]? {
		let documentsURL = AssetManager.shared.getDir()
		let manager = FileManager.default
		let fileURLs = try? manager.contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: nil, options: skipsHiddenFiles ? .skipsHiddenFiles : [] )
		return fileURLs
	}
	
	func deleteAsset(assetUri: String, onSuccess: @escaping ((Bool) -> Void)) {

		guard let bundle = URL(string: assetUri) else {
			Debug.print("cannot convert to url")
			onSuccess(false)
			return
		}
		let manager = FileManager.default
		
		if manager.fileExists(atPath: bundle.path) {
			
			do {
				try manager.removeItem(at: bundle)
				onSuccess(true)
				
			} catch {
				print("delete()",error.localizedDescription)
			}
			
		} else {
			Debug.print("Could not deleteAsset🥺\nFile is not exist.")
			onSuccess(false)
			
			
		}
		
	}
	
	func getDir() -> URL {
		
		let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
		
		return paths.first!
	}
	
	func getAssetData(assetUri:String) -> Data? {
		
		let bundle = URL(fileURLWithPath: AssetManager.shared.getDir().absoluteString).appendingPathComponent(assetUri)
		
		guard let data = try? Data(contentsOf: bundle) else {
			print("SwiftGif: Cannot turn image named \"\(assetUri)\" into NSData")
			return nil
		}
		
		return data
	}
}
