//
//  CallActionManager.swift
//


import Foundation
import UIKit
import Contacts
import ContactsUI

final class CallActionManager {
    
    static let shared = CallActionManager()
    
    private let contactStore = CNContactStore()
    
    private var savedIdentifier: String? = nil
}

extension CallActionManager {
    func actionCall(
        name: String,
        number: String,
        isOneNumber: Bool
    ) {
        
        self.startCallTwoNumberWithContact(
            name: name, number: number
        )
        
//        let app = UIApplication.shared
//
//        guard let encoded = number.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else { return }
//
//        if !isOneNumber {
//            if #available(iOS 15.0, *) {
//                self.startCallTwoNumberWithContact(
//                    name: name, number: number
//                )
//                return
//            }
//
//        }
//
//        let u = "tel://\(encoded)"
//        guard let url = URL(string: u) else { return }
//        if app.canOpenURL(url) {
//            app.open(url)
//        }
        
    }
    private func startCallTwoNumberWithContact(
        name: String,
        number: String
    ) {
        self.checkAddrBookPermission(completion: { [weak self] (isEnable) in
            guard let self = self else { return }
            if isEnable {
                if let contact = self.createAddressAndSave(
                    name: name,
                    phoneNumber: number
                ) {
                    let vc = CNContactViewController(forUnknownContact: contact)
                    guard let topVC = UIApplication.topViewController() else { return }
                    topVC.navigationController?.setNavigationBarHidden(false, animated: false)
                    topVC.navigationController?.pushViewController(vc, animated: true)
//                    self.removeTempAddress()
                }
                
            }
        })
    }
    

}

extension CallActionManager {
    /// 연락처 저장
    private func createAddressAndSave(name: String, phoneNumber: String) -> CNContact? {
        
        let contact = CNMutableContact()
        // Name
        contact.givenName = name

        // Phone
        contact.phoneNumbers.append(
            CNLabeledValue(
                label: "투넘버 문자",
                value: CNPhoneNumber(stringValue: phoneNumber)
            )
        )
        
        contact.note = "투넘버로 전화 발신을 위한 임시 연락처 입니다\niOS 15이상 버전부터 사용됩니다\n[주의사항]\n이 상태에서 문자 발송 시 원넘버로 전송됩니다"
        return contact
//        // Save
//        let saveRequest = CNSaveRequest()
//        saveRequest.add(contact, toContainerWithIdentifier: nil)
//        do {
//            try self.contactStore.execute(saveRequest)
//            savedIdentifier = contact.identifier
//            Debug.print(contact.identifier)
//            return contact
//        }
//        catch {
//            return nil
//        }
        
    }
    
    /// 연락처 삭제 (Contact 화면 출력 이 후 즉시 삭제)
    private func removeTempAddress() {

        let keys: [CNKeyDescriptor] = [
            CNContactIdentifierKey,
            CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
            CNContactPhoneNumbersKey,
            CNContactEmailAddressesKey,
            CNContactJobTitleKey,
            CNContactPostalAddressesKey
            
        ] as! [CNKeyDescriptor]
        
        if let id = self.savedIdentifier {
            do {
                let contact = try self.contactStore.unifiedContact(withIdentifier: id, keysToFetch: keys)
                
                let req = CNSaveRequest()
                let mutableContact = contact.mutableCopy() as! CNMutableContact
                req.delete(mutableContact)
                try self.contactStore.execute(req)
                
            }
            catch {
                Debug.print(error.localizedDescription)
            }
        }
    }
    
}

extension CallActionManager {
    /// 연락처 접근 권한 Alert
    private func checkAddrBookPermission(completion: @escaping (Bool) -> Void) {
        let authorizationStatus = CNContactStore.authorizationStatus(for: CNEntityType.contacts)
        
        switch authorizationStatus {
        case .denied, .notDetermined:
            self.contactStore.requestAccess(for: CNEntityType.contacts, completionHandler: { (access, accessError) -> Void in
                if !access {
                    if authorizationStatus == CNAuthorizationStatus.denied {
                        DispatchQueue.main.async { [weak self] in
                            guard self != nil else { return }
                        
                            let message = "ios15 이상 버전은 특수문자 전화 바로 걸기가 불가합니다\n\n기능사용을 위해 설정에서 접근권한을 허용하세요."
                            
                            let dismissAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (action) -> Void in
                                completion(false)
                            }
                            
                            UIAlertController.showAlert(title: "연락처 접근", message: message, actions: [dismissAction])
                            
                        }
                    }
                }
            })
                
        default:
            completion(true)
        }
        
    }
}
