//
//  CallKitManager.swift
//  twoNumSender
//
//  Created by seungmin kim on 02/06/2020.
//  Copyright © 2020 remain. All rights reserved.
//

import Foundation
import CallKit
import SVProgressHUD
import CoreData
import StoreKit
import ObjectMapper


enum SaveCallKitDataCycleType:Int{
    case first
    case reload
}

class CallKitManager: NSObject{
    
    static let shared = CallKitManager()
    
//    private var errorCnt = 0
//    private let appDelegateObj : AppDelegate = UIApplication.shared.delegate as! AppDelegate
//    private var saveCallKitDataCycle: SaveCallKitDataCycleType = .first
//    private var isSuccessSync: Bool = false
	
    private override init(){}
    
    
}


//MARK: - Method
extension CallKitManager{
	func saveIsPurchased(status:Bool){
		let userDefaults = UserDefaults(suiteName: CallKitInfo.suite_name)
		userDefaults?.set(status, forKey: "purchase")
	}

	public func saveCallKitData(isProgress:Bool = true,
                                success: ( () -> Void)?,
                                failure: ( () -> Void)?
    ) {
		if isProgress{
			SVProgressHUD.show()
		}
		
		self.checkCallkitEnable(success: {
			self.makeCallkitData(completion: {
				self.reloadCallKitSync()
				success?()
				
			}, failure: {
				failure?()
				SVProgressHUD.dismiss()
			})
			
		}, failure: { (errType) in
			
			// CALL KIT 권한 체크 FAIL
			Debug.print("CALL KIT 권한 체크 FAIL")
			LogManager.sharedInstance.write("CALL KIT 권한 체크 FAIL")
			let title = TextInfo.alert_title
			var msg = ""
			
			switch errType{
				
				case .unknown:
					msg = "발신번호 표시 기능 설정 중 입니다\n최초 앱 설치 시 최대 1시간까지 소요될 수 있습니다\n잠시 후 다시 시도 해주세요"
				case .disabled:
					msg = "발신번호 표시 권한을 확인해주세요"
				case .enabled:
					break
				@unknown default:
					msg = "발신번호 표시 기능 설정 중 입니다\n최초 앱 설치 시 최대 1시간까지 소요될 수 있습니다\n잠시 후 다시 시도 해주세요"
			}
			
			let okAction = UIAlertAction(title: TextInfo.alert_confirm, style: .default, handler: {action in
				self.openSettingAlert()
			})
			UIAlertController.showAlert(title: title, message: msg, actions: [okAction])
			SVProgressHUD.dismiss()
			failure?()
			
		})
		
	}
	
	
	
//	/** 임시 저장 */
//	private func makeDummyData(completion: @escaping () -> Void, failure: @escaping ()->Void){
////		let data :[[String:String]] = [["name":""],
////										 ["phoneNumber":""]]
//        let data:[ContactModel] = [ContactModel(seq: 0, name: "", phone: "")]
//        let json = data.toJSONString()
//        let userDefaults = UserDefaults(suiteName: CallKitInfo.suite_name)
//        userDefaults?.set(json, forKey: "data")
//        completion()
//
//	}
	
	/** 데이터 저장 */
	public func makeCallkitData(completion: @escaping () -> Void, failure: @escaping ()->Void){
        SVProgressHUD.show()
        /*기존 연락처전체*/
//		var dbData = [[String:String]]()
        var dropData = [[String:String]]()

		let data: [ContactRealmModel] = RealmManager.shared.getAllContacts() ?? []
        var models:[ContactModel] = []
		Debug.print("start")
//		for i in 0 ... 99999999{
//			
//			print(String(format: "%08d", i))
//		}
		Debug.print("end")
//		models.append(ContactModel(seq: 0, name: "070 차단", phone: "070"))

        var expectModels:[ContactModel] = []
        for el in data{
            models.append(ContactModel(seq: el.seq, name: el.name, phone: el.phone, memo: el.memo))
        }
        
        for model in models{
            
            var item = ""
            
            model.phone.forEach { el in
                let v = String(el)
                if Int(v) != nil || v == "*" {
                    item += v
                }
            }
//            print(item)
//            item = item.replacingOccurrences(of: "‬", with: "")
//            item = item.replacingOccurrences(of: "‬", with: "")
//            item = item.replacingOccurrences(of: "‭", with: "")
//            item = item.replacingOccurrences(of: ".", with: "")
            
            // 특수 문자 제거
            item = item.removingAllWhitespacesAndNewlines
            
            // 기존 규칙 제거
//            item = item.replacingOccurrences(of: "", with: "")
//            item = item.replacingOccurrences(of: " ", with: "")
            
            
            if item.contains("*281"){
                item = item.replacingOccurrences(of: "*281", with: "")
            }
            
            if item.contains("*77"){
                item = item.replacingOccurrences(of: "*77", with: "")
            }
            // ulpus 발신전화는 82를 붙이지 않음
            var uplusTel: String? = nil
            if item.contains("*"){
                item = item.replacingOccurrences(of: "*", with: "")
                uplusTel = item
            }

            item = "82" + item.dropFirst()
            
            let emojiName = "☎️ \(model.name)"
            item = item.replacingOccurrences(of: "‬", with: "")
            item = item.replacingOccurrences(of: "‭", with: "")
            
            uplusTel = uplusTel?.replacingOccurrences(of: "‬", with: "")
            uplusTel = uplusTel?.replacingOccurrences(of: "‭", with: "")
            
            
            if (item.prefix(4) == "*77‭") || (item.prefix(4) == "*28") || (item.prefix(1) == "*") || (item.count > 12) || (item.count < 7){
                // 로그용
                
                dropData.append(["phoneNumber" : item , "name" : emojiName])

            }
            else if expectModels.filter({ $0.phone == item}).count > 0{
//                (item.contains { $0["phoneNumber"] == item }){
                
                dropData.append(["phoneNumber" : item , "name" : emojiName])
                
            }
            else{
//                        print(debug: "appended Data\n\(replaceMotype)")
                model.phone = item
                model.name = emojiName
                expectModels.append(model)
                if let uplusTel = uplusTel {
                    expectModels.append(ContactModel(seq: 0, name: emojiName, phone: uplusTel))
                }
                uplusTel = nil
//                dbData.append(["phoneNumber" : item , "name" : emojiName])
                
            }
            
        }
        let removeDupModels = expectModels.filterDuplicate{ $0($1.phone) }
		var validModels:[ContactModel] = []
		for model in removeDupModels{
			if let _ = Int(model.phone){
				validModels.append(model)
			}
		}
		
		var isPurchased:Bool = false
		if let isPurchase = IAPManager.shared.getPurchaseStatus(){
			isPurchased = isPurchase
			
		}
		
		if validModels.isEmpty || !isPurchased{
			validModels = [ ContactModel(seq: 0, name: "", phone: "") ]
		}
		
		
		
		
        let jsonModels = validModels.sorted{ Int($0.phone)! < Int($1.phone)! }
        let json = jsonModels.toJSONString()
        
        if let userDefaults = UserDefaults(suiteName: CallKitInfo.suite_name){
            
            userDefaults.set(json, forKey: "data")
            completion()
            
        }else{
            Debug.print("cannot create userdefault json")
            failure()
        }
        Debug.print("droped Data============\n\(dropData)")
        LogManager.sharedInstance.write("droped Data============\n\(dropData)", self)

        
    }

	/** 권한 체크 */
	func checkCallkitEnable(success: @escaping() -> Void, failure: @escaping(CXCallDirectoryManager.EnabledStatus) -> Void){
		// 최초 1회 userdefault 적용
		
		if let _ = UserDefaults(suiteName: CallKitInfo.bundle_name){
			
		}
		else{
			self.saveCallKitData(success: {}, failure: {})
			
		}
		
		let callDirManager = CXCallDirectoryManager.sharedInstance
		callDirManager.getEnabledStatusForExtension(withIdentifier: CallKitInfo.bundle_name, completionHandler: { (enableStatus, error) in
			if let error = error{
				
                Debug.print(error.localizedDescription)
				LogManager.sharedInstance.write("권한체크 성공 : enabled" + CallKitInfo.bundle_name, self)
				failure(.unknown)
				
			}
			else{
				switch enableStatus{
					case .disabled:
						failure(.disabled)
						LogManager.sharedInstance.write("권한체크 실패 : disabled" + CallKitInfo.bundle_name, self)
						break
					case .enabled:
						success()
						break
					case .unknown:
						failure(.unknown)
						LogManager.sharedInstance.write("권한체크 실패 : unknown" + CallKitInfo.bundle_name, self)
						break
					@unknown default:
						failure(.unknown)
						LogManager.sharedInstance.write("권한체크 실패 : unknown" + CallKitInfo.bundle_name, self)
						fatalError("CallKit 권한체크 unknown 에러")
						break
				}
			}
			
		})
		
		
    }

	
	func reloadCallKitSync(){
		Debug.print("Save Call Kit Started")
		let callDirManager = CXCallDirectoryManager.sharedInstance
		callDirManager.reloadExtension(withIdentifier: CallKitInfo.bundle_name, completionHandler: { (error) in
			SVProgressHUD.dismiss()
			if let error = error{
				
				let errCode = (error as NSError).code
				LogManager.sharedInstance.write(error.localizedDescription , self)

				let title = TextInfo.alert_title
				var msg = "동기화 에러가 발생하였습니다.\n앱 종료 후 다시 시도 해 주세요."
				if errCode == 4097{
					Debug.print("보조 응용 프로그램과 통신할 수 없습니다.")
				}
				let callKitErrCode:CallKitErrorCode = CallKitErrorCode(rawValue: errCode) ?? .unknown
				switch callKitErrCode {
					
					case .unknown:
					msg = "동기화 에러가 발생하였습니다.\n앱 종료 후 다시 시도 해 주세요."
					case .noExtensionFound:
					msg = "발신자 표시 기능이 확인되지 않습니다"
					case .loadingInterrupted:
					msg = "동기화가 강제종료 되었습니다\n앱 종료 후 다시 시도해 주세요"
					case .entriesOutOfOrder:
					msg = "연락처 동기 이상이 발생하여 중지되었습니다"
					case .duplicateEntries:
					msg = "중복된 연락처가 존재합니다"
					case .maximumEntriesExceeded:
					msg = "최대 등록가능 개수를 초과하였습니다"
					case .extensionDisabled:
					msg = "발신자 표시 기능이 비 활성화 되었습니다"
					case .currentlyLoading:
					msg = "차단기능 활성화 중 입니다\n사용환경에 따라 최대 1시간까지 소요될 수 있습니다\n앱을 강제종료하는 경우 설정이 정상적으로 이루어지지 않을 수 있습니다"
					case .unexpectedIncrementalRemoval:
					msg = "예기치 않은 오류가 발생했습니다"
				}
				
				Debug.print("\(msg) + code : \(errCode)")
				let okAction = UIAlertAction(title: TextInfo.alert_cancel, style: .default, handler: {action in
					LogManager.sharedInstance.write(error.localizedDescription + "\n" + msg , self)
				})
				UIAlertController.showAlert(title: title, message: msg, actions: [okAction])

			}else{

				Debug.print("Save Call Kit Success")
			}
		})
		
		
	}
	
    
}


//MARK:- Alert
extension CallKitManager{
    /*설정 alert*/
    func openSettingAlert(){
        let title = "설정이 필요합니다"
        let msg = "아래경로에서 투넘버 문자를 활성화 해 주세요\n설정 -> 전화 -> 전화 차단 및 발신자 확인"
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        
		if #available(iOS 13.4, *) {
			alert.addAction(UIAlertAction(title: "이동하기", style: .default) { action in
				self.moveToSetting()
			})
			
		}
        
        alert.addAction(UIAlertAction(title: "나중에", style: .destructive) { action in
            
        })
        
        UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
        
    }
	func moveToSetting(){
		if #available(iOS 13.4, *) {
			CXCallDirectoryManager.sharedInstance.openSettings(completionHandler: { (error) in
				if let error = error{
					Debug.print("open Setting Error\(error)")
				}
			})
		}
	}
}


public enum CallKitErrorCode : Int {

    public typealias _ErrorType = CXErrorCodeCallDirectoryManagerError

    case unknown                       // Code=0

    case noExtensionFound              // Code=1

    case loadingInterrupted            // Code=2

    case entriesOutOfOrder             // Code=3

    case duplicateEntries              // Code=4

    case maximumEntriesExceeded        // Code=5

    case extensionDisabled             // Code=6

    @available(iOS 10.3, *)
    case currentlyLoading              // Code=7

    @available(iOS 11.0, *)
    case unexpectedIncrementalRemoval  // Code=8
}


