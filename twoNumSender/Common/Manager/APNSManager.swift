//
//  APNSManager.swift
//  twoNumSender
//
//  Created by vincent_kim on 2020/10/04.
//  Copyright © 2020 remain. All rights reserved.
//

import Foundation
import UserNotifications
import SwiftyJSON

class APNSManager {
    
    static let shared = APNSManager()

    var deviceToken:String?
    var aps: Aps?
    var fcm_option: FCM_OPTION?
	var phone: String = ""
	var name: String = ""
    
    
    var click_link: String?
    var large_image: String?
    var large_body: String?
	var isDidRecieve:Bool = false

    func userInfo(_ data: [AnyHashable : Any]?) {
        let userInfo = data != nil ? data : sampleData()
        Debug.print("[origin data]\n \(String(describing: userInfo))")
        
//		guard let message = userInfo?["message"] as? [AnyHashable : Any] else {
//			print(debug: "[parsing error] message Key not found")
//			return
//		}
//
//		guard let data = message["data"] as? [AnyHashable : Any] else {
//			print(debug: "[parsing error] data Key not found")
//			return
//		}
		guard let info = userInfo?["userInfo"] as? [AnyHashable : Any] else {
			Debug.print("[userInfo] is not available")
			return
		}
		guard let phone = info["phone"] as? String else{
            Debug.print("[phone] is not available")
            return
        }
        
        self.phone = phone
		
		guard let name = info["name"] as? String else{
            Debug.print("[name] is not available")
            return
        }
		self.name = name
        
//		guard let aps = userInfo?["aps"] as? [AnyHashable: Any] else {
//            print(debug: "[parsing error] aps key not found")
//            return
//        }
//
//        self.aps = Aps(aps)
//        self.fcm_option = FCM_OPTION(userInfo!)
//
//        self.large_image = userInfo?["large-image"] as? String
//        self.large_body  = userInfo?["large-body"] as? String
//        self.click_link  = (userInfo?["click-link"] as? String)?.lowercased()
        
//        if let click_link = self.click_link, let schemeURL = URL(string: click_link) {
//            let _ = SchemeManager.shared.handleOpenUrl(url: schemeURL)
//        }
    }

    
    struct Aps {
        var body: String?
        var title: String?
        var mutable_content: String?
        var badge: String?
        var sound: String?
        
        init(_ aps: [AnyHashable: Any]) {
            if let alert = aps["alert"] as? [AnyHashable: String] {
                self.body  = alert["body"]
                self.title = alert["title"]
            }
            
            self.badge = aps["badge"] as? String
            self.mutable_content = aps["mutalbe-content"] as? String
            self.sound = aps["sound"] as? String
        }
    }
    
    struct FCM_OPTION {
        var image: String?
        
        init(_ aps: [AnyHashable: Any]) {
            self.image = aps["image"] as? String
        }
    }
}


//MARK: - Sample
extension APNSManager {
	func setIsDidRecieve(value:Bool){
		self.isDidRecieve = value
	}
	func getIsDidRecieve() -> Bool{
		self.isDidRecieve
	}
    func getDeviceToken() -> String{
        return self.deviceToken ?? ""
    }
    func setDeviceToken(token:String){
        self.deviceToken = token
    }

    
    func sampleData() -> [String: Any]? {
        let jsonString = """
        {
          "message": {
            "token": "bk3RNwTe3H0:CI2k_HHwgIpoDKCIZvvDMExUdFQ3P1...",
            "notification": {
              "title": "테스트 데이터",
              "body": "테스트 데이터 내용",
              "image": ""
            },
            "data": {
              "body": "821034747123",
              "image": "",
              "click_action": "Project100://webview/project/1405",
        "phone":"821012341234"
            },
            "apns": {
              "headers": {
                "apns-priority": "10"
              },
              "payload": {
                "aps": {
                  "sound": "default"
                }
              }
            }
          }
        }
        """
        
		return jsonString.convertToDictionary()
    }
}

extension UNNotificationSettings {

    func isAuthorized() -> Bool {
        guard authorizationStatus == .authorized else {
            return false
        }

        return alertSetting == .enabled ||
            soundSetting == .enabled ||
            badgeSetting == .enabled ||
            notificationCenterSetting == .enabled ||
            lockScreenSetting == .enabled
    }
}
