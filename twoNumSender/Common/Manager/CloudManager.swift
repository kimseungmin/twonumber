//
//  CloudManager.swift
//  twoNumSender
//
//  Created by vincent_kim on 2020/06/05.
//  Copyright © 2020 remain. All rights reserved.
//

import Foundation
import CloudKit
import CoreData
import ObjectMapper

class CloudManager: NSObject{
	static let shared = CloudManager()
	let database = CKContainer.default().privateCloudDatabase
	
	private override init() {
		 
	}
}

extension CloudManager{
	
	func getCloudRecord(recordType:String, result: @escaping (_ objects: [CKRecord]?, _ error: Error?) -> Void) {
		// predicate
		let predicate = NSPredicate(value: true)
		
		// query
		let cloudKitQuery = CKQuery(recordType: recordType, predicate: predicate)
		
		// records to store
		var records = [CKRecord]()


		// recurrent operations function
		var recurrentOperationsCounter = 101
		func recurrentOperations(cursor: CKQueryOperation.Cursor?){
			let recurrentOperation = CKQueryOperation(cursor: cursor!)
			recurrentOperation.recordFetchedBlock = { (record:CKRecord!) -> Void in
				print("-> getCloudRecord - recurrentOperations - fetch \(recurrentOperationsCounter)")
				recurrentOperationsCounter += 1

				records.append(record)
			}
			recurrentOperation.queryCompletionBlock = { (cursor: CKQueryOperation.Cursor?, error: Error?) -> Void in
				if ((error) != nil) {
					print("-> getCloudRecord - recurrentOperations - error - \(String(describing: error))")
					result(nil, error)
				} else {
					if cursor != nil {
						print("-> getCloudRecord - recurrentOperations - records \(records.count) - cursor \(cursor!.description)")
						recurrentOperations(cursor: cursor!)
					} else {
						print("-> getCloudRecord - recurrentOperations - records \(records.count) - cursor nil - done")
						result(records, nil)
					}
				}
			}
			database.add(recurrentOperation)
		}
		// initial operation
		var initialOperationCounter = 1
		let initialOperation = CKQueryOperation(query: cloudKitQuery)
		initialOperation.recordFetchedBlock = { (record:CKRecord!) -> Void in
			print("-> getCloudRecord - initialOperation - fetch \(initialOperationCounter)")
			initialOperationCounter += 1

			records.append(record)
		}
		initialOperation.queryCompletionBlock = { (cursor: CKQueryOperation.Cursor?, error: Error?) -> Void in
			if ((error) != nil) {
				print("-> getCloudRecord - initialOperation - error - \(String(describing: error))")
				result(nil, error)
			} else {
				if cursor != nil {
					print("-> getCloudRecord - initialOperation - records \(records.count) - cursor \(cursor!.description)")
					recurrentOperations(cursor: cursor!)
				} else {
					print("-> getCloudRecord - initialOperation - records \(records.count) - cursor nil - done")
					result(records, nil)
				}
			}
		}
		database.add(initialOperation)
	}
	
	
//	public func saveToCloudEach(name: String, note:String, phone:String) {
//
//        //클라우드 킷에 새로운 레코드를 저장한다.
//        let newCloudContact = CKRecord(recordType: CloudKitInfo.cloud_name_old)
//        newCloudContact.setValue(name, forKey: "name")
//        newCloudContact.setValue(note, forKey: "note")
//        newCloudContact.setValue(phone, forKey: "phone")
//
//
//		self.database.save(newCloudContact) { (record, error) in
//
//
//            if error == nil{
//                //                guard record != nil else { return }
//				print(debug: "별도 추가 완료")
////                msg = "백업 완료"
//            }else{
//
//                print(debug: error!.localizedDescription)
//
//                if error?.localizedDescription == "CloudKit access was denied by user settings"{
//					print(debug: "설정 -> iCloud -> iCloud Drive를 On 시켜주세요")
////                    msg = "설정 -> iCloud -> iCloud Drive를 On 시켜주세요"
//                }
//
//            }
//
//        }
//
//    }
	
	func removeAllDataFromCloud(recordType:String, completion: @escaping (Bool) -> Void){
		
		// predicate
		let predicate = NSPredicate(value: true)
		
		// query
		let cloudKitQuery = CKQuery(recordType: recordType, predicate: predicate)
		
		// records to store
		var currentRecords = [CKRecord]()
		var deleteCounter = 0


		// recurrent operations function
		var recurrentOperationsCounter = 101
		func recurrentOperations(cursor: CKQueryOperation.Cursor?){
			let recurrentOperation = CKQueryOperation(cursor: cursor!)
			recurrentOperation.recordFetchedBlock = { (record:CKRecord!) -> Void in
				print("-> cloudKitLoadRecords - recurrentOperations - fetch \(recurrentOperationsCounter)")
				recurrentOperationsCounter += 1
				self.database.delete(withRecordID: record.recordID, completionHandler: { (recordID, error) in
					Debug.print("delete")
					deleteCounter += 1
					if deleteCounter == currentRecords.count{
						completion(true)
					}
				})
				currentRecords.append(record)
			}
			recurrentOperation.queryCompletionBlock = { (cursor: CKQueryOperation.Cursor?, error: Error?) -> Void in
				if ((error) != nil) {
					print("-> cloudKitLoadRecords - recurrentOperations - error - \(String(describing: error))")
					
				} else {
					if cursor != nil {
						print("-> cloudKitLoadRecords - recurrentOperations - records \(currentRecords.count) - cursor \(cursor!.description)")
						recurrentOperations(cursor: cursor!)
					} else {
						print("-> cloudKitLoadRecords - recurrentOperations - records \(currentRecords.count) - cursor nil - done")
						completion(true)
						
					}
				}
			}
			database.add(recurrentOperation)
		}
		// initial operation
		var initialOperationCounter = 1
		let initialOperation = CKQueryOperation(query: cloudKitQuery)
		initialOperation.recordFetchedBlock = { (record:CKRecord!) -> Void in
			print("-> cloudKitLoadRecords - initialOperation - fetch \(initialOperationCounter)")
			initialOperationCounter += 1
			self.database.delete(withRecordID: record.recordID, completionHandler: { (recordID, error) in
				Debug.print("delete")
				deleteCounter += 1
				if deleteCounter == currentRecords.count{
					completion(true)
				}
				
			})
			currentRecords.append(record)
		}
		initialOperation.queryCompletionBlock = { (cursor: CKQueryOperation.Cursor?, error: Error?) -> Void in
			if ((error) != nil) {
				print("-> cloudKitLoadRecords - initialOperation - error - \(String(describing: error))")
				completion(false)
			} else {
				if cursor != nil {
					print("-> cloudKitLoadRecords - initialOperation - records \(currentRecords.count) - cursor \(cursor!.description)")
					recurrentOperations(cursor: cursor!)
				} else {
					print("-> cloudKitLoadRecords - initialOperation - records \(currentRecords.count) - cursor nil - done")
					if initialOperationCounter == 1{
						completion(true)
					}
					
				}
			}
		}
		database.add(initialOperation)


//		completion(true)
	}
	
	func uploadToCloud(models:[ContactRealmModel], completion: @escaping (Bool) -> Void){
		var newModels :[ContactModel] = []
		for model in models{
			
			let newModel = ContactModel(seq: model.seq, name: model.name, phone: model.phone, memo: model.memo)
			Debug.print(newModel)
			newModels.append(newModel)
		}
		
		
		let splitResults = newModels.chunked(into: 200)
		
		for (index, models) in splitResults.enumerated(){
			
			let json = models.toJSONString()
			let newCloudContact = CKRecord(recordType: CloudKitInfo.cloud_name)
			newCloudContact.setValue(index, forKey: "idx")
			newCloudContact.setValue(json, forKey: "contacts")
			self.database.save(newCloudContact) { (record, error) in

				
				if error == nil{
					//                guard record != nil else { return }
					Debug.print("백업 완료")
	//                msg = "백업 완료"
				}else{
					
					Debug.print(error!.localizedDescription)
					
					if error?.localizedDescription == "CloudKit access was denied by user settings"{
						Debug.print("설정 -> iCloud -> iCloud Drive를 On 시켜주세요")
	//                    msg = "설정 -> iCloud -> iCloud Drive를 On 시켜주세요"
					}
					
				}
				
				if index == splitResults.count - 1{
					completion(true)
				}
				
			}
			
		}
		
		
	}
}
