
import Foundation
import Realm
import RealmSwift

extension Results {
    var array: [Element]? {
        return self.count > 0 ? self.map { $0 } : nil
    }
}

final class RealmManager: RealmUsable {
    static let shared = RealmManager()
    
    func createRealm() -> Realm? {
        do {
            return try Realm(configuration: RealmManager.config)
        } catch let error as NSError {
            Debug.print("realm error: \(error)")
            LogManager.sharedInstance.write("realm error: \(error)", self)
            var config = RealmManager.config
            config.deleteRealmIfMigrationNeeded = true
            return try? Realm(configuration: config)
        }
    }
	
	private func getContact(seq:Int, realm:Realm) -> ContactRealmModel?{
		
		if realm.objects(ContactRealmModel.self).count == 0{
			
			return nil
		}
		
		guard let expectModel:ContactRealmModel = realm.objects(ContactRealmModel.self).filter("seq=\(seq)").first else {
			Debug.print("no content")
			
			return nil
		}
		
		return expectModel
	}
	func getAllContacts() -> [ContactRealmModel]?{
			
		if let realm = self.createRealm(){
			return realm.objects(ContactRealmModel.self).sorted(byKeyPath: "name").array
		}else{
			return nil
		}
		
	}
}

extension RealmManager{
	func createContatct(name:String, phone:String, memo:String, completion: @escaping (Bool, String?, ContactRealmModel?) -> Void) {
		
		if let realm = self.createRealm(){
			
			
			if let models = self.getAllContacts(){
				
//				if models.filter({ $0.phone == phone }).count > 0{
					
				if let expectModel = models.filter({ $0.phone == phone }).first {
					self.updateContact(seq: expectModel.seq, memo: (expectModel.memo ?? "") + "\n- 이름 :(" + name + ")의 연락처가 중복되어 통합되었습니다", completion: { (finish) in
						completion(true, expectModel.name, expectModel)
					})
				}
				else{
					let model = ContactRealmModel()
					model.seq = model.incrementSeq()
					model.name = name
					model.phone = phone
					model.memo = memo
					try! realm.write{
						realm.add(model)
					}
                    Debug.print("\(name) saved")
					completion(true, nil, model)
				}
				
			}
			else{
				let model = ContactRealmModel()
				model.seq = model.incrementSeq()
				model.name = name
				model.phone = phone
				model.memo = memo
				try! realm.write{
					realm.add(model)
				}
                Debug.print("\(name) saved")
				completion(true, nil, model)
			}
			
			
		}else{
			LogManager.sharedInstance.write("realm not created", self)
            Debug.print("\(name) failed")
			completion(false, nil, nil)
		}
		
		
	}
	
	func deleteContact(seq:Int, completion: @escaping (Bool) -> Void){
		
		do {
			guard let realm = self.createRealm() else {
				completion(false)
				return
			}
			
			guard let expectModel = self.getContact(seq: seq, realm: realm) else {
				completion(false)
				return
			}
			
			
			realm.beginWrite()
			realm.delete(expectModel)
			try realm.commitWrite()
			completion(true)
		}
		catch {
			Debug.print("deleteItem - \(error)")
            LogManager.sharedInstance.write("deleteItem - \(error.localizedDescription)", self)
			completion(false)
		}
		
		
	}
	
	func deleteAllContacts(completion: @escaping (Bool) -> Void){
        
		do {
			guard let realm = self.createRealm() else {
				completion(false)
				return
			}
			
			let models = realm.objects(ContactRealmModel.self)
            
			realm.beginWrite()
            
			for item in models{
				realm.delete(item)
                
			}
			
			try realm.commitWrite()
            Debug.print("deleteAllRealm complete")
            completion(true)
			
		}
		catch {
			Debug.print("deleteItem - \(error)")
            LogManager.sharedInstance.write("deleteItem - \(error.localizedDescription)", self)
			completion(false)
		}
	}
	
	func updateContact(seq:Int, name:String? = nil, phone:String? = nil, memo:String? = nil, completion: @escaping (Bool) -> Void) {
		guard let realm = self.createRealm() else {
			completion(false)
			return
		}
		
		guard let expectModel = self.getContact(seq: seq, realm: realm) else {
			completion(false)
			return
		}
		
		
		realm.beginWrite()
		expectModel.name = name ?? expectModel.name
		expectModel.phone = phone ?? expectModel.phone
		expectModel.memo = memo
		try! realm.commitWrite()
		
		
		
		completion(true)

	}
	
	func getContact(seq:Int) -> ContactRealmModel?{
		guard let realm = self.createRealm() else {
			return nil
		}
		return self.getContact(seq: seq, realm: realm)
	}
}
