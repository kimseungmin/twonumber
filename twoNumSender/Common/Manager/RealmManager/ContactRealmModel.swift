
import Foundation
import RealmSwift
import ObjectMapper

class ContactRealmModel: Object{
	@objc dynamic var seq: Int = 0
	@objc dynamic var name: String = ""
    @objc dynamic var phone: String = ""
    @objc dynamic var memo: String? = ""

	override static func primaryKey() -> String? {
		return "seq"
	}
	
	//Incrementa ID
	func incrementSeq() -> Int{
		let realm = try! Realm()
		if let retNext = realm.objects(ContactRealmModel.self).sorted(byKeyPath: "seq").last?.seq {
			return retNext + 1
		}else{
			return 1
		}
	}
}

class ContactModel: Mappable{
    var seq: Int        = 0
    var name: String    = ""
    var phone: String   = ""
    var memo: String    = ""
    var initial: String = ""
	var isSelected = false
	
    required init(map: ObjectMapper.Map) {
        self.seq     = 0
        self.name    = ""
        self.phone   = ""
        self.memo    = ""
        self.initial = ""
		self.isSelected = false
		
	}
	
	init(seq:Int, name:String, phone:String, memo: String? = nil, initial:String = "", isSelected:Bool = false) {
        self.seq     = seq
        self.name    = name
        self.phone   = phone
        self.memo    = memo ?? ""
        self.initial = initial
		self.isSelected = isSelected

		
	}
	
    func mapping(map: ObjectMapper.Map) {
		self.seq 				<- map["seq"]
		self.name 				<- map["name"]
		self.phone 				<- map["phone"]
		self.memo 				<- map["memo"]
		
		if map.mappingType == MappingType.fromJSON {
			self.isSelected     <- map["isSelected"]
		}
		
	}
}
