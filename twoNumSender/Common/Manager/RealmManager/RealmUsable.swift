
import RealmSwift

protocol RealmUsable {
    static var config: Realm.Configuration { get }
    func createRealm() -> Realm?
}

extension RealmUsable {
    
    static var config: Realm.Configuration {
        
        // copy file
        let defaultURL = Realm.Configuration.defaultConfiguration.fileURL!
//		print(debug: defaultURL)
        if !SystemUtils.shared.fileExists(path: defaultURL.path) {
            if let v0URL = SystemUtils.shared.bundleURL("default", withExtension: "realm") {
                do {
                    try FileManager.default.copyItem(at: v0URL, to: defaultURL)
                    Debug.print("default-v0.realm copy success.")
                } catch {
                    Debug.print("default-v0.realm copy failed.\n\(error)")
                    LogManager.sharedInstance.write("default-v0.realm copy failed.\n\(error)", self)
                }
            }
        }
        
        // migration
        return Realm.Configuration(schemaVersion: 1, migrationBlock: { migration, oldSchemeVersion in
            // We haven’t migrated anything yet, so oldSchemaVersion == 0
            if (oldSchemeVersion < 1) {
//                migration.enumerateObjects(ofType: ContactRealmModel.className()) { oldObject, newObject in
//
//                    // copy file
//                    let defaultURL = Realm.Configuration.defaultConfiguration.fileURL!
//                    if let v0URL = SystemUtils.shared.bundleURL("default", withExtension: "realm") {
//                        do {
//                            try FileManager.default.removeItem(at: defaultURL)
//                            try FileManager.default.copyItem(at: v0URL, to: defaultURL)
//                            print(debug: "default-v0.realm copy success.")
//                        } catch {
//                            print(debug: "default-v0.realm copy failed.\n\(error)")
//                            LogManager.sharedInstance.write("default-v0.realm copy failed.\n\(error)")
//                        }
//                    }
//
////                    // combine name fields into a single field
////                    newObject?["shotMode"] = 0
//                }
            }
        })
        
        // copy file
        
    }
}













