//
//  RouterManager.swift
//  CallRecorder
//
//  Created by vincent_kim on 2020/11/09.
//

import Foundation
import UIKit


enum ViewType {
	/**스플래시 화면*/
	case splash
	/** 메인화면 */
	case main
	
	/**튜토리얼*/
	case tutorial
    
    case addrDetail
}

enum ViewValueKeys: String {
    case realmContactModel = "realmContactModel"
    case expactContactModel = "expactContactModel"
    case isRegist = "isRegist"
}

final class RouterManager {

	static let shared = RouterManager()
	var window = UIWindow.key//UIApplication.shared.keyWindow
	
	
	private init() {
		
	}

    public func dispatcher(_ type: ViewType, values: [ViewValueKeys: Any]? = nil) {
		switch type {
			case .splash:
				self.moveToSplash()
			case .main:
				self.moveToMain()
			case .tutorial:
				self.moveToTutorial()
            case .addrDetail:
                let model = values?[.realmContactModel] as? ContactRealmModel
                let expectModel = values?[.expactContactModel] as? ContactModel
                let isRegist = values?[.isRegist] as? Bool
                
                self.moveToAddrDetail(model: model, expectModel: expectModel, isRegist: isRegist)
        }
	}
}

// MARK: move to View Controller
extension RouterManager {
	private func moveToExample(){
		let vc = SplashVC()
		self.window?.setRootNavVC(vc, options: .init(direction: .fade,
													 style: .linear,
													 duration: .main))
	}
	// splash view
	private func moveToSplash() {
		let vc = SplashVC()
		
		vc.modalTransitionStyle = .crossDissolve
		self.window?.rootViewController = vc

	}
	
	private func moveToMain(){
		let mainVC = MainTabBarVC()

//		let nav = UINavigationController(rootViewController: mainVC)
		self.window?.setRootNavVC(mainVC, options: .init(direction: .fade,
													  style: .easeOut,
													  duration: .main))
//		let vc = MainVC()
//		self.window?.setRootVC(vc, options: .init(direction: .toTop,
//												  style: .linear,
//												  duration: .main))
	}
	
	private func moveToTutorial(){
		let mainVC = TutorialCollectionVC()
		mainVC.tutorialType = .tutorial
		mainVC.modalTransitionStyle = .crossDissolve
		self.window?.rootViewController = mainVC
		
	}
	
	
    private func moveToAddrDetail(
        model: ContactRealmModel?,
        expectModel: ContactModel?,
        isRegist: Bool?
    ){
        let vc = AddrDetailVC()
        if let model = model {
            vc.model = model
        }
        
        if let expectModel = expectModel {
            vc.expectModel = expectModel
        }
        
        if let isRegist = isRegist {
            vc.isRegist = isRegist
        }
        
        self.window?.pushVC(vc)
    }
}
