//
//  ContactsConvertManager.swift
//  twoNumSender
//
//  Created by vincent_kim on 2020/10/06.
//  Copyright © 2020 remain. All rights reserved.
//

import Foundation

enum ConvertType{
	case phone
	case tel
}

enum MobileType:String{
	case kt = "kt"
	case skt = "skt"
	case uplus = "uplus"
}
class ContactsConvertManager {

	static let shared = ContactsConvertManager()
	
	func getWhiteSpaceString() -> String{
		if let whiteSpace = UnicodeScalar(8237){
			return String(whiteSpace)
			
		}else{
			return ""
		}
	}
}

extension ContactsConvertManager{
	func registerNewContact(convertType: ConvertType, phone:String, moType:MobileType) -> String{
		
		switch convertType {
			case .phone:
				return self.phoneContact(phone: phone, moType: moType)
			case .tel:
				return self.telContact(phone: phone, moType: moType)
		}
	}
	private func telContact(phone:String, moType:MobileType) -> String{
		var expectNumber = ""
		
		expectNumber = phone.replacingOccurrences(of: "-", with: "", options: .literal, range: nil)
		expectNumber = expectNumber.replacingOccurrences(of: " ", with: "", options: .literal, range: nil)
		expectNumber = expectNumber.replacingOccurrences(of: "*", with: "", options: .literal, range: nil)
		expectNumber = expectNumber.replacingOccurrences(of: "+82", with: "", options: .literal, range: nil)
		expectNumber = expectNumber.replacingOccurrences(of: self.getWhiteSpaceString(), with: "", options: .literal, range: nil)
		
		switch moType {
			case .kt:
				expectNumber = "*77 " + expectNumber
			case .skt:
				expectNumber = "*281 " + expectNumber
			case .uplus:
				expectNumber = expectNumber + "*"
		}
		
		return expectNumber
	}
	private func phoneContact(phone:String, moType:MobileType) -> String {
		var expectNumber = ""
		
		expectNumber = phone.replacingOccurrences(of: "-", with: "", options: .literal, range: nil)
		expectNumber = expectNumber.replacingOccurrences(of: " ", with: "", options: .literal, range: nil)
		expectNumber = expectNumber.replacingOccurrences(of: "*", with: "", options: .literal, range: nil)
		expectNumber = expectNumber.replacingOccurrences(of: "+", with: "", options: .literal, range: nil)
		expectNumber = expectNumber.replacingOccurrences(of: self.getWhiteSpaceString(), with: "", options: .literal, range: nil)
		
		expectNumber = String(expectNumber.suffix(8))
		Debug.print(expectNumber)
		
//
//
//
//		01034747123
//		if expectNumber.hasPrefix("8210") {
//            expectNumber = String(expectNumber.dropFirst(4))
//        }
//		else if expectNumber.hasPrefix("010"){
//			expectNumber = String(expectNumber.dropFirst(3))
//		}
//
//		if expectNumber.contains("*77") || expectNumber.contains("*281"){
//
//            if expectNumber.range(of: "*77010") != nil{
//                expectNumber = expectNumber.replacingOccurrences(of: "*77010", with: "")
//
//
//            }else if expectNumber.range(of: "*7710") != nil{
//                expectNumber = expectNumber.replacingOccurrences(of: "*7710", with: "")
//
//                //
//            }else if expectNumber.range(of: "*281010") != nil{
//                expectNumber = expectNumber.replacingOccurrences(of: "*281010", with: "")
//
//
//            }else if expectNumber.range(of: "*28110") != nil{
//                expectNumber = expectNumber.replacingOccurrences(of: "*28110", with: "")
//
//
//            }else if expectNumber.range(of: "*28182") != nil{
//                expectNumber = expectNumber.replacingOccurrences(of: "*28182", with: "")
//
//
//            }
//            //    print(array)
//
//        }
//
		switch moType {
			case .kt:
				expectNumber = "*77 010" + expectNumber
			case .skt:
				expectNumber = "*281 010" + expectNumber
			case .uplus:
				expectNumber = "010" + expectNumber + "*"
		}
		return expectNumber
	}
	
}
