
import Foundation

protocol KeyNamespaceable {
    func namespaced<T : RawRepresentable>(_ key: T) -> String
}

extension KeyNamespaceable {
    func namespaced<T: RawRepresentable>(_ key: T) -> String {
        return "\(Self.self).\(key.rawValue)"
    }
}

protocol StringDefaultSettable : KeyNamespaceable {
    associatedtype StringKey : RawRepresentable
}

extension StringDefaultSettable where StringKey.RawValue == String {
    
    // set
    func set(_ value: Int, forKey key: StringKey) {
        let key = namespaced(key)
        UserDefaults.standard.set(value, forKey: key)
    }
    
    func set(_ value: Float, forKey key: StringKey) {
        let key = namespaced(key)
        UserDefaults.standard.set(value, forKey: key)
    }
    
    func set(_ value: String, forKey key: StringKey) {
        let key = namespaced(key)
        UserDefaults.standard.set(value, forKey: key)
    }
    
    func set(_ value: Bool, forKey key: StringKey) {
        let key = namespaced(key)
        UserDefaults.standard.set(value, forKey: key)
    }
    
    func set(_ value: Any, forKey key: StringKey) {
        let key = namespaced(key)
        UserDefaults.standard.set(value, forKey: key)
    }
    
    // get
    func int(forKey key: StringKey) -> Int? {
        let keySpaced = namespaced(key)
        guard let _ = self.object(forKey: key) else {
            return nil
        }
        
        return UserDefaults.standard.integer(forKey: keySpaced)
    }
    
    func float(forKey key: StringKey) -> Float? {
        let keySpaced = namespaced(key)
        guard let _ = self.object(forKey: key) else {
            return nil
        }
        
        return UserDefaults.standard.float(forKey: keySpaced)
    }
    
    @discardableResult
    func string(forKey key: StringKey) -> String? {
        let keySpaced = namespaced(key)
        guard let _ = self.object(forKey: key) else {
            return nil
        }
        
        return UserDefaults.standard.string(forKey: keySpaced)
    }
    
    func bool(forKey key: StringKey) -> Bool? {
        let keySpaced = namespaced(key)
        guard let _ = self.object(forKey: key) else {
            return nil
        }
        
        return UserDefaults.standard.bool(forKey: keySpaced)
    }
    
    func object(forKey key: StringKey) -> Any? {
        let keySpaced = namespaced(key)
        return UserDefaults.standard.object(forKey: keySpaced)
    }
}

extension UserDefaults : StringDefaultSettable {
    enum StringKey : String {
        
        case seq            // 2.0 버전 유저 정보
        case id             // 2.0 버전 유저 정보
        case nick           // 2.0 버전 유저 정보
        case purchase_type  // 2.0 버전 유저 정보
        case tel_type       // 2.0 버전 유저 정보
        case reg_type       // 2.0 버전 유저 정보
        case expiration_date// 2.0 버전 유저 정보
        case create_date    // 2.0 버전 유저 정보
        case launchedBefore // launchedBefore
        case facebook       // 이벤트 체크
        case syncCheck      // 2.0 버전 싱크 처리
        
        case mobileType     // 앱 통신사 유형
        case localSync      // 3.0 버전 로컬 적용 싱크 처리
        case finalLogin     // 3.0 버전 로컬 적용 싱크 처리
        case premium        // 프리미엄 회원
        case lastPremiumDate     // 프리미엄 회원
        
        case userContacts   // 유저 투넘버 정보
		case isMigration	// 3.2.1 realm으로 마이그레이션
    }
}

