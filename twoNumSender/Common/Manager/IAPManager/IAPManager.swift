//
//  IAPManager.swift
//  twoNumSender
//
//  Created by vincent_kim on 2020/05/29.
//  Copyright © 2020 remain. All rights reserved.
//

import UIKit
import StoreKit
import SwiftyStoreKit
import CallKit
import SVProgressHUD
import TPInAppReceipt

class IAPManager : NSObject{
    
    private var sharedSecret = "9fa8fa07b4b644b4b9a5cbc392bc1247"
    @objc static let shared = IAPManager()
	private var isPurchased: Bool? = nil
	static let adminString: String = "vincent"
	
	func getPurchaseStatus() -> Bool?{
		return self.isPurchased
	}
	func setPurchaseStatus(_ status:Bool){
		self.isPurchased = status
	}
    private override init(){}
    
    
}

struct IAPTextInfo {
	/**복원 완료*/
	static let restore_success       = "복원 완료"

	/**복원 실패*/
	static let restore_failed        = "복원 실패"

	/**복원할 상품이 없습니다*/
	static let no_content            = "복원할 상품이 없습니다"

	/**복원이 완료 되었습니다.*/
	static let msg_restore_success   = "복원이 완료 되었습니다.\n연락처 표시기능이 활성화됩니다"

	/**복원할 상품이 없습니다.\n앱스토어 구매 내역 및 만료 일자를 확인해주세요*/
	static let msg_restore_faild     = "복원할 상품이 없습니다.\n앱스토어 구매 내역 및 만료 일자를 확인해주세요\n해당 화면을 캡쳐하여 메일로 제보하실 수 있습니다"

	/**구독상품 검증중입니다\n화면이 새로고침 될 수 있습니다*/
	static let msg_checking_purchase = "구독상품 검증중입니다\n화면이 새로고침 될 수 있습니다"
	
}

//MARK:- 구매
extension IAPManager{
    // 구매 케이스
    public func purchase(_ purchase: RegisteredPurchase, atomically: Bool) {
        SVProgressHUD.show()
//        NetworkActivityIndicatorManager.networkOperationStarted()
        SwiftyStoreKit.purchaseProduct(purchase.rawValue, atomically: atomically) { result in
//            NetworkActivityIndicatorManager.networkOperationFinished()

            if case .success(let purchase) = result {
                let downloads = purchase.transaction.downloads
                if !downloads.isEmpty {
                    SwiftyStoreKit.start(downloads)
                }
                // Deliver content from server, then:
                if purchase.needsFinishTransaction {
                    SwiftyStoreKit.finishTransaction(purchase.transaction)
                }
            }
            
            if self.checkPurchaseResult(result) == true{
                switch result {
                case .success(let purchase):
                    let okAction = UIAlertAction(title: "확인", style: .default, handler: {action in
                        
                        self.savePurchasedProductIdentifier(purchase.productId)
                    })
                    UIAlertController.showAlert(title: "구독 완료", message: "구독이 완료 되었습니다", actions: [okAction])
                default:
                    break
                }
                
                
            }
            else{
                
                
                let okAction = UIAlertAction(title: "확인", style: .default, handler: {action in
                    
                    self.removePurchasedProductIdentifier()
                })
                UIAlertController.showAlert(title: "결재 실패", message: "재 시도 해 주세요", actions: [okAction])
                
            }
            
                
//            if let alert = self.alertForPurchaseResult(result) {
//
//                self.showAlert(alert)
//            }
        }
    }
    //신규 구매 검증
    public func verifyPurchase(_ purchase: RegisteredPurchase) {

//        NetworkActivityIndicatorManager.networkOperationStarted()
        verifyReceipt { result in
//            NetworkActivityIndicatorManager.networkOperationFinished()

            switch result {
            case .success(let receipt):

                let productId = purchase.rawValue

                switch purchase {
//                case .autoRenewableWeekly:
//                    break
                case .autoRenewableMonthly, .autoRenewableYearly:
                    let purchaseResult = SwiftyStoreKit.verifySubscription(
                        ofType: .autoRenewable,
                        productId: productId,
                        inReceipt: receipt)
                    if (self.checkUserSubscription(purchaseResult, productIds: [productId]) == true){
                        // 성공 시
                        self.savePurchasedProductIdentifier(productId)
                    }else{
                        // 실패 시
                        
                        self.removePurchasedProductIdentifier()
                    }
//                    self.showAlert(self.alertForVerifySubscriptions(purchaseResult, productIds: [productId]))
//                case .nonRenewingPurchase:
//                    let purchaseResult = SwiftyStoreKit.verifySubscription(
//                        ofType: .nonRenewing(validDuration: 60),
//                        productId: productId,
//                        inReceipt: receipt)
//                    self.showAlert(self.alertForVerifySubscriptions(purchaseResult, productIds: [productId]))
//                default:
//                    let purchaseResult = SwiftyStoreKit.verifyPurchase(
//                        productId: productId,
//                        inReceipt: receipt)
//                    self.showAlert(self.alertForVerifyPurchase(purchaseResult, productId: productId))
                }

            case .error:
                if (self.checkVerifyReceipt(result) == false){
                    Debug.print(result)
					SVProgressHUD.dismiss()
                    
                }
//                self.showAlert(self.alertForVerifyReceipt(result))
            }
        }
    }

}

//MARK: - 복원
extension IAPManager{
	// 유저가 직접 복원한 케이스
	public func restore(){
		SVProgressHUD.show()
//        NetworkActivityIndicatorManager.networkOperationStarted()
//
//        SwiftyStoreKit.restorePurchases(atomically: true) { results in
////            NetworkActivityIndicatorManager.networkOperationFinished()
//
//            for purchase in results.restoredPurchases {
//                let downloads = purchase.transaction.downloads
//                if !downloads.isEmpty {
//                    SwiftyStoreKit.start(downloads)
//                } else if purchase.needsFinishTransaction {
//                    // Deliver content from server, then:
//                    SwiftyStoreKit.finishTransaction(purchase.transaction)
//                }
//            }
			InAppReceipt.refresh { (error) in
				if let err = error{
					
                    Debug.print(err.localizedDescription, simple: true)
					self.removePurchasedProductIdentifier()
					SVProgressHUD.dismiss()
				}else{
					// initializeRecipt
					if let receipt = try? InAppReceipt.localReceipt() {
						 
						if receipt.hasActiveAutoRenewablePurchases {
							// user has active subscription
							// return true
							
							if let purchase = receipt.purchases.filter({ $0.subscriptionExpirationDate! >= Date()}).last{
                                Debug.print(" ✅ HasActiveAutoRenewable\nPurchases Info : \(purchase) \ncreate: \(String(describing: purchase.purchaseDate.dateStringKR)) \nexpiry: \(String(describing: purchase.subscriptionExpirationDate!.dateStringKR)) \ntransactionIdentifier: \(purchase.transactionIdentifier)")
							}
							
							self.savePurchasedProductIdentifier(RegisteredPurchase.autoRenewableMonthly.rawValue)
							UIApplication.topViewController()?.dismiss(animated: true, completion: {
								
								let okAction = UIAlertAction(title: TextInfo.alert_confirm, style: .default, handler: {action in

								})
								UIAlertController.showAlert(title: IAPTextInfo.restore_success, message: IAPTextInfo.msg_restore_success, actions: [okAction])
							})
							
						}
						else{
							Debug.print("⛔️ noneActiveAutoRenewablePurchases\(receipt.expirationDate?.dateStringKR ?? "") ")
							// none
							self.removePurchasedProductIdentifier()
							UIApplication.topViewController()?.dismiss(animated: true, completion: {
								
								let okAction = UIAlertAction(title: TextInfo.alert_confirm, style: .default, handler: {action in

								})
								UIAlertController.showAlert(title: IAPTextInfo.restore_failed, message: IAPTextInfo.msg_restore_faild, actions: [okAction])
							})
							
						}
					
					}
					
				}
			}
			

//            self.showAlert(self.alertForRestorePurchases(results))
//        }
	}
}


//MARK: - 검증
extension IAPManager{
    
    //MARK: 영수증 검증 공통
    public func verifyReceipt(completion: @escaping (VerifyReceiptResult) -> Void) {
        
        let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: self.sharedSecret)
        SwiftyStoreKit.verifyReceipt(using: appleValidator, completion: completion)
    }
	
	
	func verifyRecipt(withProgress:Bool){
		if withProgress{
			SVProgressHUD.show()
		}
		else{
			if let controller = UIApplication.topViewController(){
				Toast.makeToast(message: IAPTextInfo.msg_checking_purchase, controller: controller)
			}
			
		}
		InAppReceipt.refresh { (error) in
			if let err = error{
				
                Debug.print(err.localizedDescription, simple: true)
				self.removePurchasedProductIdentifier()
				SVProgressHUD.dismiss()
			}else{
				// initializeRecipt
				if let receipt = try? InAppReceipt.localReceipt() {
					  // ...
                    Debug.print("🟩 Found LocalReceipt")
					
					do {
                        try receipt.validate()
					} catch IARError.validationFailed(reason: .hashValidation) {
						// Hash validation failed
                        Debug.print("⛔️ hashValidation")
						self.removePurchasedProductIdentifier()
//						SVProgressHUD.dismiss()
						return
					} catch IARError.validationFailed(reason: .bundleIdentifierVerification) {
						// Bundle identifier verification failed
                        Debug.print("⛔️ bundleIdentifierVerification")
						self.removePurchasedProductIdentifier()
//						SVProgressHUD.dismiss()
						return
					} catch IARError.validationFailed(reason: .signatureValidation) {
						// Signature validation
                        Debug.print("⛔️ signatureValidation")
						self.removePurchasedProductIdentifier()
//						SVProgressHUD.dismiss()
						return
					} catch {
						// Miscellaneous error
                        Debug.print("⛔️ Miscellaneous")
						self.removePurchasedProductIdentifier()
//						SVProgressHUD.dismiss()
						return
					}
					
					if receipt.hasActiveAutoRenewablePurchases {
						// user has active subscription
						// return true
						
						if let purchase = receipt.purchases.filter({ $0.subscriptionExpirationDate! >= Date()}).last{
                            Debug.print(" ✅ HasActiveAutoRenewable\nPurchases Info : \(purchase) \ncreate: \(String(describing: purchase.purchaseDate.dateStringKR)) \nexpiry: \(String(describing: purchase.subscriptionExpirationDate!.dateStringKR)) \ntransactionIdentifier: \(purchase.transactionIdentifier)")
                            Debug.print("🔵 receiptCount =  \(receipt.purchases.count)")
						}
						
						self.savePurchasedProductIdentifier(RegisteredPurchase.autoRenewableMonthly.rawValue)

					}
					else{
						Debug.print("⛔️ noneActiveAutoRenewablePurchases\(receipt.expirationDate?.dateStringKR ?? "") ")
						// none
						self.removePurchasedProductIdentifier()
						
					}
				
				}
				
			}
		}
	}
//    //MARK: 앱 실행 시 구독 검증 1
//    func verifySubscriptions(_ purchases: Set<RegisteredPurchase>) {
//        SVProgressHUD.show()
//        NetworkActivityIndicatorManager.networkOperationStarted()
//        verifyReceipt { result in
//            NetworkActivityIndicatorManager.networkOperationFinished()
//
//            switch result {
//            case .success(let receipt):
//                let productIds = Set(purchases.map { $0.rawValue })
//                let purchaseResult = SwiftyStoreKit.verifySubscriptions(productIds: productIds, inReceipt: receipt)
//                switch purchaseResult {
//                case .purchased://(let expiryDate, let items):
////                    print(debug: "\(productIds)'s \(items) is expiry at: \(expiryDate) ex")
//                    self.savePurchasedProductIdentifier(RegisteredPurchase.autoRenewableMonthly.rawValue)
//
//                case .notPurchased:
//                    self.verifyUserSubscription(purchaseResult: purchaseResult, productIds: productIds)
////                    print(debug:"The user has never purchased \(productIds)")
//
//                case .expired://let expiryDate, let items):
//                    self.verifyUserSubscription(purchaseResult: purchaseResult, productIds: productIds)
////                    print(debug: "\(productIds) is expiryDate: \(expiryDate) - item : \(items)")
//
//                }
//
////                self.showAlert(self.alertForVerifySubscriptions(purchaseResult, productIds: productIds))
//            case .error:
//                if (self.checkVerifyReceipt(result) == false){
//                    print(debug: result)
//					SVProgressHUD.dismiss()
//
//                }
////                self.showAlert(self.alertForVerifyReceipt(result))
//            }
//        }
//    }
//    //MARK: 앱 실행 시 구독 검증 2 (영수증 검사에서 미 사용으로 판별 시)
//    func verifyUserSubscription(purchaseResult:VerifySubscriptionResult, productIds: Set<String>){
//        if (self.checkUserSubscription(purchaseResult, productIds: productIds) == true){
//            // 성공시
//            self.savePurchasedProductIdentifier(RegisteredPurchase.autoRenewableMonthly.rawValue)
//
//        }else{
//            // 실패시
//            self.restoreForVerify(success: { (result) in
//                self.savePurchasedProductIdentifier(RegisteredPurchase.autoRenewableMonthly.rawValue)
//            }, failure: {
//                self.removePurchasedProductIdentifier()
//            })
//
//
//        }
//    }
//
//    //MARK: 앱 실행 시 구독 검증 3 (구독 검증 이 후 복원 시도)
//    public func restoreForVerify(success: @escaping (RestoreResults) -> Void, failure: @escaping () -> Void){
//
//        NetworkActivityIndicatorManager.networkOperationStarted()
//        SwiftyStoreKit.restorePurchases(atomically: true) { results in
//            NetworkActivityIndicatorManager.networkOperationFinished()
//
//            for purchase in results.restoredPurchases {
//                let downloads = purchase.transaction.downloads
//                if !downloads.isEmpty {
//                    SwiftyStoreKit.start(downloads)
//                } else if purchase.needsFinishTransaction {
//                    // Deliver content from server, then:
//                    SwiftyStoreKit.finishTransaction(purchase.transaction)
//                }
//            }
//            print(debug: self.checkUserRestorePurchases(results))
//            if self.checkUserRestorePurchases(results) == true{
//                success(results)
//            }
//            else{
//                failure()
//
//            }
//
//        }
//    }

    
}


//MARK: METHOD
extension IAPManager{
    public func getInfo(_ purchase: RegisteredPurchase) {

//        NetworkActivityIndicatorManager.networkOperationStarted()
        SwiftyStoreKit.retrieveProductsInfo([purchase.rawValue]) { result in
//            NetworkActivityIndicatorManager.networkOperationFinished()

//            self.showAlert(self.alertForProductRetrievalInfo(result))
        }
    }
    func checkPurchaseResult(_ result: PurchaseResult) -> Bool {
        switch result {
        case .success(let purchase):
            print("Purchase Success: \(purchase.productId)")
            return true
        case .error(let error):
            print("Purchase Failed: \(error)")

            return false
        }
    }
    
    
    // swiftlint:disable cyclomatic_complexity
//    func alertForPurchaseResult(_ result: PurchaseResult) -> UIAlertController? {
//        switch result {
//        case .success(let purchase):
//            print("Purchase Success: \(purchase.productId)")
//            return nil
//        case .error(let error):
//            print("Purchase Failed: \(error)")
//            switch error.code {
//            case .unknown: return alertWithTitle("Purchase failed", message: error.localizedDescription)
//            case .clientInvalid: // client is not allowed to issue the request, etc.
//                return alertWithTitle("Purchase failed", message: "Not allowed to make the payment")
//            case .paymentCancelled: // user cancelled the request, etc.
//                return nil
//            case .paymentInvalid: // purchase identifier was invalid, etc.
//                return alertWithTitle("Purchase failed", message: "The purchase identifier was invalid")
//            case .paymentNotAllowed: // this device is not allowed to make the payment
//                return alertWithTitle("Purchase failed", message: "The device is not allowed to make the payment")
//            case .storeProductNotAvailable: // Product is not available in the current storefront
//                return alertWithTitle("Purchase failed", message: "The product is not available in the current storefront")
//            case .cloudServicePermissionDenied: // user has not allowed access to cloud service information
//                return alertWithTitle("Purchase failed", message: "Access to cloud service information is not allowed")
//            case .cloudServiceNetworkConnectionFailed: // the device could not connect to the nework
//                return alertWithTitle("Purchase failed", message: "Could not connect to the network")
//            case .cloudServiceRevoked: // user has revoked permission to use this cloud service
//                return alertWithTitle("Purchase failed", message: "Cloud service was revoked")
//            default:
//                return alertWithTitle("Purchase failed", message: (error as NSError).localizedDescription)
//            }
//        }
//    }
    
    func checkUserRestorePurchases(_ results: RestoreResults) -> Bool {

        if results.restoreFailedPurchases.count > 0 {
//            print("Restore Failed: \(results.restoreFailedPurchases)")
            Debug.print("restore Failed")
            return false
        } else if results.restoredPurchases.count > 0 {
//            print("Restore Success: \(results.restoredPurchases)")
            Debug.print("restore Success")
            return true
        } else {
            Debug.print("Nothing to Restore")
//            print("Nothing to Restore")
            return false
        }
    }

//    func alertForRestorePurchases(_ results: RestoreResults) -> UIAlertController {
//
//        if results.restoreFailedPurchases.count > 0 {
//            print("Restore Failed: \(results.restoreFailedPurchases)")
//            return alertWithTitle("Restore failed", message: "Unknown error. Please contact support")
//        } else if results.restoredPurchases.count > 0 {
//            print("Restore Success: \(results.restoredPurchases)")
//            return alertWithTitle("Purchases Restored", message: "All purchases have been restored")
//        } else {
//            print("Nothing to Restore")
//            return alertWithTitle("Nothing to restore", message: "No previous purchases were found")
//        }
//    }
    func checkVerifyReceipt(_ result: VerifyReceiptResult) -> Bool {
        switch result {
        case .success://(let receipt):
            Debug.print("Verify receipt Success")
//            print("Verify receipt Success: \(receipt)")
            return true
        case .error(let error):
            Debug.print("Verify receipt Failed")
//            print("Verify receipt Failed: \(error)")
            switch error {
            case .noReceiptData:
                return false
            case .networkError(let error):
                Debug.print(error)
                return false
            default:
                return false
            }
        }
    }
//    func alertForVerifyReceipt(_ result: VerifyReceiptResult) -> UIAlertController {
//
//        switch result {
//        case .success(let receipt):
//            print("Verify receipt Success: \(receipt)")
//            return alertWithTitle("Receipt verified", message: "Receipt verified remotely")
//        case .error(let error):
//            print("Verify receipt Failed: \(error)")
//            switch error {
//            case .noReceiptData:
//                return alertWithTitle("Receipt verification", message: "No receipt data. Try again.")
//            case .networkError(let error):
//                return alertWithTitle("Receipt verification", message: "Network error while verifying receipt: \(error)")
//            default:
//                return alertWithTitle("Receipt verification", message: "Receipt verification failed: \(error)")
//            }
//        }
//    }

    func checkUserSubscription(_ result: VerifySubscriptionResult, productIds: Set<String>) -> Bool {

        switch result {
        case .purchased://(let expiryDate, let items):
            
//            print("\(productIds) is valid until \(expiryDate)\n\(items)\n")
            return true
        case .expired://(let expiryDate, let items):
//            print("\(productIds) is expired since \(expiryDate)\n\(items)\n")
            return false
        case .notPurchased:
//            print("\(productIds) has never been purchased")
            return false
        }
    }
    
//    func alertForVerifySubscriptions(_ result: VerifySubscriptionResult, productIds: Set<String>) -> UIAlertController {
//
//        switch result {
//        case .purchased(let expiryDate, let items):
//            print("\(productIds) is valid until \(expiryDate)\n\(items)\n")
//            return alertWithTitle("Product is purchased", message: "Product is valid until \(expiryDate)")
//        case .expired(let expiryDate, let items):
//            print("\(productIds) is expired since \(expiryDate)\n\(items)\n")
//            return alertWithTitle("Product expired", message: "Product is expired since \(expiryDate)")
//        case .notPurchased:
//            print("\(productIds) has never been purchased")
//            return alertWithTitle("Not purchased", message: "This product has never been purchased")
//        }
//    }

//    func alertForVerifyPurchase(_ result: VerifyPurchaseResult, productId: String) -> UIAlertController {
//
//        switch result {
//        case .purchased:
//            print("\(productId) is purchased")
//            return alertWithTitle("Product is purchased", message: "Product will not expire")
//        case .notPurchased:
//            print("\(productId) has never been purchased")
//            return alertWithTitle("Not purchased", message: "This product has never been purchased")
//        }
//    }
    
}



//MARK:- Save & Remove Method

extension IAPManager{
    func savePurchasedProductIdentifier(_ id:String) {
		CallKitManager.shared.saveIsPurchased(status: true)
		
		SVProgressHUD.dismiss()
        Debug.print("✅ Success Check Payment [Saved UserDefaults]")
		
        UserDefaults.standard.set(id, forKey: UserDefaults.StringKey.premium)
//        CallKitManager.shared.startSaveCallkitData()
		self.isPurchased = true
        self.refreshSetting()
        
    }
    
    func removePurchasedProductIdentifier(){
		CallKitManager.shared.saveIsPurchased(status: false)
		
		SVProgressHUD.dismiss()
		Debug.print("🚫 Failed Check Payment\n-- [Removed UserDefaults]")

		if let purchaseStatus = UserDefaults.standard.string(forKey: .premium){
			if purchaseStatus == IAPManager.adminString{
                Debug.print("🟩 Admin User")
				self.savePurchasedProductIdentifier(IAPManager.adminString)
				
				return
			}
		}
		
		
		UserDefaults.standard.set("", forKey: UserDefaults.StringKey.premium)
		self.isPurchased = false
		
		
        self.refreshSetting()
        
    }
    func refreshSetting(){
		CallKitManager.shared.saveCallKitData(isProgress:false, success: nil, failure: nil)
        let userInfo: [String: String] = ["refresh":"refreshSetting"]
        NotificationCenter.default.post(name: Notification.Name.refreshSetting, object: userInfo)
        SVProgressHUD.dismiss()
    }
}





//MARK: - Alert
extension IAPManager{
    
}
