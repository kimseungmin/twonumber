//
//  IAPModel.swift
//  twoNumSender
//
//  Created by seungmin kim on 01/06/2020.
//  Copyright © 2020 remain. All rights reserved.
//

import Foundation
enum RegisteredPurchase: String {
//    case purchase1
//    case purchase2
//    case nonConsumablePurchase
//    case consumablePurchase
//    case nonRenewingPurchase
//    case autoRenewableWeekly
    case autoRenewableMonthly  = "com.remain.twoNumSender.1month"
    case autoRenewableYearly = "com.remain.twoNumSender.year"
}

enum RestoreType{
    case restore
    case verify
    
    
    var info: (title: String, msg: String){
        switch self {
        case .restore:
            return ("복원 성공", "복원이 완료 되었습니다")
        case .verify:
            return ("앱 안정화 완료", "기다려주셔서 감사합니다")
        }
    }
    
}
