//
//  DeepLinkManager.swift
//


import Foundation

struct DeeplinkInfo: Codable {
    var name: String = ""
    var tel: String = ""
}

final class DeepLinkManager {
    static let shared = DeepLinkManager()
    
    private(set) var deepLinkInfo: DeeplinkInfo? = nil
}

extension DeepLinkManager {
    func setDeeplinkInfo(info: DeeplinkInfo) {
        self.deepLinkInfo = info
    }
    
    func getDeeplinkInfo() -> DeeplinkInfo? {
        return self.deepLinkInfo
    }
    
    func clear() {
        self.deepLinkInfo = nil
    }
}
