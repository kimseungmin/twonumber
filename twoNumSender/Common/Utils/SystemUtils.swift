//
//  SystemUtils.swift
//  twoNumSender
//
//  Created by dkt_mac on 19/07/2019.
//  Copyright © 2019 remain. All rights reserved.
//

import UIKit

final class SystemUtils {
    static let shared = SystemUtils()
    
   
    private init() {
        
    }
    
    func appVersion() -> String {
        return Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
    }
    
    func appstoreVersion() -> String {
        let url = URL(string: "https://itunes.apple.com/kr/lookup?id=\(AppInfo.appId)")!
        do {
            let data = try Data(contentsOf: url)
            let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any]
            let results = json?["results"] as! [[String: Any]]
            
            let appStoreVersion: String
            if results.count > 0 {
                appStoreVersion = results[0]["version"] as! String
            }
            else {
                appStoreVersion = ""
            }
            return appStoreVersion
        }
        catch {
            Debug.print("\(error.localizedDescription)")
        }
        
        return ""
    }
    
    func bundleId() -> String {
        return Bundle.main.infoDictionary!["CFBundleIdentifier"] as! String
    }
    
    func appName() -> String {
        return Bundle.main.infoDictionary!["CFBundleDisplayName"] as! String
    }
    
    func osVersion() -> String {
        return ProcessInfo().operatingSystemVersionString
    }
    
    func statusBarHeight() -> CGFloat {
        return UIApplication.shared.statusBarFrame.height
    }
    
    func bundleURL(_ name: String, withExtension: String) -> URL? {
        return Bundle.main.url(forResource: name, withExtension: withExtension)
    }
    
    func adjustValue(value: CGFloat) -> CGFloat {
        return UIScreen.main.bounds.size.width * value / 375.0
    }
    
    func adjustWidth(value: CGFloat) -> CGFloat {
        return UIScreen.main.bounds.size.width * value / 375.0
    }
    
    func adjustHeight(value: CGFloat) -> CGFloat {
        return UIScreen.main.bounds.size.height * value / 667.0
    }
    
    // format : yyyy-MM-dd, yyyy-MM-dd HH:mm:ss, etc
    func currentDate(_ format: String) -> String {
        
        let now = Date()
        
        let date = DateFormatter()
        date.locale = Locale(identifier: "ko_kr")
        date.timeZone = TimeZone(abbreviation: "KST")    // "2018-03-21 18:07:27"
        date.dateFormat = format
        
        return date.string(from: now)
    }
    
    func delay(delay: Double, closure: @escaping () -> ()) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
            closure()
        }
    }
    
    
    
    // 앱스토어 버전과 앱 버전 비교하여 업데이트 가능 여부체크
    func isUpdateAvailable() -> Bool {
        
        let appVersion = self.appVersion()
        let appStoreVersion = self.appstoreVersion()
        Debug.print(appVersion)
        Debug.print(appStoreVersion)
        let needUpdate = appVersion.compare(appStoreVersion, options: .numeric)
        
        if needUpdate == .orderedSame {
            // same version
            Debug.print("버전 같음 (DEBUG)")
            return false
        } else if needUpdate == .orderedAscending {
            Debug.print("기 설치 버전이 더 작은 경우")
            return true
        } else if needUpdate == .orderedDescending {
            Debug.print("기 설치 버전이 더 큰 경우")
            return false
        }
        return false
        
    }

    func fileExists(path: String) -> Bool {
        return FileManager.default.fileExists(atPath: path)
            
    }
    
    
	func getCurrentTimeZone() -> String{

	   return TimeZone.current.identifier

	}
	func getTimeStamp() -> Int{
		let nowDouble = NSDate().timeIntervalSince1970
		
		return Int(nowDouble*1000)
	}
	
	static func getNibBundle(vc: AnyClass) -> Bundle {
		return Bundle(for: vc.self)
//        return Bundle(for: SelectNetworkVC.self)
		// print(Utility.getNibBundl1e)
	}
	
	static func getNibName(name: String) -> String {
		return name
//        return "SelectNetworkVC"
		// print(Utility.getNibName)
	}
	
}

extension UIViewController {
    
    
    enum AlertType {
        case normal
        case force
    }
    
    func showUpdateAlert(alertType: AlertType) {
        let alert = UIAlertController(title: AppInfo.alert_update_title, message: AppInfo.alert_update_message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: AppInfo.alert_ok, style: .default, handler: { Void in
            guard let url = URL(string: "itms-apps://itunes.apple.com/app/id\(AppInfo.appId)") else { return }
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        })
        
        
        alert.addAction(okAction)
        if alertType == .normal {
            alert.addAction(UIAlertAction(title: AppInfo.alert_cancel, style: .cancel) { action in
                Timer.scheduledTimer( timeInterval: 0.5 , target:self, selector: #selector(SplashVC.verifyUserInfo), userInfo: nil, repeats: false)
            })
            
        }
        //        UIApplication.shared.keyWindow?.rootViewController
        self.present(alert, animated: true, completion: nil)
    }
}

