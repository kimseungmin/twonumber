//
//  NetInfo.swift
//  twonumsender
//
//  Created by kimseungmin on 2017. 2. 28..
//  Copyright © 2017년 remain. All rights reserved.
//

import Foundation
struct NetInfo {
 
    // MainURl
    static let Api                 = "http://remainsvc.synology.me:8000/dualnumber/"
    static let WebUrl              = "http://remainsvc.synology.me:8000/"

    // 일반 로그인 & 유저 조회 & 수정 // LoginVC
    static let userLogin           = "userinfo/"

    static let userReg             = "user_reg"

    // 유저 연락처 전체 조회
    static let userContactList     = "/contact_list/"

    // 유저 연락처 등록
    static let contact_add         = "contact_add"

    // 연락처 상세 조회
    static let contact_detail      = "contact_detail/"

    // 연락처 text검색
    static let filter_contact_list = "/filter_contact_list/?text="

    // 개인정보 처리 방침
//    static let policy_term = WebUrl + "twonumber/privacy_term"
    static let policy_term         = "https://ashlab.synology.me/twonum_terms/Privacy.html"
//    static let policy_term         = "http://remainsvc.synology.me/twonum_terms/Privacy.html"
    static let qna                 = "https://www.facebook.com/twonumMsg/"
    static let faq                 = "https://www.facebook.com/123515911577387/posts/429321450996830?s=100003282938789&sfns=mo"
    
}
