//
//  model.swift
//  twoNumSender
//
//  Created by seungmin kim on 2018. 6. 4..
//  Copyright © 2018년 remain. All rights reserved.
//

import Foundation

func resetUserModel(){
    let defaults = UserDefaults.standard
//    defaults.set(nil, forKey: UserModel.seq)
//    defaults.set(nil, forKey: UserModel.id)
//    defaults.set(nil, forKey: UserModel.nick)
//    defaults.set(nil, forKey: UserModel.purchase_type)
//    defaults.set(nil, forKey: UserModel.tel_type)
//    defaults.set(nil, forKey: UserModel.reg_type)
//    defaults.set(nil, forKey: UserModel.expiration_date)
//    defaults.set(nil, forKey: UserModel.create_date)
//    defaults.set(false, forKey: "launchedBefore")
//    
    defaults.set("", forKey: UserDefaults.StringKey.seq)
    defaults.set("", forKey: UserDefaults.StringKey.id)
    defaults.set("", forKey: UserDefaults.StringKey.nick)
    defaults.set("", forKey: UserDefaults.StringKey.purchase_type)
    defaults.set("", forKey: UserDefaults.StringKey.tel_type)
    defaults.set("", forKey: UserDefaults.StringKey.reg_type)
    defaults.set("", forKey: UserDefaults.StringKey.expiration_date)
    defaults.set("", forKey: UserDefaults.StringKey.create_date)
    defaults.set(false, forKey: .launchedBefore)
    
    print("유저 정보 초기화 완료")
}

func setUserInfo(_ seq:Int, id:String, nick:String, purchase_type:Int, tel_type:String, reg_type:String, expiration_date:String, create_date:String){
    let defaults = UserDefaults.standard
//    defaults.set(seq, forKey: UserModel.seq)
//    defaults.set(id, forKey: UserModel.id)
//    defaults.set(nick, forKey: UserModel.nick)
//    defaults.set(purchase_type, forKey: UserModel.purchase_type)
//    defaults.set(tel_type, forKey: UserModel.tel_type)
//    defaults.set(reg_type, forKey: UserModel.reg_type)
//    defaults.set(expiration_date, forKey: UserModel.expiration_date)
//    defaults.set(create_date, forKey: UserModel.create_date)
    
    defaults.set(seq, forKey: .seq)
    defaults.set(seq, forKey: .id)
    defaults.set(seq, forKey: .nick)
    defaults.set(seq, forKey: .purchase_type)
    defaults.set(seq, forKey: .tel_type)
    defaults.set(seq, forKey: .reg_type)
    defaults.set(seq, forKey: .expiration_date)
    defaults.set(seq, forKey: .create_date)
    
    
}

//struct UserModel {
//
//    static let seq             = "seq"
//    static let id              = "id"
//    static let nick            = "nick"
//    static let purchase_type   = "purchase_type"
////    static let tel_type = "tel_type"
//    static let tel_type        = "mobileType"
//    static let reg_type        = "reg_type"
//    static let expiration_date = "expiration_date"
//    static let create_date     = "create_date"
//    static let localVersion    = "localVersion"
//}


