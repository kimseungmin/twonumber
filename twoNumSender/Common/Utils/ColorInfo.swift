//
//  ColorInfo.swift
//  twoNumSender
//
//  Created by kimseungmin on 2017. 4. 26..
//  Copyright © 2017년 remain. All rights reserved.
//

import Foundation

struct ColorInfo {
    
    static let TOPBAR = "9C76EB"
    static let MAIN = "#B8A4E2"
    //#B8A4E2
    //#9C76EB
    //#8900FF
    static let MAIN2 = "#F3F2F7"
    
    static let DARK = "#3B383F"
    
    static let INDICATORBG = "#00000020"
    
    static let hide = "#00000000"
}


