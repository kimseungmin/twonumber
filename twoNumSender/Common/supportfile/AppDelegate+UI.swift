//
//  AppDelegate+UI.swift
//


import Foundation
import UIKit

// MARK: - UI
extension AppDelegate {
    
    func initDefaultSettings() {
        self.setTableViewAppearance()
        self.setTabbarAppearance()
    }
    
    private func setTableViewAppearance() {
        UITableView.appearance().tableHeaderView = .init(frame: .init(x: 0, y: 0, width: 0, height: CGFloat.leastNormalMagnitude))
        UITableView.appearance().tableFooterView = .init(frame: .init(x: 0, y: 0, width: 0, height: CGFloat.leastNormalMagnitude))
        if #available(iOS 15.0, *) {
            UITableView.appearance().sectionHeaderTopPadding = 0.0
            UITableView.appearance().sectionFooterHeight = 0.0
        }
        UITableView.appearance().contentInsetAdjustmentBehavior = .never
    }
    
    private func setTabbarAppearance() {
        //tabBar 선택 시 색상지정
        
//        UITabBar.appearance().tintColor = UIColor(hexString: ColorInfo.MAIN)
//        UITabBar.appearance().barTintColor = UIColor(hexString: ColorInfo.DARK)
        
    }
}
