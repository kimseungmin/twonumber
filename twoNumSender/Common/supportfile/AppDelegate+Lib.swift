//
//  AppDelegate+Lib.swift
//


import Foundation
import SVProgressHUD
import SwiftyStoreKit
import Firebase
import IQKeyboardManagerSwift
import RealmSwift
import CoreData

extension AppDelegate{
    func initProgressHUD(){
        // SVProgressHUD setting
        self.setSVProgressHUD()
    }
    
    func initStoreKit(){
    
        
        SwiftyStoreKit.completeTransactions(atomically: true) { purchases in

            for purchase in purchases {
                switch purchase.transaction.transactionState {
                case .purchased, .restored:
                    let downloads = purchase.transaction.downloads
                    if !downloads.isEmpty {
                        SwiftyStoreKit.start(downloads)
                    } else if purchase.needsFinishTransaction {
                        // Deliver content from server, then:
                        
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                    }
                    print("\(purchase.transaction.transactionState.debugDescription): \(purchase.productId)")
                    
                case .failed, .purchasing, .deferred:
                    break // do nothing
                @unknown default:
                    break // do nothing
                }
            }
        }
        
        SwiftyStoreKit.updatedDownloadsHandler = { downloads in

            // contentURL is not nil if downloadState == .finished
            let contentURLs = downloads.compactMap { $0.contentURL }
            if contentURLs.count == downloads.count {
                print("Saving: \(contentURLs)")
                SwiftyStoreKit.finishTransaction(downloads[0].transaction)
            }
        }
        
        
        
    }
    
    func initFireBase(){
        // Firebase setting
        FirebaseApp.configure()
        
    }
    
    func initKeyBoardManager(){
    
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "닫기"
    }
    
    
}

extension AppDelegate{
    private func setSVProgressHUD() {
        SVProgressHUD.setDefaultStyle(.light)
        SVProgressHUD.setDefaultMaskType(.black)
    }
}

//MARK: - realm
extension AppDelegate{
    func initRealm(){
        
        if let _ = RealmManager.shared.createRealm(){
            Debug.print("create realm")
            
            if UserDefaults.standard.bool(forKey: .isMigration) != true{
                var dataArray: [NSManagedObject] = []
                
                var isMergedContact:Bool = false
                var mergedArray:[String] = []
                CoreDataManager.shared.getData(completion: { (result) in
                    dataArray = result
                })
                
                
                for item in dataArray{
                    let name = item.value(forKey: kNameStr) as? String ?? ""
                    let phone = item.value(forKey: kPhoneStr) as? String ?? ""
                    let memo = item.value(forKey: kNoteStr) as? String ?? ""
                    RealmManager.shared.createContatct(name: name, phone: phone, memo: memo, completion: { (success, mergedName, _) in
                        if let mergedName = mergedName{
                            isMergedContact = true
                            mergedArray.append(mergedName)
                        }
                        if success{
                            Debug.print("save success\n - \(name)")
                        }else{
                            Debug.print("save failed\n - \(name)")
                        }
                        
                    })
                }
                Debug.print(isMergedContact)
                Debug.print(mergedArray)
                UserDefaults.standard.set(true, forKey: .isMigration)
            }
            
            
            #if DEBUG
//            for i in 1...1200{
//                RealmManager.shared.createContatct(name: "\(i) 번", phone: "\(i)", memo: "", completion: { (success, mergedName, _) in
//
//                    if success{
//
//                        print(debug: "save ")
//                    }else{
//                        print(debug: "save failed")
//                    }
//
//                })
//            }
            #endif
            
        }
        else{
            Debug.print("failed create realm")
        }
    }
}
