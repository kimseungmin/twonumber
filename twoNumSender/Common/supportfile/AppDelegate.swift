//
//  AppDelegate.swift
//  twoNumSender
//
//  Created by kimseungmin on 2017. 4. 26..
//  Copyright © 2017년 remain. All rights reserved.
//

import UIKit
import CallKit
import UserNotifications

let kEntityStr = "Contacts"
let kNameStr = "name"
let kPhoneStr = "phone"
let kNoteStr = "note"
//let kSeqStr = "seq"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
	

//    class var shared: AppDelegate {
//        return UIApplication.shared.delegate as! AppDelegate
//    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window!.makeKeyAndVisible()
        NSSetUncaughtExceptionHandler { exception in
            print("Error Handling:\(exception)")
            print("Error Handling callStackSymbols: \(exception.callStackSymbols)")
        }
        
		// Realm
		self.initRealm()
		
		// Loading Progress
        self.initProgressHUD()
		
		// In App Purchase
        self.initStoreKit()
		
		// FireBase crashlitycs
        self.initFireBase()
		
		// Keyboard Lib
        self.initKeyBoardManager()
		
		
		self.initNotification(application: application)
        
        

        self.initDefaultSettings()
		RouterManager.shared.dispatcher(.splash)
        
        return true
        
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        return true
    }
    

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.

//        let purchaseStatus = UserDefaults.standard.string(forKey: .premium)
//        if purchaseStatus == "" || purchaseStatus == nil{
//            print(debug: "\(purchaseStatus ?? "")", self)
//        }else{
//            if purchaseStatus != "test"{
//                InAppPurchase.sharedInstance.restoreTransactions("동기화 중", indicator: false)
//            }
//
//        }
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
//        CoreDataManager.shared.saveContext()
    }
}

// MARK: - Handle Deep Link & Lifecycle
extension AppDelegate {
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        if let urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: true),
           let queryItems = urlComponents.queryItems,
           let host = urlComponents.host {
            
            let queryResult = self.queryToDictionary(queryItems)
            do {
                let jsonData = try JSONSerialization.data(
                    withJSONObject: queryResult,
                    options: .prettyPrinted
                )
                
                let result = try JSONDecoder().decode(DeeplinkInfo.self, from: jsonData)
                Debug.print(result)
                DeepLinkManager.shared.setDeeplinkInfo(info: result)
            }

            catch {
                Debug.print(error.localizedDescription)
            }
            
        }
        
        return false
    }
    
    private func queryToDictionary(_ array: [URLQueryItem]?) -> [String:String?]{
        guard array != nil else {
            return [:]
        }
        
        var dictionary = [String : String?]()
        for v in array! {
            dictionary.updateValue(v.value, forKey: v.name)
        }
        return dictionary
    }
}



//MARK: - custom scheme
extension AppDelegate{
	
}

//MARK: - Notification

extension AppDelegate: UNUserNotificationCenterDelegate {
	func initNotification(application: UIApplication){

		let center = UNUserNotificationCenter.current()
        center.delegate = self
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)

            center.requestAuthorization(options: [.alert, .sound, .badge]) {
                (granted, error) in
                guard granted else { return }
                DispatchQueue.main.async {
                    application.registerForRemoteNotifications()

                }
            }
        }
        /**permission 과 무관하게 token 전송*/
        application.registerForRemoteNotifications()
		
	}
	
	//MARK: Success
	func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
		let tokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
//        let tokenString = deviceToken.map { String(format: "%02x", $0) }.joined()
		Debug.print("device token: \(tokenString)")
		APNSManager.shared.setDeviceToken(token: tokenString)
		
		
	}
	func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
		print(error)
//        print(debug: error)
	}

	//MARK: recieve message
	func application(_ application: UIApplication,
					 didReceiveRemoteNotification userInfo: [AnyHashable : Any],
					 fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
		
		print("\n"+"didReceiveRemoteNotification = \(userInfo)"+"\n")
		completionHandler(UIBackgroundFetchResult.newData)
	}
	
	//MARK: Foreground Noti event
	func userNotificationCenter(_ center: UNUserNotificationCenter,
								willPresent notification: UNNotification,
								withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
		Debug.print("UNUserNotificationCenterDelegate willPresent notification = \(notification.request.content.userInfo)")
		
		
        completionHandler([.alert, .sound])
		
		
	}
	
	//MARK: action
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {

		
		APNSManager.shared.userInfo(response.notification.request.content.userInfo)
//		print(debug: APNSManager.shared.phone, self)
//		let phone = APNSManager.shared.phone
//		let name = APNSManager.shared.name
//		let model = UserData(phoneNumber: phone, name: name)
//		SchemeManager.shared.setIsSelectedPhone(user: model)
		
        completionHandler()
		
    }


}


